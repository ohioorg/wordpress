<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|V Yn Wm.%+Y|/2D;S0g|,]nNA)<I#u`6T-Io-Mrew6re6&?Wx+F56xSKR[ZRZS.');
define('SECURE_AUTH_KEY',  'qE]r;aPINeoHJ*|dvZR,^Tft@F$`eqv,1!oSYP^]1kPEf$`m@lpR%(DRb/N+P((|');
define('LOGGED_IN_KEY',    'C3/U$3@V*Joag@/O^`?cs#RBxA&rGMq[&a3j6h!TYZpm><p:&6mXkc)tXU[KL$</');
define('NONCE_KEY',        'ph vRI}Cv6jJqaf:&e&6YbE<h7=k[1IkQs5;-ZW[{VD-7pPE_^zg!5cSOK _.+)l');
define('AUTH_SALT',        'm2y2~>up6u!#V4/w|(dWPZzJdCEi$AV<B8@E(m3IZ8<Zy-<QGA)Cp2aq+]Z$G(1:');
define('SECURE_AUTH_SALT', '>.!#FA_|_oKt5@1uz}F#uB0qhFWLI[M;?TLfnkVfZ5EhE26|ASEq{Ijs_=0/mx~2');
define('LOGGED_IN_SALT',   '$@i2CXO7pnx/G5U5IC *7c(Q<4k#{|?Oa=F!$6N3fBVc{X=1]%01,cSuBdE2ALeZ');
define('NONCE_SALT',       'j;TP@H :a*0OcHmkgpS>L?~EiXA2xevIbWSAm9#WU;ST:<y0wSQtR(IL51T,p]%s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
