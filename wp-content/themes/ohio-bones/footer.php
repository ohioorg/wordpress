
<?php include( locate_template( '/page-components/banners/newsletter-signup.php', false, false ) ); ?>

<footer class="footer pad-20 stone-back" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="cf">
					
					<div class="cf">
					<div class="col-4 tab-6 phone-12">
						<div id="social-icons" class="pad-10 cf center">
						<div class="col-fifths center">
							  <span class="fa-layers">
								<i class="fas fa-2x fa-circle white"></i>
								<i class="fa-inverse fa-2x fab fa-facebook stone" data-fa-transform="shrink-6 right-1"></i>
							  </span>
						</div>
						<div class="col-fifths center">
							  <span class="fa-layers fa-fw">
								<i class="fas fa-2x fa-circle white"></i>
								<i class="fa-inverse fa-2x fab fa-twitter stone" data-fa-transform="shrink-6"></i>
							  </span>
						</div>
						<div class="col-fifths center">
							  <span class="fa-layers">
								<i class="fas fa-2x fa-circle white"></i>
								<i class="fa-inverse fa-2x fab fa-instagram stone" data-fa-transform="shrink-6 right-1"></i>
							  </span>
						</div>
						<div class="col-fifths center">
							  <span class="fa-layers fa-fw">
								<i class="fas fa-2x fa-circle white"></i>
								<i class="fa-inverse fa-2x fab fa-pinterest stone" data-fa-transform="shrink-6"></i>
							  </span>
						</div>
						<div class="col-fifths center">
							  <span class="fa-layers">
								<i class="fas fa-2x fa-circle white"></i>
								<i class="fa-inverse fa-2x fab fa-youtube stone" data-fa-transform="shrink-6 left-1"></i>
							  </span>
						</div>
						</div>
						
						<div id="search-wrap">
							<?php echo get_search_form(); ?>
							<div class="clear"></div>
						</div>
						
					</div>
				
				    <div class="col-4 tab-6 phone-12 center">
						<div class="logo-main" itemscope itemtype="http://schema.org/Organization">
							<a href="<?php echo home_url(); ?>" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/library/logo/logo-tagline.svg" height="89"></a>
						</div>
					</div>
					
					<div class="col-4 tab-12 tab-pad">
						<div class="center white font-12">
						© 2018 Ohio Development Services Agency, TourismOhio.
						John R. Kasich, Governor. Mary Taylor, Lt. Governor.
						</div>
					</div>
					<div class="clear"></div>

					<div id="footer-nav-wrap">
					<nav role="navigation">
						<div class="col-2 tab-4">
							<?php dynamic_sidebar( 'footer1' ); ?>
						</div>
						<div class="col-2 tab-4">
							<?php dynamic_sidebar( 'footer2' ); ?>
						</div>
						<div class="col-2 tab-4">
							<?php dynamic_sidebar( 'footer3' ); ?>
						</div>
						<div class="col-2 tab-4">
							<?php dynamic_sidebar( 'footer4' ); ?>
						</div>
						<div class="col-2 tab-4">
							<?php dynamic_sidebar( 'footer5' ); ?>
						</div>
						<div class="col-2 tab-4">
							<?php dynamic_sidebar( 'footer6' ); ?>
						</div>
						<div class="clear"></div>
					</nav>
					</div>

					<!-- <p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p> -->

				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=210026906469855&autoLogAppEvents=1';
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
