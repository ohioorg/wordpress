<?php get_header(); ?>

<main id="main" role="main" class="texture-light" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

<?php
$user = wp_get_current_user();
$blog_id = get_queried_object_id();

//Query Options
$blog_args = array(
	'post_type' => 'post',
	'page_id' => $blog_id,
	'status' => 'publish',
	'posts_per_page'=> 1
);

$blog_query = new WP_Query( $blog_args ); ?>

<?php if( $blog_query->have_posts() ) : while ( $blog_query->have_posts() ) : $blog_query->the_post(); ?>

<?php

//Headlines
$blog_title = get_the_title();
$blog_head = get_field('blog-headline');
$blog_sub = strip_tags(get_field('blog-subtitle'));

if ($blog_head) :
	$blog_title = $blog_head;
endif;

//Main Image & Caption
if( have_rows('blog-image') ): while( have_rows('blog-image') ): the_row();
	
	$blog_image_pull = get_sub_field('image');
	$blog_image = $blog_image_pull['sizes']['large'];
	$blog_image_caption = get_sub_field('caption');
	$blog_image_url = get_sub_field('url');

	if (!($blog_image)):
		$blog_image = $blog_image_url;
	endif;

endwhile; endif;

// Author & Pic
$blog_author = get_field('blog-author');
$blog_user = get_the_author_meta( 'display_name');
$blog_author_pic = get_avatar( get_the_author_email(), '60' );
$blog_date = get_the_time(get_option('date_format'));

if ($blog_author) :
	$blog_user = $blog_author;
endif;

//Text
$blog_intro = get_field('blog-body');

//Categories
$blog_cats =  wp_get_post_terms(get_the_ID(), 'content', array('orderby' => 'term_order' ) );
foreach($blog_cats as $cat){ 
	$blog_category = $cat->name; 
	$blog_cat_slug = $cat->slug; 
	$cat_string .= $cat->name . ", ";
	$categories = rtrim($cat_string, ", ");
	break; 
}

//Show Region
$blog_region =  wp_get_post_terms(get_the_ID(), 'region', array('orderby' => 'term_order' ) );
foreach($blog_region as $reg){ 
	$blog_region_name = $reg->name; 
	$blog_region_slug = $reg->slug; 
	$region_string .= $reg->name . ", ";
	$regions = rtrim($reg_string, ", ");
	break; 
}
	
//Show Region
$blog_city =  wp_get_post_terms(get_the_ID(), 'city', array('orderby' => 'term_order' ) );
foreach($blog_city as $cit){ 
	$blog_city_name = $cit->name; 
	$blog_city_slug = $cit->slug; 
	$city_string .= $cit->name . ", ";
	$citys = rtrim($city_string, ", ");
break; 
}

//Media
$blog_gallery = get_field('blog-gallery');
$blog_video = get_field('blog-video');
$blog_map = get_field('blog-map');

$blog_related_articles = get_field('blog-related-articles');

endwhile; endif;

?>
<meta itemprop="primaryImageOfPage" content="<?php echo $blog_image; ?>" />
<!---- STRUCTURED DATA --->
<span itemscope role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
	<meta itemprop="url" content="<?= get_the_permalink(); ?>" />
	<meta itemprop="image" content="<?php echo $blog_image; ?>">
	<meta itemprop="name" content="<?php $blog_title; ?>" />
	<meta itemprop="headline" content="<?php $blog_title; ?>" />
	<meta itemprop="isFamilyFriendly" content="true" />
	<meta itemprop="author" content="<?php echo $blog_user; ?>" />
	<meta itemprop="articleSection" content="<?php echo $blog_category; ?>" />
	<meta itemprop="keywords" content="<?php echo $categories; ?>" />
	<meta itemprop="about" content="<?php echo $categories; ?>" />
	<meta itemprop="dateCreated" content="<?php echo $blog_date; ?>" />
	<meta itemprop="dateModified" content="<?php echo $blog_date; ?>" />
	<meta itemprop="copyrightYear" content="<?php echo date('Y'); ?>" />
	<meta itemprop="copyrightHolder" content="Ohio. Find It Here." />
	<meta itemprop="publisher" content="Ohio. Find It Here." />


<article id="post-<?php the_ID(); ?>" class="nopadding" itemprop="articleBody">
<div class="blog-scroll-head texture-light">
	<div class="col-8 tab-12">
	<div class="head-24"><?php echo $blog_title; ?></div>
	</div>
	<div class="col-4 tab-12">
	<div class="share-wrap nopadding nomargin">
		  <div class="a2a_kit a2a_kit_size_48 a2a_default_style tab-hide">
				<a class="a2a_button_facebook blue"></a>
				<a class="a2a_button_twitter foam"></a>
				<a class="a2a_button_pinterest red"></a>
				<a class="a2a_button_email green"></a>
				<a class="a2a_dd a2a_counter" href="https://www.addtoany.com/share"></a>
		  </div>
		  <div class="a2a_kit a2a_kit_size_32 a2a_default_style tab-show">
				<a class="a2a_button_facebook blue"></a>
				<a class="a2a_button_twitter foam"></a>
				<a class="a2a_button_pinterest red"></a>
				<a class="a2a_button_email green"></a>
				<a class="a2a_dd a2a_counter" href="https://www.addtoany.com/share"></a>
		  </div>
		</div>
	</div>
	<div class="clear"></div>
</div>
	
<!-- HEADER IMAGE -->
<div class="main-image" style="background-image: url('<?php echo $blog_image; ?>')">
	<section class="full nopadding relative">	
			<div class="main-image-inner">
				
			    <div class="main-image-inner-content textalignleft bottom">
					<div id="breadcrumbs" class="center top padbottom">
					<a href="<?php echo get_site_url(); ?>">Ohio. Find It Here.</a> <i class="fas fa-angle-right orange"></i>
					<a href="<?php echo get_site_url(); ?>/blog/">Ohio Stories Blog</a> <i class="fas fa-angle-right orange"></i>
					<?php if($blog_region_name):?><a href="<?php echo get_site_url(); ?>/region/<?php echo $blog_region_slug; ?>"><?php echo $blog_region_name; ?></a> <?php endif; ?><?php if($blog_city_name):?><i class="fas fa-angle-right orange"></i>
					<a href="<?php echo get_site_url(); ?>/city/<?php echo $blog_city_slug; ?>"><?php echo $blog_city_name; ?></a><?php endif; ?>
				</div>
					<h1 class="head-72 handy shadow white center"><?php echo $blog_title; ?></h1>
					<?php if($blog_sub): ?><h2 class="head-28 light shadow white center margin-bottom-15"><?php echo $blog_sub ;?></h2><?php endif; ?>
					<?php if($blog_image_caption) : ?><div class="main-image-caption"><?php echo $blog_image_caption ;?></div><?php endif; ?>
				</div>
			</div>
			
	
	</section>
</div>
	
<div id="content" class="nopadding">			
<section class="cf full nopadding" itemprop="articleBody">
	
	<!-- CONTENT -->
	<div class="inner wsidebar dropcap">
		<div id="blog-main" class="box margin-top-30">
			<div class="byline-photo cf">
				<?php echo $blog_author_pic; ?>
			</div>
			<div class="byline-meta cf">
				<div class="byline-name">
					<div class="head-18">By <span itemprop="author" itemscope itemptype="http://schema.org/Person"><?php echo $blog_user; ?></span></div>
					<div><span class="font-12 italic red">
						<time class="updated" datetime="<?php get_the_time('Y-m-d'); ?>" itemprop="datePublished">Posted On: <?php echo $blog_date; ?></time></span></div>
				</div>
			</div>
		<div class="share-wrap">
		  <div class="a2a_kit a2a_kit_size_48 a2a_default_style">
				<a class="a2a_button_facebook blue"></a>
				<a class="a2a_button_twitter foam"></a>
				<a class="a2a_button_pinterest red"></a>
				<a class="a2a_button_email green"></a>
				<a class="a2a_dd a2a_counter" href="https://www.addtoany.com/share"></a>
			</div>
		</div>
			
			<?php echo $blog_intro; ?>
			<?php the_content(); ?>
			
		</div> 
		
		<!-- VIDEO -->
		<?php if ($blog_video) : ?>
		<div id="blog-footer" class="box margin-top-60">
			<div class="circle-icon overlap">
				<span class="fa-layers fa-fw">
					<i class="fas fa-circle red"></i>
					<i class="fa-inverse fas fa-video" data-fa-transform="shrink-6"></i>
				</span>
			</div>
			<div class="head-42 handy center">related video</div>
			<div class="video-wrap">
				<?php echo $blog_video ;?>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/0cgCUskHqGM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				
			</div>
			<div class="clear"></div>
		</div>
		<?php endif; ?>
		
		<!-- COMMENTS -->
		<section>
		<div id="blog-comments" class="box margin-top-60">
			<div class="circle-icon overlap">
				<span class="fa-layers fa-fw">
					<i class="fas fa-circle green"></i>
					<i class="fa-inverse fas fa-comments" data-fa-transform="shrink-6"></i>
				</span>
			</div>
			<div class="head-42 handy center">leave a comment</div>
			<div class="fb-comments" data-width="100%" data-href="<?php global $wp; echo home_url( $wp->request ) ; ?>" data-numposts="10"></div>
			<div class="clear"></div>
		</div>
		</section>
		
		
		<?php if ($blog_related_articles) : ?>
		<!-- RELATED ARTICLES -->
		<div id="related-articles" class="relative">
		<div class="circle-icon">
			<span class="fa-layers fa-fw">
				<i class="fas fa-circle red"></i>
				<i class="fa-inverse fas fa-pencil-alt" data-fa-transform="shrink-6"></i>
			</span>
		</div>
		<div class="head-54 handy center">related articles</div>
		<p class="italics center"> Get even more great Ohio trip inspiration with these articles we think you'll like:</p>
			
		<?php include( locate_template( '/page-components/carousels/related-articles.php', false, false ) ); ?>
			
		</div>
		<?php endif; ?>
		
		</div>
		
		<!-- SIDEBAR -->
		<?php include( locate_template( '/page-components/sidebars/blog.php', false, false ) ); ?>

</section> 
</div>

</article> <?php // end article ?>
	
</span> <?php // schema Wrapper ?>

</main>
					

<?php get_footer(); ?>
