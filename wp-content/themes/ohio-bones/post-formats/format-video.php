<?php
$user = wp_get_current_user();
$blog_id = get_queried_object_id();

//Fixes URL Redirect Issues
if(!$blog_id){
	$blog_id = mbcom_getPostID();
	$post = get_post($blog_id);
}

//Query Options
$blog_args = array(
	'post_type' => 'post',
	'page_id' => $blog_id,
	'status' => 'publish',
	'posts_per_page'=> 1
);

$blog_query = new WP_Query( $blog_args ); ?>

<?php if( $blog_query->have_posts() ) : while ( $blog_query->have_posts() ) : $blog_query->the_post(); ?>

<?php

//Main Image & Caption
if( have_rows('media-image') ): while( have_rows('media-image') ): the_row();
	$blog_image_pull = get_sub_field('image');
	$blog_image = $blog_image_pull['sizes']['large'];
	$blog_image_caption = get_sub_field('caption');
	$blog_image_align = get_sub_field('position');
	$blog_image_alt = get_sub_field('alt');

endwhile; endif;

//Show Category
$blog_cats =  wp_get_post_terms(get_the_ID(), 'content', array('orderby' => 'term_order' ) );
foreach($blog_cats as $cat){ 
	$blog_category = $cat->name; 
	$blog_cat_slug = $cat->slug; 
	$cat_string .= $cat->name . ", ";
	$categories = rtrim($cat_string, ", ");
	break; 
}

//Text
$blog_intro = get_field('media-text');

//Gallery
$blog_gallery = get_field('media-gallery');

//Video
$blog_video = get_field('media-video');

//Map
$blog_map = get_field('media-map');

endwhile; endif;

?>

              <article id="post-<?php the_ID(); ?>" role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

                <header class="article-header entry-header">
				  <div class="label-14 orange pad-5"> <?php printf( __( '', 'bonestheme' ).'%1$s', get_the_category_list(', ') ); ?></div>
                  <h1 class="head-66 handy" itemprop="headline" rel="bookmark"><?php the_title(); ?></h1>
					
				   <div class="byline-meta cf">
					<div class="byline-photo">
						<?php echo get_avatar( get_the_author_email(), '24' ); ?>
					</div>
					<div class="byline-name">
						<div class="head-18">By <span itemprop="author" itemscope itemptype="http://schema.org/Person"><?php echo get_the_author_meta( 'display_name' ); ?></span>
						<span class="font-12 italic red"><time class="updated" datetime="<?php get_the_time('Y-m-d'); ?>" itemprop="datePublished">Posted On: <?php echo get_the_time(get_option('date_format')); ?></time></span></div>
					</div>
				  </div>
				
				  <?php if ($blog_image) :?>
				  <div class="img-wrap">
				 	 <img src="<?php echo $blog_image; ?>" alt="<?php echo $blog_image_alt; ?>" title="<?php echo $blog_image_alt; ?>">
				  </div>
				  <?php if ($blog_image_caption) :?>
					<div class="img-caption"><?php echo $blog_image_caption; ?> </div>
				  <?php endif; ?>
				  <?php endif; ?>

                </header> <?php // end article header ?>

                <section class="cf" itemprop="articleBody">
                  <div class="dropcap">
					  
					<?php echo $blog_intro; ?>
					<?php
                    // the content (pretty self explanatory huh)
                    the_content(); ?>
					  
				  </div>
                </section> <?php // end article section ?>

                <footer class="article-footer">

                 

                  <div class="video-wrap">
					<?php echo $blog_video ;?>
				  </div>

                </footer> <?php // end article footer ?>

                <?php //comments_template(); ?>

              </article> <?php // end article ?>
