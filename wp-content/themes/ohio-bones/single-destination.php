<?php /*Destination Pages */ ?>

<?php get_header(); ?>

<?php 

	//Grabs page_id for current page
	$page_id = get_queried_object_id();

	//Query options
	 $destination_args = array( 
	 'post_type' => 'destination',
	 'posts_per_page'=>1,
	 'p' => $page_id,
	 );

	$destination_query = new WP_Query( $destination_args );

	if ($destination_query->have_posts() ) : while ( $destination_query->have_posts() ) : $destination_query->the_post();

	//Headlines
	$dest_title = get_the_title();
	$dest_head = get_field('listing-headline');
	$dest_rating = get_field('listing-rating');

	if ($dest_head) :
		$dest_title = $dest_head;
	endif;

	//Main Image & Caption
	if( have_rows('listing-image') ): while( have_rows('listing-image') ): the_row();

		$dest_image_pull = get_sub_field('image');
		$dest_image = $dest_image_pull['url'];
		$dest_image_sm = $dest_image_pull['sizes']['small'];
		$dest_image_caption = get_sub_field('caption');
		$dest_image_url = get_sub_field('url');

		if (!($dest_image)):
			$dest_image = $dest_image_url;
			$dest_image_sm = $dest_image_url;
		endif;

		if (!($dest_image)):
			$img_short = 'yes';
			$dest_image = '/wordpress/wp-content/uploads/2018/05/BackGenericGray.jpg';
			$dest_image_sm = '/wordpress/wp-content/uploads/2018/05/BackGenericGray.jpg';
		endif;

	endwhile; endif;

	$dest_gallery = get_field('listing-gallery');
	$dest_video = get_field('listing-video');
	
	//Contact Info
	$dest_email = get_field('listing-email');
	$dest_phone = get_field('listing-phone');
	$dest_url = get_field('listing-website');

	//Location Info
	if( have_rows('listing-location') ): while( have_rows('listing-location') ): the_row();
		$dest_street = get_sub_field('address');
		$dest_street_2 = get_sub_field('address-2');
		$dest_city = get_sub_field('city');
		$dest_state = get_sub_field('state');
		$dest_zip = get_sub_field('zipcode');
		$dest_lat = get_sub_field('latitude');
		$dest_long = get_sub_field('longitude');
		$dest_map = get_sub_field('map');
		$dest_directions = get_sub_field('directions');
	endwhile; endif;
	
	$dest_address = $dest_street.', '.$dest_city.', '.$dest_state.' '.$dest_zip;

	//Details
	$dest_text = get_field('listing-body');
	$dest_amenities = strip_tags(get_field('listing-amenities'));
	$dest_amenities_2 = strip_tags(get_field('listing-amenities-2'));
	
	if($dest_amenities_2):
		$dest_amenities_list = $dest_amenities.', '.$dest_amenities_2;
 	else: 
		$dest_amenities_list = $dest_amenities;
	endif; 

	$dest_rates_from = get_field('listing-rates-from');


	//Social Links
	if( have_rows('listing-social') ): while( have_rows('listing-social') ): the_row();
		$social_platform = get_sub_field('name');
		$social_url = get_sub_field('url');
	endwhile; endif;
	
	//Related
	$related_dest = get_field('listing-related-destinations');
	$related_events = get_field('listing-related-events');

	//Tripadvisor
	$listing_trip = get_field('listing-tripadvisor');
	$listing_rate = get_field('listing-rating');

	if ($listing_rate == '5'): $listing_rate = '5.0'; endif;
	if ($listing_rate == '4'): $listing_rate = '4.0'; endif;
	if ($listing_rate == '3'): $listing_rate = '3.0'; endif;
	if ($listing_rate == '2'): $listing_rate = '2.0'; endif;
	if ($listing_rate == '1'): $listing_rate = '1.0'; endif;
	if ($listing_rate == '0'): $listing_rate = '0.0'; endif;

	$listing_ta_img = 'http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/'.$listing_rate.'-'.$listing_trip.'-5.png';

	//Show Category
	$listing_string ='';
	$listing_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );
	foreach($listing_cats as $cat){ 
		$listing_category = $cat->name; 
		$listing_cat_slug = $cat->slug; 
		$listing_string .= $cat->name . ", ";
		$categories = rtrim($listing_string, ", ");

		$listing_string ='';
		$listing_string .= $cat->name . " ";
		$cat_cats = strtolower($listing_string);
	break; 
	}

	//Show Region
	$region_string ='';
	$listing_region =  wp_get_post_terms(get_the_ID(), 'region', array('orderby' => 'term_order' ) );
	foreach($listing_region as $reg){ 
		$listing_region = $reg->name; 
		$listing_region_slug = $reg->slug; 
		$region_string .= $reg->name . ", ";
		$regions = rtrim($region_string, ", ");

		$region_string ='';
		$region_string .= $reg->name . " ";
		$region_cats = strtolower($region_string);
	break; 
	}

	//Show City
	$city_string ='';
	$listing_city =  wp_get_post_terms(get_the_ID(), 'city', array('orderby' => 'term_order' ) );
	foreach($listing_city as $cit){ 
		$listing_city_name = $cit->name; 
		$listing_city_slug = $cit->slug; 
		$city_string .= $cit->name . ", ";
		$cities = rtrim($city_string, ", ");

		$city_string ='';
		$city_string .= $cit->name . " ";
		$city_cats = strtolower($city_string);
	break; 
	}


	endwhile; endif;
?>

<script type="text/javascript">
        var map;
        
        function initMap() {                            
            var latitude = <?php echo $dest_lat; ?>; // YOUR LATITUDE VALUE
            var longitude = <?php echo $dest_long; ?>; // YOUR LONGITUDE VALUE
            
            var myLatLng = {lat: latitude, lng: longitude};
            
            map = new google.maps.Map(document.getElementById('map'), {
              center: myLatLng,
              zoom: 10,
			  scaleControl: false,
				  mapTypeControl: false,
				  zoomControl: true,
				  zoomControlOptions: {
					  position: google.maps.ControlPosition.LEFT_TOP
				  },
			  styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#040000"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#ffffff"
      },
      {
        "weight": 5.5
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#474c55"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#a30c33"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#474c55"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#d7eb9c"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#093266"
      }
    ]
  },
  {
    "featureType": "poi.school",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#bfbfbf"
      }
    ]
  },
  {
    "featureType": "poi.sports_complex",
    "stylers": [
      {
        "color": "#474c55"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.icon",
    "stylers": [
      {
        "saturation": 75
      },
      {
        "lightness": 55
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#75cede"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
            });
			
			var image = {
				url: '<?php echo get_template_directory_uri(); ?>/library/images/marker.png',
				scaledSize: new google.maps.Size(50, 80), // scaled size
			  };

                    
            var marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
			  icon: image,
            }); 
			
			 var contentString = "<div class='map-popup relative'><img src='<?php echo $dest_image_sm; ?>' width='150' height='150' style='padding-right: 15px; padding-bottom: 15px;' align='left'><div class='head-18'><?php echo $dest_title; ?></div><div class='font-12'><?php echo strip_tags($dest_street); ?></div><div class='padtop'><a href='https://www.google.com/maps/dir//<?php echo $dest_title; ?>,<?php echo strip_tags($dest_address); ?>, USA/' target='_blank'><button class='small red-back'><i class='fas fa-map-signs'></i> Get directions </button></a></div>";
			
			var infowindow = new google.maps.InfoWindow({
				content: contentString,
			  });


		  marker.addListener('click', function() {
			infowindow.open(map, marker);
		  });
        }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN2NfzOkaKE98ANqRWALhlA0xQdNixK9Q&callback=initMap" async defer></script>

<div id="page-head" class="relative">
			 
		 <div class="main-image <?php if ($img_short) :?> small <?php endif; ?> destination" style="background-image: url('<?php echo $dest_image; ?>')">
			<section class="full">	
				<div class="inner wide">
					
					<div class="main-image-inner">
						
						<section id="google-map" class="full nopadding test">
							<div id="map"></div>
						</section>
						

						
						
						<div id="destination-title-wrap">
							
						<div data-match-height="title" class="equal col-8 desk-7 tab-12">
							<div id="breadcrumbs">
							<span class="tab-hide"><a href="/">Home</a> <i class="fas fa-angle-right orange"></i></span>
							<a href="/regions/<?php echo $listing_region_slug; ?>/"><?php echo $listing_region; ?></a> <i class="fas fa-angle-right orange"></i>
							<a href="/regions/<?php echo $listing_region_slug; ?>/<?php echo $listing_city_slug; ?>/"><?php echo $listing_city_name; ?></a> <i class="fas fa-angle-right orange"></i>
							<a href="/things-to-do/<?php echo $listing_cat_slug; ?>/"><?php echo $listing_category; ?></a> 
						</div>
							<h1 class="head-72 handy white"><?php echo $dest_title; ?></h1>
							<h2 class="head-24 light white"><?php echo $listing_category ;?> in <?php echo $listing_city_name; ?>, Ohio</h2>
						</div>
							
						<div data-match-height="title" class="equal col-4 desk-5 tab-12">
						<div id="destination-buttons">
						<div class="col-3 center">
							<a href="">
								<div class="circle-icon">
								<span class="fa-layers fa-fw">
									<i class="fas fa-circle red"></i>
									<i class="fa-inverse fas fa-link" data-fa-transform="shrink-6"></i>
								</span>
								</div>
								<span class="white"> Website </span>
							</a>	
						</div>
						<div class="col-3 center">
							<a href="">
								<div class="circle-icon">
								<span class="fa-layers fa-fw">
									<i class="fas fa-circle orange"></i>
									<i class="fa-inverse fas fa-phone" data-fa-transform="shrink-6"></i>
								</span>
								</div>
								<span class="white"> Call </span>
							</a>	
						</div>
						<div class="col-3 center">
							<a id="show-map">
								<div class="circle-icon">
								<span class="fa-layers fa-fw">
									<i class="fas fa-circle green"></i>
									<i class="fa-inverse fas fa-map-signs" data-fa-transform="shrink-6"></i>
								</span>
								</div>
								<span class="white text"> Map </span>
							</a>	
						</div>
						<div class="col-3 center">
							<a href="#details">
								<div class="circle-icon">
								<span class="fa-layers fa-fw">
									<i class="fas fa-circle blue"></i>
									<i class="fa-inverse fas fa-angle-down" data-fa-transform="shrink-6"></i>
								</span>
								</div>
								<span class="white"> More </span>
							</a>	
						</div>
						<div class="clear"></div>
						</div>
						</div>
						
						<div class="clear"></div>
						</div>
					</div>
					
					
					
				</div>
			</section>
		</div>
</div>
			
</div>

<div id="content" class="nopadding">

	<section class="full nopadding">
		<aside id="details" class="red-back white texture-dark pad-30 relative equal" data-match-height="destination">

			<div class="head-48 handy center">details</div>
			
			<div class="sidebar-section">
				<div class="sidebar-label"><i class="fas fa-map-marker-alt"></i> Address</div>
				<div class="padbottom"><?php echo $dest_address ; ?></div>
				<?php if ($dest_directions): ?>
				<div class="sidebar-sub-label nopadding bold"> Rates From: </div>
				<div><?php echo $dest_directions; ?></div>
				<?php endif; ?>
				<a href="https://www.google.com/maps/dir//<?php the_title(); ?>,<?php echo strip_tags($dest_address); ?>, USA/"><button class="medium red-back"><i class="fas fa-map-signs"></i> Get directions</button></a>
			</div>
			
			<?php if ($dest_phone) : ?>
			<div class="sidebar-section">
				<div class="sidebar-label"><i class="fas fa-phone"></i> Phone Number</div>
				<p><?php echo $dest_phone ; ?></p>
			</div>
			<?php endif; ?>
			
			<?php if ($dest_amenities_list) : ?>
			<div class="sidebar-section">
				<div class="sidebar-label"><i class="fas fa-check"></i> Features/Amenities</div>
				<p><?php echo $dest_amenities_list ; ?></p>
			</div>
			<?php endif; ?>
			
			<?php if( have_rows('listing-hours') ): ?>
			<div class="sidebar-section">
				<div class="sidebar-label"><i class="fas fa-clock"></i> Hours</div>
				
				
				<ul class="clean">
				<?php while( have_rows('listing-hours') ): the_row();
						$hours_day = get_sub_field('week-day');
						$hours_open = get_sub_field('time-open');
						$hours_close = get_sub_field('time-close'); ?>
					
				<li>• <strong><?php echo $hours_day ; ?></strong>: <?php echo $hours_open; ?> to <?php echo $hours_close; ?></li>
					
		        <?php endwhile; ?>
				</ul>
		
			</div>
			<?php endif; ?>
			
			<?php if( have_rows('listing-rates') ): ?>
			<div class="sidebar-section">
				<div class="sidebar-label"><i class="fas fa-money-bill-alt"></i> Pricing</div>
				
				<?php if ($dest_rates_from): ?>
				<div class="sidebar-sub-label nopadding bold"> Rates From: </div>
				<div class="head-48 gold">$<?php echo $dest_rates_from; ?></div>
				<?php endif; ?>
				
				<ul class="clean">
				<?php while( have_rows('listing-rates') ): the_row();
						$rates_name = get_sub_field('name');
						$rates_avg = get_sub_field('average-rate');
						$rates_low = get_sub_field('low-season');
						$rates_high = get_sub_field('high-season');
						$rates_freq = get_sub_field('frequency'); 
					
						if ($rates_avg == '0.00'):
							$rates_avg = 'Free';
						endif;
					?>
					
				<li>• <strong><?php echo $rates_name ; ?></strong>: <?php echo $rates_avg; ?> <?php if ($rates_low || $rates_high) : ?>(<?php echo $rates_low; ?> <?php if ($rates_high) : ?> to <?php echo $rates_high ; endif; ?> <?php echo $rates_freq; ?>) <?php endif; ?></li>
					
		        <?php endwhile; ?>
				</ul>
		
			</div>
			<?php endif; ?>
			
			<div class="sidebar-section">
			<div class="sidebar-label"><i class="fas fa-share-square"></i> Share This Listing</div>
			<div class="share-wrap nopadding nomargin">
				  <div class="a2a_kit a2a_kit_size_48 a2a_default_style">
						<a class="a2a_button_facebook blue"></a>
						<a class="a2a_button_twitter foam"></a>
						<a class="a2a_button_pinterest white"></a>
						<a class="a2a_button_email green"></a>
						<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
				  </div>
			</div>
			</div>
		
		</aside>
		<div class="inner wsidebar margin-top-30 equal" data-match-height="destination">
			<div id="about-this-destination" class="box relative">
				<div class="circle-icon overlap">
				<span class="fa-layers fa-fw">
					<i class="fas fa-circle gray"></i>
					<i class="fa-inverse fas fa-info" data-fa-transform="shrink-6"></i>
				</span>
				</div>
				<h2 class="head-54 handy center">About <?php echo $dest_title; ?></h2>
				<?php echo $dest_text; ?>
			</div>
			<div id="about-this-destination" class="box relative margin-top-60">
				<div class="circle-icon overlap">
				<span class="fa-layers fa-fw">
					<i class="fas fa-circle gray"></i>
					<i class="fa-inverse fas fa-map-signs" data-fa-transform="shrink-6"></i>
				</span>
				</div>
				<h2 class="head-54 handy center">what's nearby?</h2>
				<?php echo $dest_text; ?>
			</div>
		</div>
	</section> <!-- end article section -->

	<footer>

	</footer>

</main>

</div>

</div>

<?php get_footer(); ?>
