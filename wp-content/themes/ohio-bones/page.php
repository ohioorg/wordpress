<?php get_header(); ?>

<?php 

	//Grabs page_id for current page
	global $post;
	$page_id = get_queried_object_id();
	$parent_page = $post->post_parent;
	$parent_page_name = get_the_title($parent_page);
	$parent_page_slug = get_the_permalink($parent_page);

	//Query options
	 $page_args = array( 
	 'page_id' => $page_id,
	 'post_type' => 'page',
	 'posts_per_page'=>1, 
	 );

	$page_query = new WP_Query( $page_args );

	if ($page_query->have_posts() ) : while ( $page_query->have_posts() ) : $page_query->the_post();
	
	//Main Page Attributes
	$title = get_the_title();
	$headline = get_field('page-headline');
	$subhead = get_field('page-subtitle');
	$summary = get_field('page-summary');
	$body = get_field('page-body');
	$video = get_field('page-video');

	if ($headline) :
		$title = $headline;
	endif; 

	$related_content = get_field('page-related');
	$page_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );

	foreach($page_cats as $page_cat):
		$page_cat_ID = $page_cat->ID; 
	endforeach; 

	endwhile; endif;
?>

<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

<div id="content" class="nopadding">

	<article id="page-<?php the_ID(); ?>" class="page-main" role="article">

			<section class="cf nopadding" itemprop="articleBody">
				<div class="inner page-inner">
					
					<!-- Breadcrumbs -->
					<div id="breadcrumbs" class="red noshadow center">
						<a href="<?php echo get_site_url(); ?>"><?php echo get_bloginfo(); ?></a> <i class="fas fa-angle-right orange"></i>
						<?php if ($parent_page) : ?><a href="<?php echo get_site_url(); ?>/<?php echo $parent_page_slug; ?>"><?php echo $parent_page_name; ?></a> <i class="fas fa-angle-right orange"></i> <?php endif; ?>
						<a href="<?php echo get_the_permalink(); ?>"><?php echo $title; ?></a>
					</div>
					
					<!-- Page Title -->
					<h1 class="head-72 handy center"><?php echo $title; ?></h1>
					<?php if($subhead) :?><h2 class="head-24 center"><?php echo $subhead; ?></h2> <?php endif; ?>
					<!-- Page Content -->
					<div id="page-content" class="padtop">
						<?php the_content(); ?>
					</div>
					
				</div>
			</section>

	</article>

</div>


<?php get_footer(); ?>
