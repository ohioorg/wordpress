<?php get_header(); ?>

<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 

//Search Query Variable
$search = (isset($_REQUEST['search']) && $_REQUEST['search']) ? $_REQUEST['search'] : false;
$search = get_query_var('s');
$search_term = get_search_query();

//Year Query Variable
$year = get_query_var('year');
$monthnum = get_query_var('monthnum');
$month_name = $GLOBALS['wp_locale']->get_month($monthnum);
$archive_name = $month_name.' '.$year;

//Category Query Variable
$category = (is_category()) ? get_category(get_query_var('cat'))->slug : false;
$category_pull = get_term_by('slug', $category, 'category'); 
$category_name = $category_pull->name;

//Region Query Variable
$region = (is_tax()) ? get_query_var('region') : false;
$region_pull = get_term_by('slug', $region, 'region'); 
$region_name = $region_pull->name;

//City Query Variable
$city = (is_tax()) ? get_query_var('city') : false;
$city_pull = get_term_by('slug', $city, 'city'); 
$city_name = $city_pull->name;

//Season Query Variable
$season = (is_tax()) ? get_query_var('season') : false;
$season_pull = get_term_by('slug', $season, 'season'); 
$season_name = $season_pull->name;

if($season == '' && $city == '' && $region == '' && $city == '' && $year == '' && $search == '' ):
$allempty = 'true';
endif;


//Grabs page_id for current page
$page_id = get_queried_object_id();

//Query for Main Image & Page Info
 $page_args = array( 
 'post_type' => 'page',
 'posts_per_page'=>1,
 'p' => 5,
 );

$page_query = new WP_Query( $page_args );

if ($page_query->have_posts() ) : while ( $page_query->have_posts() ) : $page_query->the_post();

//Main Page Attributes
$title = get_the_title();
$headline = get_field('page-headline');
$subhead = get_field('page-subtitle');

?>

<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

<?php if(have_rows('page-image')): while(have_rows('page-image')): the_row(); ?>
<div id="page-head">
				
<?php 
$img_src = get_sub_field('image');
$img = $img_src['url'];
$img_word = get_sub_field('title');
$img_text = get_sub_field('text');
$img_caption = get_sub_field('authenticator'); ?>

<div class="main-image medium" style="background-image: url('<?php echo $img; ?>')">
	<section class="full">	
			<div class="main-image-inner">
			   <div class="main-image-inner-content">
				   		
						
				   
				  			<?php if(!($search) && !($year) && !($category) && !($city) && !($region) && !($season)) : ?>
								<h1 class="head-96 handy shadow white"><?php echo $img_word; ?></h1>
								<h2 class="head-24 light shadow white italic pad-5"><?php echo $img_text ;?></h2>
							<?php endif; ?>
								
							<?php if($search) : ?>
				   				<h1 class="head-96 handy shadow white">" <?php echo $search;?> "</h1>
								<h2 class="head-24 light shadow white italic pad-5">Ohio Stories for the term "<?php echo $search;?>"</h2>
							<?php endif; ?>
								
							<?php if($year) : ?>
								<h1 class="head-96 handy shadow white"><?php echo $archive_name; ?></h1>
								<h2 class="head-24 light shadow white italic pad-5">Showing Ohio stories in <?php echo $archive_name; ?></h2>
							<?php endif; ?>
								
							<?php if( $category || $region || $city || $season ) : ?>
								<h1 class="head-96 handy shadow white"><?php echo $category_name; ?><?php echo $region_name; ?><?php echo $city_name; ?><?php echo $season_name; ?></h1>
								<h2 class="head-24 light shadow white italic pad-5">Showing Ohio stories in <?php echo $category_name; ?><?php echo $region_name; ?><?php echo $city_name; ?><?php echo $season_name; ?></h2>
							<?php endif; ?>
					
				</div>
				<?php if($img_caption): ?>
				<div class="main-image-caption">
					<?php echo $img_caption ;?>
				</div>
				<?php endif; ?>
			</div>
	</section>
</div>
	
</div>
<?php endwhile; endif; ?>
<?php endwhile; endif; ?>


<div id="content" class="nopadding">

<section class="full nopadding">
	
					<div class="inner wsidebar">
						
						<div class="head-54 center"> </div>
		
						<?php
						$mainblogs_args = array(
							'post_type' => 'post',
							'status' => 'publish',
							'paged' => $paged,
			 				'posts_per_page'=> 16,
						 );

						if($search):
							$mainblogs_args = array_merge($mainblogs_args, array('s' => $search));
						endif;
						
						if($category):
							$mainblogs_args = array_merge($mainblogs_args, array('category_name' => $category));
						endif;

						if($region):
							$mainblogs_args = array_merge($mainblogs_args, array('tax_query' => array( array('taxonomy' => 'region', 'field' => 'slug', 'terms' => $region))));
						endif;
						
						if($city):
							$mainblogs_args = array_merge($mainblogs_args, array('tax_query' => array( array('taxonomy' => 'city', 'field' => 'slug', 'terms' => $city))));
						endif;
						
						if($season):
							$mainblogs_args = array_merge($mainblogs_args, array('tax_query' => array( array('taxonomy' => 'season', 'field' => 'slug', 'terms' => $season))));
						endif;

						if($year):
							$mainblogs_args = array_merge($mainblogs_args, array('date_query' => array(array('year' => $year, 'month' => $monthnum))));
						endif;

						$mainblogs_query = new WP_Query($mainblogs_args);
						?>

						<?php if ($mainblogs_query->have_posts()) : ?>
				
							<div id="blog-results" class="margin-bottom-30 black">

								<nav class="prev-next-posts relative">
										<?php if ($paged > 1) :?>
									    <div class="prev-posts-link absolute abstop absleft">
											<?php echo get_next_posts_link( '
											<div class="circle-icon">
											<span class="fa-layers fa-fw">
												<i class="fas fa-circle orange"></i>
												<i class="fa-inverse fas fa-angle-left" data-fa-transform="shrink-6"></i>
											</span>
											</div>', 
											$mainblogs_query->max_num_pages ); ?>
										</div>
										<?php endif; ?>
										<div class="center pad-10 wide"> 
											<div class="head-24">
											<?php if(!($search) && !($year) && !($category) && !($city) && !($region) && !($season)) : ?>
											Most recent articles
											<?php else: ?>
											<?php echo $mainblogs_query->found_posts; ?> total articles
											<?php endif; ?>
											</div>
											 <div>Showing Page <?php echo $paged; ?> of <?php echo $mainblogs_query->max_num_pages; ?></div>
									    </div>
										
										<?php if ($mainblogs_query->max_num_pages > 1) :?>
										<div class="next-posts-link absolute abstop absright">
											<?php echo get_next_posts_link( '
											<div class="circle-icon">
											<span class="fa-layers fa-fw">
												<i class="fas fa-circle orange"></i>
												<i class="fa-inverse fas fa-angle-right" data-fa-transform="shrink-6"></i>
											</span>
											</div>', 
											$mainblogs_query->max_num_pages ); ?>
										</div>
										<?php endif; ?>

									<div class="clear"></div>
								</nav>
									
							</div>
						
						
						<?php while ( $mainblogs_query->have_posts() ) : $mainblogs_query->the_post(); 

							//Main Image & Caption
							if( have_rows('blog-image') ): while( have_rows('blog-image') ): the_row();

								$blog_image_pull = get_sub_field('image');
								$blog_image = $blog_image_pull['sizes']['large'];
								$blog_image_caption = get_sub_field('caption');
								$blog_image_url = get_sub_field('url');

								if (!($blog_image)):
									$blog_image = $blog_image_url;
								endif;

							endwhile; endif;
	
							//Headlines
							$blog_title = get_the_title();
							$blog_head = get_field('blog-headline');
							$blog_sub = strip_tags(get_field('blog-subtitle'));

							if ($blog_head) :
								$blog_title = $blog_head;
							endif;

							//Excerpt
							$blog_body = strip_tags(get_field('blog-body'));
							$blog_excerpt = substr($blog_body, 0, 255);
							$blog_link = get_the_permalink();
							
							if (!($blog_body)) :
								$blog_content = strip_tags(get_the_content());
								$blog_excerpt = substr($blog_content, 0, 255);
							endif;
						
							$shortcode_tags = array('VC_COLUMN_INNTER');
							$values = array_values( $shortcode_tags );
							$exclude_codes  = implode( '|', $values );

							// strip all shortcodes except $exclude_codes and keep all content
							$blog_excerpt = preg_replace( "~(?:\[/?)(?!(?:$exclude_codes))[^/\]]+/?\]~s", '', $blog_excerpt );
						
							//Show Category
							$cat_string ='';
							$blog_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );
							foreach($blog_cats as $cat){ 
								$blog_category = $cat->name; 
								$blog_cat_slug = $cat->slug; 
								$cat_string .= $cat->name . ", ";
								$categories = rtrim($cat_string, ", ");
							$i++;
							if($i==3) break;
							}
						
							//Show Region
							$region_string ='';
							$blog_region =  wp_get_post_terms(get_the_ID(), 'region', array('orderby' => 'term_order' ) );
							foreach($blog_region as $reg){ 
								$blog_region = $reg->name; 
								$blog_region_slug = $reg->slug; 
								$region_string .= $reg->name . ", ";
								$regions = rtrim($region_string, ", ");
							}
						
							//Show Region
							$season_string ='';
							$blog_season =  wp_get_post_terms(get_the_ID(), 'season', array('orderby' => 'term_order' ) );
							foreach($blog_season as $sea){ 
								$blog_season = $sea->name; 
								$blog_season_slug = $sea->slug; 
								$season_string .= $sea->name . ", ";
								$seasons = rtrim($season_string, ", ");
							}

						?>

						<div data-match-height="listing" class="listing blog white-back relative col-6 desk-12 tab-6 phone-12">
							
							<a class="link-wrap" href="<?php echo $blog_link; ?>">
								<div class="img-wrap">
									<img src="<?php echo $blog_image; ?>" alt="<?php echo $blog_title;?>"  title="<?php echo $blog_title;?>">
								</div>
							</a>	

								<div class="text relative">
									<div class="circle-icon overlap">
										<div class="red-back circle">
											<i class="i-<?php echo $blog_region_slug; ?> z-index white" style="z-index: 100000;" data-fa-transform="shrink-6"></i>
										</div>
									</div>
									<a class="link-wrap" href="<?php echo $blog_link; ?>">
									<div class="inner">
									<div class="head-24"><?php echo $blog_title;?></div>
									<p class="font-12"><?php echo $blog_excerpt; ?> &hellip;</p>
									<div class="details">
										<p class="font-12 bold"><i class="fas fa-map-marker-alt foam"></i> <?php echo $regions ;?></p>
										<p class="font-12 bold"><i class="fas fa-check orange"></i> <?php echo $categories ;?></p>
										<p class="font-12 bold"><i class="fas fa-leaf green"></i> <?php echo $seasons ;?></p>
									</div>
									</div>
									</a>
								</div>

						</div>

					<?php endwhile;
					endif; ?>
					<div class="clear"></div>
						
					<nav class="prev-next-posts relative">
							<?php if ($paged > 1) :?>
							<div class="prev-posts-link absolute abstop absleft">
								<?php echo get_next_posts_link( '
								<div class="circle-icon">
								<span class="fa-layers fa-fw">
									<i class="fas fa-circle orange"></i>
									<i class="fa-inverse fas fa-angle-left" data-fa-transform="shrink-6"></i>
								</span>
								</div>', 
								$mainblogs_query->max_num_pages ); ?>
							</div>
							<?php endif; ?>
							<div class="center pad-10 wide"> 
								<div class="head-24">
								<?php if(!($search) && !($year) && !($category) && !($city) && !($region) && !($season)) : ?>
								Most recent articles
								<?php else: ?>
								<?php echo $mainblogs_query->found_posts; ?> total articles
								<?php endif; ?>
								</div>
								 <div>Showing Page <?php echo $paged; ?> of <?php echo $mainblogs_query->max_num_pages; ?></div>
							</div>

							<?php if ($mainblogs_query->max_num_pages > 1) :?>
							<div class="next-posts-link absolute abstop absright">
								<?php echo get_next_posts_link( '
								<div class="circle-icon">
								<span class="fa-layers fa-fw">
									<i class="fas fa-circle orange"></i>
									<i class="fa-inverse fas fa-angle-right" data-fa-transform="shrink-6"></i>
								</span>
								</div>', 
								$mainblogs_query->max_num_pages ); ?>
							</div>
							<?php endif; ?>

						<div class="clear"></div>
					</nav>
						
				</div>
	
				<aside id="sidebar" class="red-back white texture-dark pad-30 relative">
					<div class="white center padtop">
					<div class="head-42 handy">
						find stories
					</div>
					<p>
						Find what you're looking for by searching the thousands of great articles full of Ohio travel inspiration:
					</p>
					</div>
					<form role="search" method="post" class="search-form" action="<?php echo get_site_url(); ?>/blog/?s=<?php echo $search; ?>">
						<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="hidden" name="post_type" value="post" />
							<input type="search" class="search-field" placeholder="Search …" value="<?=($search) ? $search : '';?>" name="search">
						</label>
						<input type="submit" class="search-submit" value="Search">
					</form>
					
					<div id="categories" class="sidebar-section">
					<div class="sidebar-label">
						<i class="fas fa-check"></i> Categories
					</div>
					<ul class="clean">
					<?php wp_list_categories(array (
						taxonomy => 'category',
						show_count => 0,
						title_li => '',
					)); ?>
					</ul>
					</div>
					
					<div id="regions" class="sidebar-section">
					<div class="sidebar-label">
						<i class="fas fa-map-marker-alt"></i> Location
					</div>
					<ul class="clean">
					<?php wp_list_categories(array (
						taxonomy => 'region',
						show_count => 0,
						title_li => '',
					)); ?>
					</ul>
					</div>
					
					<div id="cities" class="sidebar-section">
					<div class="sidebar-label">
						<i class="i-ohio"></i> Location
					</div>
					<ul class="clean">
					<?php wp_list_categories(array (
						taxonomy => 'city',
						show_count => 0,
						title_li => ''
					)); ?>
					</ul>
					</div>
					
					<div id="seasons" class="sidebar-section">
					<div class="sidebar-label">
						<i class="fas fa-sun"></i> Seasons
					</div>
					<ul class="clean">
					<?php wp_list_categories(array (
						taxonomy => 'season',
						show_count => 0,
						title_li => ''
					)); ?>
					</ul>
					</div>
					
					<div id="seasons" class="sidebar-section">
					<div class="sidebar-label">
						<i class="fas fa-sun"></i> Seasons
					</div>
					<ul class="clean">
					<?php wp_get_archives(array (
						type => 'monthly',
						show_count => 0,
						title_li => ''
					)); ?>
					</ul>
					</div>

				</aside>

</section>

</div>

</main>


<?php get_footer(); ?>
