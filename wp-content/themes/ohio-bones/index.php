<?php get_header(); ?>

<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 

//Search Query Variable
$search = (isset($_REQUEST['search']) && $_REQUEST['search']) ? $_REQUEST['search'] : false;
$search = get_query_var('s');
$search_term = get_search_query();

//Year Query Variable
$year = get_query_var('year');
$monthnum = get_query_var('monthnum');
$month_name = $GLOBALS['wp_locale']->get_month($monthnum);
$archive_name = $month_name.' '.$year;

//Category Query Variable
$category = (is_category()) ? get_category(get_query_var('cat'))->slug : false;
$category_pull = get_term_by('slug', $category, 'category'); 
$category_name = $category_pull->name;

//Region Query Variable
$region = (is_tax()) ? get_query_var('region') : false;
$region_pull = get_term_by('slug', $region, 'region'); 
$region_name = $region_pull->name;

//City Query Variable
$city = (is_tax()) ? get_query_var('city') : false;
$city_pull = get_term_by('slug', $city, 'city'); 
$city_name = $city_pull->name.', Ohio';

//Season Query Variable
$season = (is_tax()) ? get_query_var('season') : false;
$season_pull = get_term_by('slug', $season, 'season'); 
$season_name = $season_pull->name;

if($season == '' && $city == '' && $region == '' && $city == '' && $year == '' && $search == '' ):
$allempty = 'true';
endif;


//Grabs page_id for current page
$page_id = get_queried_object_id();

//Query for Main Image & Page Info
 $page_args = array( 
 'post_type' => 'page',
 'posts_per_page'=>1,
 'p' => 5,
 );

$page_query = new WP_Query( $page_args );

if ($page_query->have_posts() ) : while ( $page_query->have_posts() ) : $page_query->the_post();

//Main Page Attributes
$title = get_the_title();
$headline = get_field('page-headline');
$subhead = get_field('page-subtitle');

?>

<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

<?php if(have_rows('page-image')): while(have_rows('page-image')): the_row(); ?>
<div id="page-head">
				
<?php 
$img_src = get_sub_field('image');
$img = $img_src['url'];
$img_word = get_sub_field('title');
$img_text = get_sub_field('text');
$img_caption = get_sub_field('authenticator'); ?>

<div class="main-image medium" style="background-image: url('<?php echo $img; ?>')">
	<section class="full">	
			<div class="main-image-inner">
			   <div class="main-image-inner-content">
				   		
						
				   
				  			<?php if(!($search) && !($year) && !($category) && !($city) && !($region) && !($season)) : ?>
								<h1 class="head-96 handy shadow white"><?php echo $img_word; ?></h1>
								<h2 class="head-24 light shadow white italic"><?php echo $img_text ;?></h2>
							<?php endif; ?>
								
							<?php if($search) : ?>
				   				<h1 class="head-96 handy shadow white">" <?php echo $search;?> "</h1>
								<h2 class="head-24 light shadow white italic">Viewing most recent articles matching "<?php echo $search;?>"</h2>
							<?php endif; ?>
								
							<?php if($year) : ?>
								<h1 class="head-96 handy shadow white"><?php echo $archive_name; ?></h1>
								<h2 class="head-24 light shadow white italic">Viewing articles from <?php echo $archive_name; ?></h2>
							<?php endif; ?>
				   
				   			<?php if( $city ) : ?>
								<h1 class="head-96 handy shadow white"><?php echo $city_name; ?></h1>
								<h2 class="head-24 light shadow white italic">Viewing the most recent articles about <?php echo $city_name; ?></h2>
							<?php endif; ?>
				   
				   			<?php if( $region ) : ?>
								<h1 class="head-96 handy shadow white"><?php echo $region_name; ?></h1>
								<h2 class="head-24 light shadow white italic">Viewing the most recent articles about <?php echo $region_name; ?></h2>
							<?php endif; ?>
								
							<?php if( $category || $region || $season ) : ?>
								<h1 class="head-96 handy shadow white"><?php echo $category_name; ?><?php echo $season_name; ?></h1>
								<h2 class="head-24 light shadow white italic">Viewing the most recent articles about <?php echo $category_name; ?><?php echo $season_name; ?> in Ohio</h2>
							<?php endif; ?>
					
				</div>
				<?php if($img_caption): ?>
				<div class="main-image-caption">
					<?php echo $img_caption ;?>
				</div>
				<?php endif; ?>
			</div>
	</section>
</div>
	
</div>
<?php endwhile; endif; ?>
<?php endwhile; endif; ?>


<div id="content" class="nopadding">

<section class="full nopadding">
	
					<div class="inner wsidebar nopadding nomargin">
						<div class="grid">
							
							<?php
						$mainblogs_args = array(
							'post_type' => 'post',
							'status' => 'publish',
							'paged' => $paged,
			 				'posts_per_page'=> 24,
						 );
						
						//Add Search Variable if Search Query
						if($search):
							$mainblogs_args = array_merge($mainblogs_args, array('s' => $search));
						endif;
						
						//Add Category Variables
						if($category):
							$mainblogs_args = array_merge($mainblogs_args, array('category_name' => $category));
						endif;
						
						//Add Region Variables
						if($region):
							$mainblogs_args = array_merge($mainblogs_args, array('tax_query' => array( array('taxonomy' => 'region', 'field' => 'slug', 'terms' => $region))));
						endif;
						
						//Add City Variables
						if($city):
							$mainblogs_args = array_merge($mainblogs_args, array('tax_query' => array( array('taxonomy' => 'city', 'field' => 'slug', 'terms' => $city))));
						endif;
						
						//Add Season Variables
						if($season):
							$mainblogs_args = array_merge($mainblogs_args, array('tax_query' => array( array('taxonomy' => 'season', 'field' => 'slug', 'terms' => $season))));
						endif;
						
						//Add Year Variables
						if($year):
							$mainblogs_args = array_merge($mainblogs_args, array('date_query' => array(array('year' => $year, 'month' => $monthnum))));
						endif;

						$mainblogs_query = new WP_Query($mainblogs_args);
						?>

						<?php if ($mainblogs_query->have_posts()) : ?>
							
							
						<nav class="prev-next-posts relative">
									<?php if ($paged > 1) :?>
									<div class="prev-posts-link absolute abstop absleft">
										<?php echo get_next_posts_link( '
										<div class="circle-icon">
										<span class="fa-layers fa-fw">
											<i class="fas fa-circle orange"></i>
											<i class="fa-inverse fas fa-angle-left" data-fa-transform="shrink-6"></i>
										</span>
										</div>', 
										$mainblogs_query->max_num_pages ); ?>
									</div>
									<?php endif; ?>
									<div class="center wide"> 
										<h3 class="head-54 handy center">
										<?php if(!($search) && !($year) && !($category) && !($city) && !($region) && !($season)) : ?>
										Most recent articles
										<?php else: ?>
										<?php echo $mainblogs_query->found_posts; ?> total articles
										<?php endif; ?>
										</h3>
										 <div>Showing Page <?php echo $paged; ?> of <?php echo $mainblogs_query->max_num_pages; ?></div>
									</div>

									<?php if ($mainblogs_query->max_num_pages > 1) :?>
									<div class="next-posts-link absolute abstop absright">
										<?php echo get_next_posts_link( '
										<div class="circle-icon">
										<span class="fa-layers fa-fw">
											<i class="fas fa-circle orange"></i>
											<i class="fa-inverse fas fa-angle-right" data-fa-transform="shrink-6"></i>
										</span>
										</div>', 
										$mainblogs_query->max_num_pages ); ?>
									</div>
									<?php endif; ?>

								<div class="clear"></div>
							</nav>
		
				
						<?php while ( $mainblogs_query->have_posts() ) : $mainblogs_query->the_post(); 

							//Main Image & Caption
							if( have_rows('blog-image') ): while( have_rows('blog-image') ): the_row();

								$blog_image_pull = get_sub_field('image');
								$blog_image = $blog_image_pull['sizes']['medium'];
								$blog_image_caption = get_sub_field('caption');
								$blog_image_url = get_sub_field('url');

								if (!($blog_image)):
									$blog_image = $blog_image_url;
								endif;

							endwhile; endif;
	
							//Headlines
							$blog_title = get_the_title();
							$blog_head = get_field('blog-headline');
							$blog_sub = strip_tags(get_field('blog-subtitle'));

							if ($blog_head) :
								$blog_title = $blog_head;
							endif;

							//Excerpt
							$blog_body = strip_tags(get_field('blog-body'));
							$blog_excerpt = substr($blog_body, 0, 155);
							$blog_link = get_the_permalink();
							
							if (!($blog_body)) :
								$blog_content = strip_tags(get_the_content());
								$blog_excerpt = substr($blog_content, 0, 155);
							endif;
						
							$shortcode_tags = array('VC_COLUMN_INNTER');
							$values = array_values( $shortcode_tags );
							$exclude_codes  = implode( '|', $values );

							// strip all shortcodes except $exclude_codes and keep all content
							$blog_excerpt = preg_replace( "~(?:\[/?)(?!(?:$exclude_codes))[^/\]]+/?\]~s", '', $blog_excerpt );
						
							//Show Category
							$cat_string ='';
							$blog_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );
							foreach($blog_cats as $cat){ 
								$blog_category = $cat->name; 
								$blog_cat_slug = $cat->slug; 
								$cat_string .= $cat->name . ", ";
								$categories = rtrim($cat_string, ", ");
							$i++;
							if($i==3) break;
							}
						
							//Show Region
							$region_string ='';
							$blog_region =  wp_get_post_terms(get_the_ID(), 'region', array('orderby' => 'term_order' ) );
							foreach($blog_region as $reg){ 
								$blog_region = $reg->name; 
								$blog_region_slug = $reg->slug; 
								$region_string .= $reg->name . ", ";
								$regions = rtrim($region_string, ", ");
							}
						
							//Show Region
							$season_string ='';
							$blog_season =  wp_get_post_terms(get_the_ID(), 'season', array('orderby' => 'term_order' ) );
							foreach($blog_season as $sea){ 
								$blog_season = $sea->name; 
								$blog_season_slug = $sea->slug; 
								$season_string .= $sea->name . ", ";
								$seasons = rtrim($season_string, ", ");
							}

						?>

						<div class="col-4-full desk-6-full tab-6-full phone-12-full ">
							<div class="listing square relative">
								<a class="link-wrap" href="<?php echo $blog_link; ?>">
									<div class="img-wrap square ">
										<img src="<?php echo $blog_image; ?>" alt="<?php echo $blog_title;?>"  title="<?php echo $blog_title;?>">
										<div class="text relative z-index">
										<div class="read-more label-14"><i class="fas fa-plus white"></i> READ MORE</div>
										<div class="inner">
										<div class="head-28 white"><?php echo $blog_title;?></div>
										<div class="font-14 white"><?php echo $blog_excerpt; ?> &hellip;</div>
										<div class="clear"></div>
										</div>
										</div>
									</div>
								</a>	
							</div>
						</div>

					<?php endwhile;
					endif; ?>
					<div class="clear"></div>	
					<div class="pad-30">
					<nav class="prev-next-posts relative">
							<?php if ($paged > 1) :?>
							<div class="prev-posts-link absolute abstop absleft">
								<?php echo get_next_posts_link( '
								<div class="circle-icon">
								<span class="fa-layers fa-fw">
									<i class="fas fa-circle orange"></i>
									<i class="fa-inverse fas fa-angle-left" data-fa-transform="shrink-6"></i>
								</span>
								</div>', 
								$mainblogs_query->max_num_pages ); ?>
							</div>
							<?php endif; ?>
							<div class="center pad-10 wide"> 
								<div class="head-24">
								<?php if(!($search) && !($year) && !($category) && !($city) && !($region) && !($season)) : ?>
								Most recent articles
								<?php else: ?>
								<?php echo $mainblogs_query->found_posts; ?> total articles
								<?php endif; ?>
								</div>
								 <div>Showing Page <?php echo $paged; ?> of <?php echo $mainblogs_query->max_num_pages; ?></div>
							</div>

							<?php if ($mainblogs_query->max_num_pages > 1) :?>
							<div class="next-posts-link absolute abstop absright">
								<?php echo get_next_posts_link( '
								<div class="circle-icon">
								<span class="fa-layers fa-fw">
									<i class="fas fa-circle orange"></i>
									<i class="fa-inverse fas fa-angle-right" data-fa-transform="shrink-6"></i>
								</span>
								</div>', 
								$mainblogs_query->max_num_pages ); ?>
							</div>
							<?php endif; ?>

						<div class="clear"></div>
					</nav>
					</div>
					</div>
				</div>
	
				<?php include( locate_template( '/page-components/sidebars/blog.php', false, false ) ); ?>

</section>

</div>

</main>


<?php get_footer(); ?>
