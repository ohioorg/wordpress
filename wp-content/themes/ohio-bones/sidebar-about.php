		<aside class="col-3 desk-4 tab-5 phone-12">				
			<div id="sidebar" class="sidebar cf" role="complementary">

					<?php if ( is_active_sidebar( 'sidebar-about' ) ) : ?>

						<?php dynamic_sidebar( 'sidebar-about' ); ?>

					<?php else : ?>

						<?php
							/*
							 * This content shows up if there are no widgets defined in the backend.
							*/
						?>

						<div class="no-widgets">
							<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
						</div>

					<?php endif; ?>

				</div>
		</aside>
