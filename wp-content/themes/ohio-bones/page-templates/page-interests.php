<?php /* Template Name: Interests */ ?>

<?php get_header(); ?>

<?php 

	//Grabs page_id for current page
	global $post;
	$page_id = get_queried_object_id();
	$parent_page = $post->post_parent;
	$parent_page_name = get_the_title($parent_page);
	$parent_page_slug = get_the_permalink($parent_page);

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	//Query options
	 $page_args = array( 
	 'page_id' => $page_id,
	 'post_type' => 'page',
	 'posts_per_page'=>1, 
	 );

	$page_query = new WP_Query( $page_args );

	if ($page_query->have_posts() ) : while ( $page_query->have_posts() ) : $page_query->the_post();
	
	//Main Page Attributes
	$title = get_the_title();
	$headline = get_field('page-headline');
	$subhead = get_field('page-subtitle');
	$summary = get_field('page-summary');
	$body = get_field('page-body');
	$video = get_field('page-video');

	$related_content = get_field('page-related');

	$page_cats =  wp_get_post_terms($post->ID, 'category', array('orderby' => 'term_order' ) );

	$pull_string ='';

	foreach($page_cats as $page_cat):
		$page_cat_ID = $page_cat->term_id;
		$test_string .= $page_cat->name . ",";
		$pull_string .= $page_cat->term_id . ",";
		$listings_IDs = rtrim($pull_string, ",");
	endforeach; 


	
//SEO-rich Highlight Text
$higlight_img_pull = get_field('highlight-image');
$higlight_img = $higlight_img_pull['sizes']['large'];

$higlight_head = get_field('highlight-head');
$higlight_emo = get_field('highlight-emotion');
$higlight_text = get_field('highlight-text');
$higlight_promos = get_field('highlight-promos');
$higlight_place = get_field('highlight-place');

?>

<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">


<div id="page-head">
				
			<?php if(have_rows('page-image')): while(have_rows('page-image')): the_row(); ?>
				
			<?php 
			$img_src = get_sub_field('image');
			$img = $img_src['url'];
			$img_small = $img_src['sizes']['medium'];
			$img_link = get_sub_field('link');
			$img_word = get_sub_field('title');
			$img_text = get_sub_field('text');
			$img_caption = get_sub_field('authenticator');

			endwhile;
			endif; ?>
			
			<?php if ($paged == 1) : ?>
		     <div class="main-image" style="background-image: url('<?php echo $img; ?>')">
				<section class="full">	

						<div class="main-image-inner">
						    <div class="main-image-inner-content">
								<div data-match-height="title" class="equal col-8 desk-7 tab-12">
									<div id="breadcrumbs">
										<a href="<?php echo get_site_url(); ?>"><?php echo get_bloginfo(); ?></a> <i class="fas fa-angle-right orange"></i>
										<?php if ($parent_page) : ?><a href="<?php echo get_site_url(); ?>/<?php echo $parent_page_slug; ?>"><?php echo $parent_page_name; ?></a> <i class="fas fa-angle-right orange"></i> <?php endif; ?>
										<a href="<?php echo get_the_permalink(); ?>"><?php echo $title; ?></a>
									</div>
									<?php if($img_word): ?><div class="head-96 handy shadow white"><?php echo $img_word; ?></div><?php endif; ?>
									<?php if($img_text): ?><h1 class="head-28 light shadow white pad-5"><?php echo $img_text ;?></h1><?php endif; ?>
								</div>

								 <div data-match-height="title" class="equal col-4 desk-5 tab-12">
								  <div class="main-image-buttons">
									<div class="col-4 center">
										<a href="#listings">
											<div class="circle-icon">
											<span class="fa-layers fa-fw">
												<i class="fas fa-circle red"></i>
												<i class="fa-inverse fas fa-angle-down" data-fa-transform="shrink-6"></i>
											</span>
											</div>
											<span class="white"> See All </span>
										</a>	
									</div>
									<div class="col-4 center">
										<a href="#filters">
											<div class="circle-icon">
											<span class="fa-layers fa-fw">
												<i class="fas fa-circle orange"></i>
												<i class="fa-inverse fas fa-filter" data-fa-transform="shrink-6"></i>
											</span>
											</div>
											<span class="white"> Filter Listings </span>
										</a>	
									</div>
									<div class="col-4 center">
										<a href="#highlights">
											<div class="circle-icon">
											<span class="fa-layers fa-fw">
												<i class="fas fa-circle blue"></i>
												<i class="fa-inverse fas fa-star" data-fa-transform="shrink-6"></i>
											</span>
											</div>
											<span class="white"> Highlights </span>
										</a>	
									</div>
									<div class="clear"></div>
									</div>
							   </div>
							   <div class="clear"></div>
							</div>
							<?php if($img_caption): ?>
							   <div class="main-image-caption">
								   <?php echo $img_caption ;?>
							   </div>
							<?php endif; ?>
						</div>

				</section>
			</div>
			<?php endif; ?>

<?php endwhile; endif; ?>
</div>

<div id="content" class="nopadding">
			
<section id="main-area" class="full nopadding">
				
				<?php include( locate_template( '/page-components/sidebars/filters.php', false, false ) ); ?>
				
				<div id="listings" class="inner wsidebar relative">
					
				<?php

				$per_page = -1;

				//Offset 0 on first page
				if ($paged == 1):
					$offset = 0;
				else :
					$offset = ($paged - 1) * $per_page;
				endif;

				$listings_args = array(
					'post_type' => 'destination',
					'meta_key'	=> 'listing-image',
					'orderby' => 'meta_value',
					'order' => 'ASC',
					'offset' => $offset,
					'posts_per_page'=> $per_page,
					'paged' => $paged,
					 'tax_query' => array(
						array(
							'taxonomy' 	=> 'category',
							'field' 	=> 'term_id',
							'terms' 	=> array($listings_IDs),
							'operator'	=> 'IN' 
						)
					 ),

				 );

				$listings_query = new WP_Query( $listings_args );
				?>

				<?php if ( $listings_query->have_posts() ) : ?>
				<div class="pad-30">
				<h1 class="head-54 center handy"><?php echo $headline; ?></h1>
				<div class="center"><?php echo $subhead; ?> </div>
				<p class="center"> Your search returned <strong id="listings-number"><?= number_format($listings_query->found_posts) ?></strong> results:</p>
				</div>	
					
					<div id="grid" class="relative grid">
					
						<?php while ( $listings_query->have_posts() ) : $listings_query->the_post(); ?>

						<?php	
							//Main Image & Caption
							if( have_rows('listing-image') ): while( have_rows('listing-image') ): the_row();

								$listing_image_pull = get_sub_field('image');
								$listing_image = $listing_image_pull['sizes']['large'];
								$listing_image_url = get_sub_field('url');

								if (!($listing_image)):
									$listing_image = $listing_image_url;
								endif;

							endwhile; endif;
	
							//Headlines
							$listing_title = get_the_title();
							$listing_head = get_field('listing-headline');

							if ($listing_head) :
								$listing_title = $listing_head;
							endif;

							//Excerpt
							$listing_body = strip_tags(get_field('listing-body'));
							$listing_excerpt = substr($listing_body, 0, 125);
							$listing_link = get_the_permalink();
							
							if (!($listing_body)) :
								$listing_content = strip_tags(get_the_content());
								$listing_excerpt = substr($listing_content, 0, 125);
							endif;
						
							//Tripadvisor
							$listing_trip = get_field('listing-tripadvisor');
							$listing_rate = get_field('listing-rating');
							
							if ($listing_rate == '5'): $listing_rate = '5.0'; endif;
							if ($listing_rate == '4'): $listing_rate = '4.0'; endif;
							if ($listing_rate == '3'): $listing_rate = '3.0'; endif;
							if ($listing_rate == '2'): $listing_rate = '2.0'; endif;
							if ($listing_rate == '1'): $listing_rate = '1.0'; endif;
							if ($listing_rate == '0'): $listing_rate = '0.0'; endif;
					
							$listing_ta_img = 'http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/'.$listing_rate.'-'.$listing_trip.'-5.png';
							$listing_rate_sort = (isset($listing_rate)) ? floatval($listing_rate) : 0;
						
							//Show Category
							$listing_string ='';
							$listing_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );
							foreach($listing_cats as $cat){ 
								$listing_category = $cat->name; 
								$listing_cat_slug = $cat->slug; 
								$listing_string .= $cat->name . ", ";
								$categories = rtrim($listing_string, ", ");
								
								$listing_string ='';
								$listing_string .= $cat->name . " ";
								$cat_cats = strtolower($listing_string);
							break; 
							}
						
							//Show Region
							$region_string ='';
							$listing_region =  wp_get_post_terms(get_the_ID(), 'region', array('orderby' => 'term_order' ) );
							foreach($listing_region as $reg){ 
								$listing_region = $reg->name; 
								$listing_region_slug = $reg->slug; 
								$region_string .= $reg->name . ", ";
								$regions = rtrim($region_string, ", ");
							break; 
							}
						
							//Show City
							$city_string ='';
							$listing_city =  wp_get_post_terms(get_the_ID(), 'city', array('orderby' => 'term_order' ) );
							foreach($listing_city as $cit){ 
								$listing_city = $cit->name; 
								$listing_city_slug = $cit->slug; 
								$city_string .= $cit->name . ", ";
								$cities = rtrim($city_string, ", ");
								
								$city_string ='';
								$city_string .= $cit->name . " ";
								$city_cats = strtolower($city_string);
							break; 
							}
						?>
						
						
						<div class="col-4-full desk-6-full phone-12-full iso-listing <?php echo $cat_cats; ?> <?php echo $listing_region_slug; ?> <?php echo $city_cats; ?> <?php if ($listing_rate_sort): ?>rating-<?php echo $listing_rate_sort ?><?php endif; ?>" data-rating="<?php echo $listing_rate_sort ?>" data-title="<?php the_title(); ?>">
							
						<div class="listing square relative">
							<a class="link-wrap" href="<?php echo $listing_link; ?>">
								<?php if ($listing_image) : ?>
								<div class="img-wrap square">
									<img src="<?php echo $listing_image; ?>" alt="<?php echo $listing_title;?>"  title="<?php echo $listing_title;?>">
									<div class="text relative z-index">
										<div class="read-more label-14"><i class="fas fa-info-circle white"></i> MORE INFO</div>
										<div class="inner white shadow">
										<div class="head-28"><?php echo $listing_title;?></div>
										<?php if ($listing_trip) : ?>
											<img class="ta" src="<?php echo $listing_ta_img; ?>" height="18"> 
										<?php endif; ?>
										<div class="font-14"><?php echo $listing_excerpt; ?> &hellip;</div>
										<div class="margin-top-10">
										<div class="col-6 font-14 bold">
											<i class="fas fa-map-marker-alt"></i> <?php echo $regions ;?>
										</div>
										<div class="col-6 font-14 bold">
											<i class="fas fa-building"></i> <?php echo $cities ;?>
										</div>
										<div class="clear"></div>
										</div>
										</div>
									</div>
								</div>
								<?php else: ?>
								<div class="img-wrap square blue-back relative no-img" style="background-image: url('<?php echo $img; ?>');">
									<img class="no-img-logo" src="<?php echo get_template_directory_uri(); ?>/library/logo/logo.svg" alt="<?php echo $listing_title;?>"  title="<?php echo $listing_title;?>">
									<div class="text relative z-index">
										<div class="read-more label-14"><i class="fas fa-info-circle white"></i> MORE INFO</div>
										<div class="inner white shadow">
										<div class="head-28"><?php echo $listing_title;?></div>
										<?php if ($listing_trip) : ?>
											<img class="ta" src="<?php echo $listing_ta_img; ?>" height="18"> 
										<?php endif; ?>
										<div class="font-14"><?php echo $listing_excerpt; ?> &hellip;</div>
										<div class="margin-top-10">
										<div class="col-6 font-14 bold">
											<i class="fas fa-map-marker-alt"></i> <?php echo $regions ;?>
										</div>
										<div class="col-6 font-14 bold">
											<i class="fas fa-building"></i> <?php echo $cities ;?>
										</div>
										<div class="clear"></div>
										</div>
										</div>
									</div>
								</div>
								<?php endif; ?>
							</a>	
						</div>
							
						</div>

						<?php endwhile; 
						endif; ?>
					<div class="clear"></div>
						
					</div>
					
					<div class="pagination-wrap">
					<div class="pager">
						<?php 
							echo paginate_links( array(
								'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
								'total'        => $listings_query->max_num_pages,
								'current'      => max( 1, get_query_var( 'paged' ) ),
								'format'       => '?paged=%#%',
								'show_all'     => false,
								'type'         => 'plain',
								'end_size'     => 2,
								'mid_size'     => 0,
								'prev_next'    => true,
								'prev_text'    => sprintf( '<i class="fas fa-angle-left"></i>' ),
								'next_text'    => sprintf( '<i class="fas fa-angle-right"></i>' ),
								'add_args'     => false,
								'add_fragment' => '',
							) );
						?>
					</div>
					</div>
						
					
				</div>
				<div class="clear"></div>
			</section>

	
<section id="highlights" class="full white nopadding">
		<div class="inner wide">
			<div class="col-6-full phone-12-full">
				<div class="img-wrap tall relative">
					<div class="head-96 handy white shadow absolute abstop z-index margin-top-60 center wide">
						<?php echo $higlight_emo; ?>
					</div>
					<img src="<?php echo $higlight_img; ?>">
					<div class="img-caption">
						<?php echo $higlight_place; ?>
					</div>
				</div>
			</div>
			
			<div class="col-6-full phone-12-full relative">
				<div class="circle-icon overlap left-side">
				<span class="fa-layers fa-fw">
					<i class="fas fa-circle orange"></i>
					<i class="fa-inverse fas fa-angle-right" data-fa-transform="shrink-6"></i>
				</span>
				</div>
				<div class="boxes tall blue-back texture-dark">
					<div class="pad-60">
					<?php if($higlight_head): ?><h2 class="head-48 center"><?php echo $higlight_head; ?></h2><?php endif; ?>
					<div class="font-16"><?php echo $higlight_text; ?></div>
					</div>
				
				
					
				<?php if(have_rows('highlight-promos')): ?>
					
				<div class="absbottom absolute wide">
				<div class="label-18 pad-10 darkblue-back"> <i class="fas fa-star"></i> Ohio Travel Inspiration</div>
					
				<?php while(have_rows('highlight-promos')): the_row(); 
					
					$grid_title = get_sub_field('promo-title');
					$grid_link = get_sub_field('promo-url');
					$grid_img_pull = get_sub_field('promo-image');
					$grid_img = $grid_img_pull['sizes']['medium'];
					
					$promo_content = get_sub_field('promo-content');
					
					
					if($promo_content):
					
					// override $post
						global $post;
						$post = $promo_content;
						setup_postdata($post); 

						$post_type = get_post_type($post->ID);
						
						//If Related Content is Page
						if ($post_type == 'page'):
							$promo_link = get_the_permalink();
							$promo_title = get_the_title();
					
							if(have_rows('page-image')): while(have_rows('page-image')): the_row(); 
								$pull_img_src = get_sub_field('image');
								$promo_img = $pull_img_src['sizes']['large'];
							endwhile; endif;
					
						endif;
					
						//If Related Content is Blog
						if ($post_type == 'post'):
							$promo_link = get_the_permalink();
							$promo_title = get_the_title();
					
							if( have_rows('blog-image') ): while( have_rows('blog-image') ): the_row();
					
								$rel_image_pull = get_sub_field('image');
								$rel_image_url = get_sub_field('url');
								$promo_img = $rel_image_pull['sizes']['large'];
								

								if (!($promo_img)):
									$promo_img = $rel_image_url;
								endif;

							endwhile; endif;
					
						endif;
						
						if ($grid_title):
						$promo_title = $grid_title;
					    endif;
					
						if ($grid_link):
						$promo_link = $grid_link;
					    endif;
						
						if ($grid_img):
						$promo_img = $grid_img;
					    endif;
					
				wp_reset_postdata();
				endif; 
				
				
				?>
					
				<div class="col-4-full">
					<a href="<?php echo $promo_link; ?>">
					<div class="boxes small">
						<div class="img-wrap square">
						<img src="<?php echo $promo_img; ?>">
						<div class="absolute wide pad-15 absbottom shadow head-18 z-index"> <?php echo $promo_title; ?></div>
						</div>
					</div>
					</a>
				</div>
				
				<?php 
					
				$grid_title = '';
				$grid_link = '';
				$grid_img = '';
				$promo_title = '';
				$promo_link = '';
				$promo_img = '';
					
				endwhile; ?>
				
				<div class="clear"></div>
				</div>
					
				<?php endif; ?>
				
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</section>

</div>
				
</main>

<?php get_footer(); ?>
