<?php /* Template Name: Events Search */ ?>

<?php get_header(); ?>

<?php 

//Grabs page_id for current page
$page_id = get_queried_object_id();

$date_start = $_GET["start_date"];
$date_end = $_GET["end_date"];
$event_type = $_GET["category_id"];
$event_city = $_GET["city_id"];

$date_start_words = date("F j, Y", strtotime($date_start));
$date_end_words = date("F j, Y", strtotime($date_end));
$event_type_words = $_GET["category_name"];
$event_city_words = $_GET["city_name"];

?>


<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Page">

<div id="content" class="nopadding">
	
			<section id="event-list" class="full <?php if ($event_type) : ?>hide-category <?php endif; ?> <?php if ($event_type) : ?>hide-city <?php endif; ?>">
				<div class="inner wide">
						
						<h2 class="label-18 red center">Search Results</h2>
					 	
						
						<h1 class="head-72 handy center">
							Ohio Events and Festivals <br>
							<span class="head-36 nunito light" style="position: relative; top: -45px;">
							
							<?php if ($event_city_words) : ?>
								in <a class="orange heavy popmake-date-search"><?php echo $event_city_words; ?>, Ohio</a>
							<?php endif; ?>
							<?php if ($date_start) : ?>
								for the dates <a class="orange heavy popmake-date-search"><?php echo $date_start_words; ?></a> to <a class="orange heavy popmake-date-search"><?php echo $date_end_words; ?></a>
							<?php endif; ?>
							<?php if ($event_type_words) : ?>
								tagged <a class="orange heavy popmake-date-search"><?php echo $event_type_words; ?></a>
							<?php endif; ?>
							</span>
						</h1>
						
						<?php echo do_shortcode('[add_eventon_el el_type="dr" start_range="'.$date_start.'" end_range="'.$date_end.'" event_count="100" ux_val="4" exp_so="yes" show_et_ft_img="yes" event_type="'.$event_type.'" event_type_2="'.$event_city.'" ]'); ?>
					
						<div class="line"></div>
						<div>
						<div class="col-4 center">
							<a href="<?php echo site_url(); ?>/festivals-events/">
								<button class="large red-back wide"><i class="fas fa-angle-left"></i> Back to Events Calendar</button>
							</a>
						</div>
						<div class="col-4 center">
							<a href="#" class="popmake-date-search">
								<button class="large red-back wide"><i class="fas fa-user"></i> Change My Dates</button>
							</a>
						</div>
						<div class="col-4 center">
							<a href="#" class="popmake-category-search">
								<button class="large red-back wide"><i class="fas fa-calendar-check"></i> Search by Category</button>
							</a>
						</div>
						<div class="clear"></div>
						</div>
				</div>
			</section>

</div>

</main>

	
<?php get_footer(); ?>
