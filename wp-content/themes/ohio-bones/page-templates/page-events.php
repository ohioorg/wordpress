<?php /* Template Name: Events */ ?>

<?php get_header(); ?>

<?php 

	//Grabs page_id for current page
	$page_id = get_queried_object_id();

	//Query options
	 $page_args = array( 
	 'page_id' => $page_id,
	 'post_type' => 'page',
	 'posts_per_page'=>1, 
	 );

	$page_query = new WP_Query( $page_args );

	if ($page_query->have_posts() ) : while ( $page_query->have_posts() ) : $page_query->the_post();
	
	//Main Page Attributes
	$title = get_the_title();
	$headline = get_field('page-headline');
	$subhead = get_field('page-subtitle');
	$summary = get_field('page-summary');
	$body = get_field('page-body');
	$video = get_field('page-video');

	if ($headline) :
		$title = $headline;
	endif; 

	$related_content = get_field('page-related');

	$page_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );

	foreach($page_cats as $page_cat):
		$page_cat_ID = $page_cat->ID; 
	endforeach; 

//SEO-rich Highlight Text
$higlight_img_pull = get_field('highlight-image');
$higlight_img = $higlight_img_pull['sizes']['large'];

$higlight_head = get_field('highlight-head');
$higlight_emo = get_field('highlight-emotion');
$higlight_text = get_field('highlight-text');
$higlight_promos = get_field('highlight-promos');
$higlight_place = get_field('highlight-place');

?>


<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Page">

<div id="page-head">
				
			<?php if(have_rows('page-image')): while(have_rows('page-image')): the_row(); ?>
				
			<?php 
			$img_src = get_sub_field('image');
			$img = $img_src['url'];
			$img_small = $img_src['sizes']['medium'];
			$img_link = get_sub_field('link');
			$img_word = get_sub_field('title');
			$img_text = get_sub_field('text');
			$img_caption = get_sub_field('authenticator');

			endwhile;
			endif; ?>
			
		     <div class="main-image" style="background-image: url('<?php echo $img; ?>')">
				<section class="full">	
						<div class="main-image-inner">
						    <div class="main-image-inner-content">
								<div data-match-height="title" class="equal col-8 desk-7 tab-12">
									<div id="breadcrumbs">
										<a href="<?php echo get_site_url(); ?>"><?php echo get_bloginfo(); ?></a> <i class="fas fa-angle-right orange"></i>
										<?php if ($parent_page) : ?><a href="<?php echo get_site_url(); ?>/<?php echo $parent_page_slug; ?>"><?php echo $parent_page_name; ?></a> <i class="fas fa-angle-right orange"></i> <?php endif; ?>
										<a href="<?php echo get_the_permalink(); ?>"><?php echo $title; ?></a>
									</div>
									<?php if($img_word): ?><div class="head-96 handy shadow white"><?php echo $img_word; ?></div><?php endif; ?>
									<?php if($img_text): ?><h1 class="head-28 light shadow white pad-5"><?php echo $img_text ;?></h1><?php endif; ?>
									<?php if($img_caption): ?>
									   <div class="main-image-caption">
										   <?php echo $img_caption ;?>
									   </div>
									<?php endif; ?>
								</div>
								 <div data-match-height="title" class="equal col-4 desk-5 tab-12">
								  <div class="main-image-buttons">
									  
									<div class="col-4 center">
										<a href="#event-calendar">
											<div class="circle-icon">
											<span class="fa-layers fa-fw">
												<i class="fas fa-circle orange"></i>
												<i class="fa-inverse fas fa-calendar-alt" data-fa-transform="shrink-6"></i>
											</span>
											</div>
											<span class="white"> Browse Calendar </span>
										</a>	
									</div>
									  
									<div class="col-4 center">
										<a class="popmake-date-search">
											<div class="circle-icon">
											<span class="fa-layers fa-fw">
												<i class="fas fa-circle red"></i>
												<i class="fa-inverse fas fa-search" data-fa-transform="shrink-6"></i>
											</span>
											</div>
											<span class="white"> Search by Date </span>
										</a>	
									</div>
									
									<div class="col-4 center">
										<a href="popmake-date-search">
											<div class="circle-icon">
											<span class="fa-layers fa-fw">
												<i class="fas fa-circle blue"></i>
												<i class="fa-inverse fas fa-calendar-check" data-fa-transform="shrink-6"></i>
											</span>
											</div>
											<span class="white"> Events by Category </span>
										</a>	
									</div>
									<div class="clear"></div>
									</div>
							   </div>
							   <div class="clear"></div>
							</div>
						</div>
				</section>
			</div>
</div>

<div id="content" class="nopadding">
	
	<section id="event-calendar" class="full nopadding">
		<div class="inner wide">
				<?php echo do_shortcode('[add_eventon_fc nexttogrid="yes" show_et_ft_img="yes" ft_event_priority="yes" jumper="yes" mo1st="yes" hover="numname" heat="yes" ]'); ?>
		</div>
	</section>

	<?php include( locate_template( '/page-components/banners/events-signup.php', false, false ) ); ?>

	<section id="event-blogs" class="full">
		<div class="inner wide relative">
			<h2 class="head-54 center handy">Ohio Events. Find Them Here.</h2>
			<div class="center">Check out some of our staff picks for article</div>

			<?php include( locate_template( '/page-components/carousels/events-articles.php', false, false ) ); ?>

			<div class="center"><a href="/blog/"><button class="large orange-back"> See all articles <i class="fas fa-angle-right"></i></button></a></div>

		</div>
		<div class="clear"></div>
	</section>
	
	<section id="highlight-area" class="full white nopadding">
		<div class="inner wide">
			<div class="col-6-full phone-12-full">
				<div class="img-wrap tall relative">
					<div class="head-96 handy white shadow absolute abstop z-index margin-top-60 center wide">
						<?php echo $higlight_emo; ?>
					</div>
					<img src="<?php echo $higlight_img; ?>">
					<div class="img-caption">
						<?php echo $higlight_place; ?>
					</div>
				</div>
			</div>
			
			<div class="col-6-full phone-12-full relative">
				<div class="circle-icon overlap left-side">
				<span class="fa-layers fa-fw">
					<i class="fas fa-circle orange"></i>
					<i class="fa-inverse fas fa-angle-right" data-fa-transform="shrink-6"></i>
				</span>
				</div>
				<div class="blue-back texture-dark">
					<div class="pad-60">
					<?php if($higlight_head): ?><h2 class="head-48 center"><?php echo $higlight_head; ?></h2><?php endif; ?>
					<div class="font-16"><?php echo $higlight_text; ?></div>
					</div>
				
				<div class="relative wide">
				<div class="label-18 pad-10 darkblue-back center"> <i class="fas fa-star"></i> Ohio Travel Inspiration</div>
					
				<?php if(have_rows('highlight-promos')): while(have_rows('highlight-promos')): the_row(); 
					
					$grid_title = get_sub_field('promo-title');
					$grid_link = get_sub_field('promo-url');
					$grid_img_pull = get_sub_field('promo-image');
					$grid_img = $grid_img_pull['sizes']['medium'];
					
					$promo_content = get_sub_field('promo-content');
					
					
					if($promo_content):
					
					// override $post
						global $post;
						$post = $promo_content;
						setup_postdata($post); 

						$post_type = get_post_type($post->ID);
						
						//If Related Content is Page
						if ($post_type == 'page'):
							$promo_link = get_the_permalink();
							$promo_title = get_the_title();
					
							if(have_rows('page-image')): while(have_rows('page-image')): the_row(); 
								$pull_img_src = get_sub_field('image');
								$promo_img = $pull_img_src['sizes']['large'];
							endwhile; endif;
					
						endif;
					
						//If Related Content is Blog
						if ($post_type == 'post'):
							$promo_link = get_the_permalink();
							$promo_title = get_the_title();
					
							if( have_rows('blog-image') ): while( have_rows('blog-image') ): the_row();
					
								$rel_image_pull = get_sub_field('image');
								$rel_image_url = get_sub_field('url');
								$promo_img = $rel_image_pull['sizes']['large'];
								

								if (!($promo_img)):
									$promo_img = $rel_image_url;
								endif;

							endwhile; endif;
					
						endif;
						
						if ($grid_title):
						$promo_title = $grid_title;
					    endif;
					
						if ($grid_link):
						$promo_link = $grid_link;
					    endif;
						
						if ($grid_img):
						$promo_img = $grid_img;
					    endif;
					
				wp_reset_postdata();
				endif; 
				
				
				?>
					
				<div class="col-4-full">
					<a href="<?php echo $promo_link; ?>">
					<div class="boxes small">
						<div class="img-wrap square">
						<img src="<?php echo $promo_img; ?>">
						<div class="absolute wide pad-15 absbottom shadow head-18 z-index"> <?php echo $promo_title; ?></div>
						</div>
					</div>
					</a>
				</div>
				
				<?php 
					
				$grid_title = '';
				$grid_link = '';
				$grid_img = '';
				$promo_title = '';
				$promo_link = '';
				$promo_img = '';
					
				endwhile;
				endif; ?>
				
				<div class="clear"></div>
				</div>
					
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</section>


	
</div>

</main>
<?php endwhile; endif; ?>
	
<?php get_footer(); ?>
