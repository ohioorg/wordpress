<?php /* Template Name: Homepage */ ?>

<?php get_header(); ?>

<?php 

	//Grabs page_id for current page
	$page_id = get_queried_object_id();

	//Query options
	 $page_args = array( 
	 'page_id' => $page_id,
	 'post_type' => 'page',
	 'posts_per_page'=>1, 
	 //'tag' => 'featured' --- add this back to pull only posts tagged "featured"
	 );

	$page_query = new WP_Query( $page_args );

	if ($page_query->have_posts() ) : while ( $page_query->have_posts() ) : $page_query->the_post();
	
	//Main Page Attributes
	$headline = get_field('page-headline');
	$subhead = get_field('page-subtitle');
	$summary = get_field('page-summary');
	$body = get_field('page-body');
	$video = get_field('page-video');

	$related_content = get_field('page-related');

	
?>

<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

			<header id="page-head">
				
				
					
				<?php if(have_rows('page-image')): ?>
				
				<div class="content-carousel">
				<?php while(have_rows('page-image')): the_row(); ?>
				
				<?php 
				$img_src = get_sub_field('image');
				$img = $img_src['url'];
				$img_link = get_sub_field('link');
				$img_word = get_sub_field('title');
				$img_text = get_sub_field('text');
				$img_caption = get_sub_field('authenticator');
				$img_button = get_sub_field('button-text');
				$img_text_position = get_sub_field('text-position');

				if( $img_link ): 

					// override $post
						global $post;
						$post = $img_link;
						setup_postdata($post); 

						$img_link = get_the_permalink();
					
				wp_reset_postdata();
				endif; ?>
					
					
				
				<div class="main-image" style="background-image: url('<?php echo $img; ?>')">

				<?php if($img_link): ?><a href="<?php echo $img_link; ?>"><?php endif; ?>
				<section class="full">	
						<div class="main-image-inner">
						   <div class="main-image-inner-content">
							   	<div class="col-8 desk-7 tab-12">
									<div class="head-96 handy shadow white"><?php echo $img_word; ?></div>
									<div class="head-28 light shadow white"><?php echo $img_text ;?></div>
							   	</div>
							   	<div class="col-4 desk-5 tab-12 relative">
									<button class="large red-back">
										<?php if($img_button): ?> <?php echo $img_button; else: ?> Read more <?php endif;?>&nbsp;<i class="fas fa-angle-right"></i>
									</button>
									<?php if($img_caption): ?><div class="main-image-caption"><?php echo $img_caption ;?></div><?php endif; ?>
							   	</div>
								<div class="clear"></div>
							</div>
						</div>
				</section>
					
			<?php if($img_link): ?></a><?php endif; ?>
			</div>
				
			<?php endwhile; ?>
			</div>
		<?php endif; ?>
				
				

	
					
</header>


<div id="content" class="nopadding">
		
			<section id="blogs" class="full">
				<div class="inner wide relative">
					<h1 class="head-54 center handy"><?php echo $headline; ?></h1>
					<div class="center"><?php echo $subhead; ?> </div>
					
					<?php include( locate_template( '/page-components/carousels/featured-articles.php', false, false ) ); ?>
					
					<div class="center"><a href="/blog/"><button class="large orange-back"> See all Ohio Stories <i class="fas fa-angle-right"></i></button></a></div>
					
				</div>
				<div class="clear"></div>
			</section>

<!--- Guide Order Form --->
<div>
<?php include( locate_template( '/page-components/banners/guide-order.php', false, false ) ); ?>
</div>


<!--- Destination Blocks --->
<section id="things-to-do" class="full nopadding">
<div class="inner wide">
<div class="col-6-full desk-12-full red-back relative texture-dark">
<div class="circle-icon overlap right-side">
	<span class="fa-layers fa-fw">
		<i class="fas fa-circle orange"></i>
		<i class="fa-inverse fas fa-map" data-fa-transform="shrink-6"></i>
	</span>
</div>
<div class="boxes large">
<div class="pad-60">
<h2 class="head-54 center handy white">Ohio Destinations</h2>
<div class="section-intro">From outdoor adventures and amazing arts scenes to engaging events and family fun, Ohio has so many unique destinations to explore. Here’s a look at some of our most popular and recommended areas of <em>Ohio. Find It Here</em>.</a>: </div>
</div>
</div>
</div>
	
<?php foreach($related_content as $rel_article): 
	// variable must NOT be called $post!!!
	//here you can pretty much grab any field for the Article by using $rel_article->ID; in ACF's get_field, etc, you do: $image = get_field('image', $rel_article->ID);

	$aid = $rel_article->ID;
	$rel_count = 0;

	//Headlines
	$rel_link = get_the_permalink($rel_article->ID);
	$rel_title = get_the_title($rel_article->ID);

    $rel_image = '';
	//Main Image & Caption
	if( have_rows('page-image', $rel_article->ID )): while( have_rows('page-image', $rel_article->ID)): the_row();

		$rel_image_pull = get_sub_field('image');
		$rel_image = $rel_image_pull['sizes']['large'];
		$rel_image_url = get_sub_field('url');
		$rel_sub = get_sub_field('text');

		if (!($rel_image)):
			$rel_image = $rel_image_url;
		endif;

	endwhile; endif;
?>

<div class="col-3-full tab-6-full white">
<div class="boxes small">
  <a  class="link-wrap" href="<?php echo $rel_link; ?>">
	<div class="img-wrap square relative">
		<img src="<?php echo $rel_image; ?>" alt="<?php echo $prom_title ;?>" title="<?php echo $prom_title ;?>" width="100%">
		<div class="text relative z-index">
			<div class="inner center">
			<div class="head-36 white"><?php echo $rel_title ;?></div>
			<div class="lin"></div>
			<div class="tag"><?php echo $rel_sub ;?></div>
			<div class="clear"></div>
			</div>
		</div>
	</div>
  </a>
</div>
</div>
	 
<?php endforeach; wp_reset_postdata(); ?>

</div>
</section>
	
<?php	
	
//SEO-rich Highlight Text
$higlight_img_pull = get_field('highlight-image');
$higlight_img = $higlight_img_pull['sizes']['large'];

$higlight_head = get_field('highlight-head');
$higlight_emo = get_field('highlight-emotion');
$higlight_text = get_field('highlight-text');
$higlight_promos = get_field('highlight-promos');
$higlight_place = get_field('highlight-place');

?>
	
	<section id="highlight-area" class="full white nopadding">
		<div class="inner wide">
			<div class="col-6-full phone-12-full">
				<div class="img-wrap tall relative">
					<div class="head-96 handy white shadow absolute abstop z-index margin-top-60 center wide">
						<?php echo $higlight_emo; ?>
					</div>
					<img src="<?php echo $higlight_img; ?>">
					<div class="img-caption">
						<?php echo $higlight_place; ?>
					</div>
				</div>
			</div>
			
			<div class="col-6-full phone-12-full relative">
				<div class="circle-icon overlap left-side">
				<span class="fa-layers fa-fw">
					<i class="fas fa-circle orange"></i>
					<i class="fa-inverse fas fa-angle-right" data-fa-transform="shrink-6"></i>
				</span>
				</div>
				<div class="blue-back texture-dark">
					<div class="pad-60">
					<?php if($higlight_head): ?><h2 class="head-48 center"><?php echo $higlight_head; ?></h2><?php endif; ?>
					<div class="font-16"><?php echo $higlight_text; ?></div>
					</div>
				
				<div class="relative wide">
				<div class="label-18 pad-10 darkblue-back center"> <i class="fas fa-star"></i> Ohio Travel Inspiration</div>
					
				<?php if(have_rows('highlight-promos')): while(have_rows('highlight-promos')): the_row(); 
					
					$grid_title = get_sub_field('promo-title');
					$grid_link = get_sub_field('promo-url');
					$grid_img_pull = get_sub_field('promo-image');
					$grid_img = $grid_img_pull['sizes']['medium'];
					
					$promo_content = get_sub_field('promo-content');
					
					
					if($promo_content):
					
					// override $post
						global $post;
						$post = $promo_content;
						setup_postdata($post); 

						$post_type = get_post_type($post->ID);
						
						//If Related Content is Page
						if ($post_type == 'page'):
							$promo_link = get_the_permalink();
							$promo_title = get_the_title();
					
							if(have_rows('page-image')): while(have_rows('page-image')): the_row(); 
								$pull_img_src = get_sub_field('image');
								$promo_img = $pull_img_src['sizes']['large'];
							endwhile; endif;
					
						endif;
					
						//If Related Content is Blog
						if ($post_type == 'post'):
							$promo_link = get_the_permalink();
							$promo_title = get_the_title();
					
							if( have_rows('blog-image') ): while( have_rows('blog-image') ): the_row();
					
								$rel_image_pull = get_sub_field('image');
								$rel_image_url = get_sub_field('url');
								$promo_img = $rel_image_pull['sizes']['large'];
								

								if (!($promo_img)):
									$promo_img = $rel_image_url;
								endif;

							endwhile; endif;
					
						endif;
						
						if ($grid_title):
						$promo_title = $grid_title;
					    endif;
					
						if ($grid_link):
						$promo_link = $grid_link;
					    endif;
						
						if ($grid_img):
						$promo_img = $grid_img;
					    endif;
					
				wp_reset_postdata();
				endif; 
				
				
				?>
					
				<div class="col-4-full">
					<a href="<?php echo $promo_link; ?>">
					<div class="boxes small">
						<div class="img-wrap square">
						<img src="<?php echo $promo_img; ?>">
						<div class="absolute wide pad-15 absbottom shadow head-18 z-index"> <?php echo $promo_title; ?></div>
						</div>
					</div>
					</a>
				</div>
				
				<?php 
					
				$grid_title = '';
				$grid_link = '';
				$grid_img = '';
				$promo_title = '';
				$promo_link = '';
				$promo_img = '';
					
				endwhile;
				endif; ?>
				
				<div class="clear"></div>
				</div>
					
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</section>


			<section id="instagram" class="full nopadding">
				<div class="inner wide red-back pad-60 center white relative">
					<div class="circle-icon overlap">
						<span class="fa-layers fa-fw">
							<i class="fas fa-circle orange"></i>
							<i class="fa-inverse fab fa-instagram" data-fa-transform="shrink-6"></i>
						</span>
					</div>
					<div class="head-54 handy">#OhioFindItHere on Instagram</div>
					<p>Whether it's the feeling of summer sun on your face as splash along the shores of Lake Erie, the pure joy in the eyes of your child during her first trip to the Columbus Zoo or memorable moments spent hiking through Hocking Hills with your dad, there's nothing quite like a great Ohio getaway. Here's a look at some of the memorbale moments visitors share daily on Instagram with #OhioFindItHere: </p>
				</div>
				<div class="inner wide center">
					<?php echo do_shortcode('[instagram-feed]'); ?>
				</div>
			</section>

</div>
				
</main>
<?php endwhile; endif; ?>


<?php get_footer(); ?>
