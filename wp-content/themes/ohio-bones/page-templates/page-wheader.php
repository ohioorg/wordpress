<?php /* Template Name: Blank Page w/ Header Image */ ?>

<?php get_header(); ?>



<?php 

	//Grabs page_id for current page
	global $post;
	$page_id = get_queried_object_id();
	$parent_page = $post->post_parent;
	$parent_page_name = get_the_title($parent_page);
	$parent_page_slug = get_the_permalink($parent_page);

	//Query options
	 $page_args = array( 
	 'page_id' => $page_id,
	 'post_type' => 'page',
	 'posts_per_page'=>1, 
	 );

	$page_query = new WP_Query( $page_args );

	if ($page_query->have_posts() ) : while ( $page_query->have_posts() ) : $page_query->the_post();
	
	//Main Page Attributes
	$title = get_the_title();
	$headline = get_field('page-headline');
	$subhead = get_field('page-subtitle');
	$summary = get_field('page-summary');
	$body = get_field('page-body');
	$video = get_field('page-video');

	if ($headline) :
		$title = $headline;
	endif; 

	$related_content = get_field('page-related');

	$page_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );

	foreach($page_cats as $page_cat):
		$page_cat_ID = $page_cat->ID; 
	endforeach; 

//SEO-rich Highlight Text
$higlight_img_pull = get_field('highlight-image');
$higlight_img = $higlight_img_pull['sizes']['large'];

$higlight_head = get_field('highlight-head');
$higlight_emo = get_field('highlight-emotion');
$higlight_text = get_field('highlight-text');
$higlight_promos = get_field('highlight-promos');
$higlight_place = get_field('highlight-place');

?>

<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

<?php if(have_rows('page-image')): while(have_rows('page-image')): the_row(); ?>
<div id="page-head">
				
<?php 
$img_src = get_sub_field('image');
$img = $img_src['url'];
$img_word = get_sub_field('title');
$img_text = get_sub_field('text');
$img_caption = get_sub_field('authenticator'); ?>

<div class="main-image" style="background-image: url('<?php echo $img; ?>')">
	<section class="full">	
			<div class="main-image-inner">
			   <div class="main-image-inner-content">
				   
				<div id="breadcrumbs" class="center">
					<a href="<?php echo get_site_url(); ?>"><?php echo get_bloginfo(); ?></a> <i class="fas fa-angle-right orange"></i>
					<?php if ($parent_page) : ?><a href="<?php echo get_site_url(); ?>/<?php echo $parent_page_slug; ?>"><?php echo $parent_page_name; ?></a> <i class="fas fa-angle-right orange"></i> <?php endif; ?>
					<a href="<?php echo get_the_permalink(); ?>"><?php echo $title; ?></a>
				</div>
				   
				<?php if($img_word): ?><h1 class="head-96 handy shadow white center"><?php echo $img_word; ?></h1><?php endif; ?>
				<?php if($img_text): ?><h2 class="head-28 light shadow white center margin-bottom-15"><?php echo $img_text ;?></h2><?php endif; ?>
				<?php if($img_caption): ?><div class="main-image-caption"><?php echo $img_caption ;?></div><?php endif; ?>
				</div>
			
			</div>
	</section>
</div>
	
</div>
<?php endwhile; endif; ?>
<?php endwhile; endif; ?>


<div id="content" class="nopadding">

	<article id="page-<?php the_ID(); ?>" class="page-main" role="article">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<section id="page-content" class="cf nopadding" itemprop="articleBody">
				<div class="box">
					
					<?php if(!(have_rows('page-image'))) : ?><h2 class="head-54 handy center"><?php echo $title; ?></h2> <?php endif; ?>
					<?php the_content(); ?>
				</div>
			</section>

		<?php endwhile; endif; ?>
	</article>

</div>


	
<?php get_footer(); ?>
