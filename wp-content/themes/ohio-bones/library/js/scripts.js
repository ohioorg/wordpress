/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
		if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *	// update the viewport, in case the window size has changed
 *	viewport = updateViewportDimensions();
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
*/

/*
 * We're going to swap out the gravatars.
 * In the functions.php file, you can see we're not loading the gravatar
 * images on mobile to save bandwidth. Once we hit an acceptable viewport
 * then we can swap out those images since they are located in a data attribute.
*/


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {
	
  'use strict';

  isWorking();
	
  stickyHeader();
	
  smoothScroll();
	
  matchHeight();
	
  sideMenus();
	
  showMap();

// Test Console Log
function isWorking() {
	
	console.log('hey, this is working');
}

// Sticky Main Nav
function stickyHeader() {
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1){  
    		$('.header-wrap').addClass("scroll");
			$('.header-wrap').removeClass("relative");
			$('.blog-scroll-head').addClass("scroll");
  		}
  		else{
    		$('.header-wrap').removeClass("scroll");
			$('.header-wrap').addClass("relative");
			$('.blog-scroll-head').removeClass("scroll");
  		}
		
	});	
}

//Smooth Scrolling to anchor link
function smoothScroll() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top - 216
        }, 1000);
        return false;
      }
    }
  });
}

function showMap() {
   $('#show-map').click(function() {
   
   		if($(this).hasClass('opened')){

			$('#google-map').removeClass('visible');
			$(this).removeClass('opened');
			$(this).children('i').removeClass('fa-times');
			$(this).children('i').addClass('fa-map-signs');
			$(this).children('.text').text('Map');
   			
		} else {
			
			$('#google-map').addClass('visible');
			$(this).addClass('opened');
			$(this).children('i').removeClass('fa-map-signs');
			$(this).children('i').addClass('fa-times');
			$(this).children('.text').text('Hide');
		}
   });
}

//MatchHeight Plugin
function matchHeight() {
	'use strict';
	$('.equal').matchHeight({
    	byRow: true,
    	property: 'min-height',
    	target: null,
    	remove: false
	});
}

// Side menu open & close
function sideMenus() {

	$('#menu .open-menu').click(function() {
		
		
		$('#search-menu-overlay').removeClass('visible');
		$('#search-menu').removeClass('opener');
		$('#search .fa-times').addClass('fa-search');
		$('#search .fa-times').removeClass('fa-times');
	
		
		$('body').toggleClass('noscroll');
		$('.nav-item').toggleClass('nohover');
		$('#side-menu').toggleClass('opener');
			if ( $('#menu .fa-bars').length) {
				$('#menu .fa-bars').addClass('fa-times');
				$('#menu .fa-bars').removeClass('fa-bars');
			}
			else {
				$('#menu .fa-times').addClass('fa-bars');
				$('#menu .fa-times').removeClass('fa-times');
			}
		$('#side-menu-overlay').toggleClass('visible');
	});
	
	// Side menu close on click overlay
	$('#side-menu-overlay').click(function() {
		$('#side-menu-overlay').removeClass('visible');
		$('#side-menu').removeClass('opener');
		$('body').removeClass('noscroll');
		if ( $('#menu .fa-bars').length) {
				$('#menu .fa-bars').addClass('fa-times');
				$('#menu .fa-bars').removeClass('fa-bars');
			}
		else {
			$('#menu .fa-times').addClass('fa-bars');
			$('#menu .fa-times').removeClass('fa-times');
		}
	});
	
	//Regions Menu
	$('#search .open-search').click(function() {
		
		$('#side-menu-overlay').removeClass('visible');
		$('#side-menu').removeClass('opener');
		$('#menu .fa-times').addClass('fa-bars');
		$('#menu .fa-times').removeClass('fa-times');
		
		$('body').toggleClass('noscroll');
		$('.nav-item').toggleClass('nohover');
		$('#search-menu').toggleClass('opener');
			if ( $('#search .fa-search').length) {
				$('#search .fa-search').addClass('fa-times');
				$('#search .fa-search').removeClass('fa-search');
			}
			else {
				$('#search .fa-times').addClass('fa-search');
				$('#search .fa-times').removeClass('fa-times');
			}
		$('#search-menu-overlay').toggleClass('visible');
	});
	
	$('#search-menu-overlay').click(function() {
		$('#search-menu-overlay').removeClass('visible');
		$('#search-menu').removeClass('opener');
		$('body').removeClass('noscroll');
		
		if ( $('#search .fa-search').length) {
				$('#search .fa-search').addClass('fa-times');
				$('#search .fa-search').removeClass('fa-search');
			}
		else {
			$('#search .fa-times').addClass('fa-search');
			$('#search .fa-times').removeClass('fa-times');
		}
	});
}

}); /* end of as page load scripts */
