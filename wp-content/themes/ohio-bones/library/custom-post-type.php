<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the custom type
function create_custom_post_types() { 
	
	// Cities
	register_post_type( 'town',
		array(
			'labels' => array(
			'name' => __( 'Cities' ),
			'singular_name' => __( 'City' ),
			'all_items' => __( 'All Cities'), /* the all items menu item */
		),
		'public' => true,
		'publicly_queryable' => true,
		'rewrite' => array('slug' => 'ohio-cities', 'with_front' => false ),
		'has_archive' => true,
		'supports' => array('title','author', 'revisions', 'page-attributes'),
		'taxonomies' => array('region')

		)
	);
	
	// Destinations
	register_post_type( 'destination',
		array(
			'labels' => array(
			'name' => __( 'Destinations' ),
			'singular_name' => __( 'Destination' ),
			'all_items' => __( 'All Destinations'), /* the all items menu item */
		),
		'public' => true,
			'publicly_queryable' => true,
		'rewrite' => array('slug' => 'destination', 'with_front' => false ),
		'has_archive' => true,
		'supports' => array('title','author', 'revisions', 'page-attributes'),
		'taxonomies' => array('category', 'region', 'city', 'season', )

		)
	);
	
	// Destinations
	register_post_type( 'deal',
		array(
			'labels' => array(
			'name' => __( 'Deals' ),
			'singular_name' => __( 'Deal' ),
			'all_items' => __( 'All Deals'), /* the all items menu item */
		),
		'public' => true,
			'publicly_queryable' => true,
		'rewrite' => array('slug' => 'deals', 'with_front' => false ),
		'has_archive' => true,
		'exclude_from_search' => true,
		'supports' => array('title','author', 'revisions', 'page-attributes'),
		'taxonomies' => array('category', 'region', 'city', 'season', )

		)
	);

	
	// Press Releases
	register_post_type( 'release',
		array(
			'labels' => array(
			'name' => __( 'Press Releases' ),
			'singular_name' => __( 'Press Release' ),
			'all_items' => __( 'All Press Releases'), /* the all items menu item */
		),
		'public' => true,
		'publicly_queryable' => true,
		'rewrite' => array('slug' => 'media', 'with_front' => false ),
		'has_archive' => true,
		'exclude_from_search' => true,
		'supports' => array('title','author', 'revisions', 'page-attributes'),
		'taxonomies' => array('category', 'region', 'city', 'season', )

		)
	);
	
	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type( 'category', 'deal' );
	
	// Industry Updates
	register_post_type( 'update',
		array(
			'labels' => array(
			'name' => __( 'Industry Update' ),
			'singular_name' => __( 'Industry Update' ),
			'all_items' => __( 'All Industry Updates'), /* the all items menu item */
		),
		'public' => true,
		'publicly_queryable' => false,
		'rewrite' => array('slug' => 'industry', 'with_front' => false ),
		'has_archive' => true,
		'exclude_from_search' => true,
		'supports' => array('title','author', 'revisions', 'page-attributes'),
		'taxonomies' => array('category', 'region', 'city', 'season', )

		)
	);
	
}

// adding the function to the Wordpress init
add_action( 'init', 'create_custom_post_types');
	


function create_taxonomies() {
	// Regions
	register_taxonomy(
	'region',
	array('post', 'page', 'town', 'destination', 'deal', 'release', 'update' ),
		array(
		'label' => __( 'Regions' ),
		'rewrite' => array( 'slug' => 'region' ),
		'hierarchical' => true,
		)
	);
	
	// Cities
	register_taxonomy(
	'city',
	array('post', 'page', 'destination', 'deal', 'release', 'update' ),
		array(
		'label' => __( 'Cities' ),
		'rewrite' => array( 'slug' => 'city' ),
		'hierarchical' => true,
		)
	);
	
	//Seasons
	register_taxonomy(
	'season',
	array('post', 'page', 'destination', 'deal' ),
		array(
		'label' => __( 'Seasons' ),
		'rewrite' => array( 'slug' => 'season' ),
		'hierarchical' => true,
		)
	);
	
	// Industry Updates
	register_taxonomy(
	'industry-updates',
	array('update'),
		array(
		'label' => __( 'Industry Categories' ),
		'rewrite' => array( 'slug' => 'industry-categories' ),
		'hierarchical' => true,
		)
	);
	
    // Add Interest/Categories to Pages
    register_taxonomy_for_object_type('category', 'page');  
	
}

add_action( 'init', 'create_taxonomies' );
	
	/*
		looking for custom meta boxes?
		check out this fantastic tool:
		https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
	*/
	

?>
