<?php /*City Pages */ ?>

<?php get_header(); ?>

<?php 

	//Grabs page_id for current page
	$page_id = get_queried_object_id();

	//Query options
	 $city_args = array( 
	 'post_type' => 'town',
	 'posts_per_page'=>1,
	 'p'=> $page_id,
	 );

	$city_query = new WP_Query( $city_args );

	if ($city_query->have_posts() ) : while ( $city_query->have_posts() ) : $city_query->the_post();
	
	//Basic Page Info
	$page_title = get_the_title();
	$page_headline = get_field('city-headline');
	
	//Header Override
	if ($page_headline) :
		$page_title = $page_headline;
	endif;

	$page_subtitle = get_field('subtitle');
	$page_summary = get_field('summary');
	$page_text = get_field('city-body');
	$page_node = get_field('node-id');

	//Media
	if( have_rows('city-image') ): while( have_rows('city-image') ): the_row();
		$img_src = get_sub_field('image');
		$img = $img_src['url'];
		$img_word = get_sub_field('title');
		$img_text = get_sub_field('text');
		$img_caption = get_sub_field('authenticator');
		$img_url = get_sub_field('url');
		
		//If no image, fallback to Drupal URL
		if (!($img)):
			$img = $img_url;
		endif;
		
	endwhile; endif;
	endwhile; endif;
?>

<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

<?php if ($img) : ?>
<header>
			
	<div class="main-image" style="background-image: url('<?php echo $img; ?>')">
		<section class="full">	
				<div class="main-image-inner">
				   <div class="main-image-inner-content">
					
					<?php if($img_word): ?>
					   <h1 class="head-96 handy shadow white"><?php echo $img_word; ?></h1>
					<?php else: ?>
					   <h1 class="head-96 handy shadow white"><?php echo $page_title; ?>, Ohio</h1>
					<?php endif; ?>
					<?php if($img_text): ?>
					   <h2 class="head-28 light shadow white margin-bottom-15"><?php echo $img_text ;?></h2>
					<?php elseif ($page_subitle): ?>
					   <h2 class="head-28 light shadow white margin-bottom-15"><?php echo $page_subtitle ;?></h2>
					<?php endif; ?>
					</div>
				</div>
		</section>
	</div>
				
</header>
<?php endif; ?>


<div id="content">		
		<section id="blogs" class="nopadding">
			<div class="inner">
				<h2 class="head-54 center handy">Welcome to <?php echo $page_title; ?>, Ohio!</h2>
				<div><?php echo $page_text; ?></div>
			</div>
		</section>
	
		<section id="main-area" class="full nopadding">
				
				<?php include( locate_template( '/page-components/sidebars/filters.php', false, false ) ); ?>
				
				<div id="listings" class="inner wsidebar relative">
					
				<?php

				$listings_args = array(
					'post_type' => 'destination',
					'meta_key'	=> 'listing-image',
					'orderby' => 'meta_value',
					'order' => 'ASC',
					'offset' => $offset,
					'posts_per_page'=> $per_page,
					'paged' => $paged,
					'tax_query' => array(
						array(
							'taxonomy' 	=> 'city',
							'field' 	=> 'term_id',
							'terms' 	=> array($listings_IDs),
							'operator'	=> 'IN' 
						)
					 ),

				 );

				$listings_query = new WP_Query( $listings_args );
				?>

				<?php if ( $listings_query->have_posts() ) : ?>
				<div class="pad-30">
				<h1 class="head-54 center handy"><?php echo $headline; ?></h1>
				<div class="center"><?php echo $subhead; ?> </div>
				<p class="center"> Your search returned <strong id="listings-number"><?= number_format($listings_query->found_posts) ?></strong> results:</p>
				</div>	
					
					<div id="grid" class="relative grid">
					
						<?php while ( $listings_query->have_posts() ) : $listings_query->the_post(); ?>

						<?php	
							//Main Image & Caption
							if( have_rows('listing-image') ): while( have_rows('listing-image') ): the_row();

								$listing_image_pull = get_sub_field('image');
								$listing_image = $listing_image_pull['sizes']['large'];
								$listing_image_url = get_sub_field('url');

								if (!($listing_image)):
									$listing_image = $listing_image_url;
								endif;

							endwhile; endif;
	
							//Headlines
							$listing_title = get_the_title();
							$listing_head = get_field('listing-headline');

							if ($listing_head) :
								$listing_title = $listing_head;
							endif;

							//Excerpt
							$listing_body = strip_tags(get_field('listing-body'));
							$listing_excerpt = substr($listing_body, 0, 125);
							$listing_link = get_the_permalink();
							
							if (!($listing_body)) :
								$listing_content = strip_tags(get_the_content());
								$listing_excerpt = substr($listing_content, 0, 125);
							endif;
						
							//Tripadvisor
							$listing_trip = get_field('listing-tripadvisor');
							$listing_rate = get_field('listing-rating');
							
							if ($listing_rate == '5'): $listing_rate = '5.0'; endif;
							if ($listing_rate == '4'): $listing_rate = '4.0'; endif;
							if ($listing_rate == '3'): $listing_rate = '3.0'; endif;
							if ($listing_rate == '2'): $listing_rate = '2.0'; endif;
							if ($listing_rate == '1'): $listing_rate = '1.0'; endif;
							if ($listing_rate == '0'): $listing_rate = '0.0'; endif;
					
							$listing_ta_img = 'http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/'.$listing_rate.'-'.$listing_trip.'-5.png';
							$listing_rate_sort = (isset($listing_rate)) ? floatval($listing_rate) : 0;
						
							//Show Category
							$listing_string ='';
							$listing_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );
							foreach($listing_cats as $cat){ 
								$listing_category = $cat->name; 
								$listing_cat_slug = $cat->slug; 
								$listing_string .= $cat->name . ", ";
								$categories = rtrim($listing_string, ", ");
								
								$listing_string ='';
								$listing_string .= $cat->name . " ";
								$cat_cats = strtolower($listing_string);
							break; 
							}
						
							//Show Region
							$region_string ='';
							$listing_region =  wp_get_post_terms(get_the_ID(), 'region', array('orderby' => 'term_order' ) );
							foreach($listing_region as $reg){ 
								$listing_region = $reg->name; 
								$listing_region_slug = $reg->slug; 
								$region_string .= $reg->name . ", ";
								$regions = rtrim($region_string, ", ");
							break; 
							}
						
							//Show City
							$city_string ='';
							$listing_city =  wp_get_post_terms(get_the_ID(), 'city', array('orderby' => 'term_order' ) );
							foreach($listing_city as $cit){ 
								$listing_city = $cit->name; 
								$listing_city_slug = $cit->slug; 
								$city_string .= $cit->name . ", ";
								$cities = rtrim($city_string, ", ");
								
								$city_string ='';
								$city_string .= $cit->name . " ";
								$city_cats = strtolower($city_string);
							break; 
							}
						?>
						
						
						<div class="col-4-full desk-6-full phone-12-full iso-listing <?php echo $cat_cats; ?> <?php echo $listing_region_slug; ?> <?php echo $city_cats; ?> <?php if ($listing_rate_sort): ?>rating-<?php echo $listing_rate_sort ?><?php endif; ?>" data-rating="<?php echo $listing_rate_sort ?>" data-title="<?php the_title(); ?>">
							
						<div class="listing square relative">
							<a class="link-wrap" href="<?php echo $listing_link; ?>">
								<?php if ($listing_image) : ?>
								<div class="img-wrap square">
									<img src="<?php echo $listing_image; ?>" alt="<?php echo $listing_title;?>"  title="<?php echo $listing_title;?>">
									<div class="text relative z-index">
										<div class="read-more label-14"><i class="fas fa-info-circle white"></i> MORE INFO</div>
										<div class="inner white shadow">
										<div class="head-28"><?php echo $listing_title;?></div>
										<?php if ($listing_trip) : ?>
											<img class="ta" src="<?php echo $listing_ta_img; ?>" height="18"> 
										<?php endif; ?>
										<div class="font-14"><?php echo $listing_excerpt; ?> &hellip;</div>
										<div class="margin-top-10">
										<div class="col-6 font-14 bold">
											<i class="fas fa-map-marker-alt"></i> <?php echo $regions ;?>
										</div>
										<div class="col-6 font-14 bold">
											<i class="fas fa-building"></i> <?php echo $cities ;?>
										</div>
										<div class="clear"></div>
										</div>
										</div>
									</div>
								</div>
								<?php else: ?>
								<div class="img-wrap square blue-back relative no-img" style="background-image: url('<?php echo $img; ?>');">
									<img class="no-img-logo" src="<?php echo get_template_directory_uri(); ?>/library/logo/logo.svg" alt="<?php echo $listing_title;?>"  title="<?php echo $listing_title;?>">
									<div class="text relative z-index">
										<div class="read-more label-14"><i class="fas fa-info-circle white"></i> MORE INFO</div>
										<div class="inner white shadow">
										<div class="head-28"><?php echo $listing_title;?></div>
										<?php if ($listing_trip) : ?>
											<img class="ta" src="<?php echo $listing_ta_img; ?>" height="18"> 
										<?php endif; ?>
										<div class="font-14"><?php echo $listing_excerpt; ?> &hellip;</div>
										<div class="margin-top-10">
										<div class="col-6 font-14 bold">
											<i class="fas fa-map-marker-alt"></i> <?php echo $regions ;?>
										</div>
										<div class="col-6 font-14 bold">
											<i class="fas fa-building"></i> <?php echo $cities ;?>
										</div>
										<div class="clear"></div>
										</div>
										</div>
									</div>
								</div>
								<?php endif; ?>
							</a>	
						</div>
							
						</div>

						<?php endwhile; 
						endif; ?>
					<div class="clear"></div>
						
					</div>
					
					<div class="pagination-wrap">
					<div class="pager">
						<?php 
							echo paginate_links( array(
								'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
								'total'        => $listings_query->max_num_pages,
								'current'      => max( 1, get_query_var( 'paged' ) ),
								'format'       => '?paged=%#%',
								'show_all'     => false,
								'type'         => 'plain',
								'end_size'     => 2,
								'mid_size'     => 0,
								'prev_next'    => true,
								'prev_text'    => sprintf( '<i class="fas fa-angle-left"></i>' ),
								'next_text'    => sprintf( '<i class="fas fa-angle-right"></i>' ),
								'add_args'     => false,
								'add_fragment' => '',
							) );
						?>
					</div>
					</div>
						
					
				</div>
				<div class="clear"></div>
			</section>
	
</div>


</main>

<?php get_footer(); ?>
