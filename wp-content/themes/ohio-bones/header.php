<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>
		
		<script type="text/javascript"> 
		var disqus_developer = 1; // this would set it to developer mode
		</script> 

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
        <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<!-- Stylesheets -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
		<script>
  			FontAwesomeConfig = { searchPseudoElements: true };
		</script>
		<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
		

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		
		<?php // end analytics ?>
		
		<?php
			global $post;
			global $template;
    		$template_name = basename($template);

			//Page Tracking Variables
			$post_date = date( 'l F j, Y' );
			$page_title = $post->post_title;
			$page_slug = $post->post_name;
			$page_meta = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);
			$page_parent = get_post($post->post_parent);
			$page_parent_slug = $page_parent->post_name;
			$page_parent_title = $page_parent->post_title;
			$page_post_type = get_post_type($post->ID);
			$page_post_type_object = get_post_type_object( get_post_type($post->ID) );
			$page_post_type_title = $page_post_type_object->label;
			$page_post_category = wp_get_post_terms( get_the_ID(), 'category', array('orderby' => 'term_order' ) );

			foreach($page_post_category as $ppc){
				$page_cat_slug = $ppc->slug;
				$page_cat_label = $ppc->slug;
				$page_cat_title = $ppc->name;
			break;
			}
		?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
	
	<span class="structured-data none">
		<meta itemprop="name" content="<?php echo $page_cat_title; ?>" />
		<meta itemprop="url" content="<?php echo get_the_permalink(); ?>" />
		<meta itemprop="description" content="<?php echo $page_meta; ?>" />
		<meta itemprop="lastReviewed" content="<?php echo $post_date; ?>" />
		<meta itemprop="reviewedBy" content="Ohio.org Staff" />
		
	<?php if ($page_post_type == 'page') : ?>
	<span itemscope itemtype="http://schema.org/BreadcrumbList">
	  <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="<?= get_site_url(); ?>">
		<span itemprop="name">Ohio. Find It Here.</span></a>
		<meta itemprop="position" content="1" />
	  </span>
		
	  <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<meta itemprop="item" content="<?= get_site_url(); ?>/<?php echo $page_parent_slug; ?>/<?php echo $page_slug; ?>">
		<meta itemprop="name" content="<?php echo $page_parent_title; ?>" />
		<meta itemprop="position" content="2" />
	  </span>
	  <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<meta itemprop="item" content="<?= get_site_url(); ?>/<?php echo str_replace("101-", "102-", $page_parent_slug); ?>/<?php echo $page_slug; ?>">
		<meta itemprop="name" content="<?php echo $page_title; ?>" />
		<meta itemprop="position" content="3" />
	  </span>
	</span>
	<?php endif; ?>
	
	</span>

	<div id="container">

	<header class="header-wrap red-back white <?php if( $template_name == 'page.php' || $template_name == 'page-events-search.php' || is_singular('ajde_events')) : ?> no-ribbon <?php endif; ?>" role="banner" itemscope itemtype="http://schema.org/WPHeader">

		<?php get_template_part( 'page-components/menus/nav-main' ); ?>

	</header>
