<div class="four-column-carousel padtop">

						<!--FEATURED ARTICLES--->
						<?php

						$featuredblogs_args = array(
							'post_type' => 'post',
							'status' => 'publish',
							'orderby' => 'date',
							'order' => 'DESC',
							'offset' => 0,
							'posts_per_page'=> 12,
						 );

						$featuredblogs_query = new WP_Query( $featuredblogs_args );
						?>

						<?php if ( $featuredblogs_query->have_posts() ) : while ( $featuredblogs_query->have_posts() ) : $featuredblogs_query->the_post(); ?>

						<?php

							//Main Image & Caption
							if( have_rows('blog-image') ): while( have_rows('blog-image') ): the_row();

								$blog_image_pull = get_sub_field('image');
								$blog_image = $blog_image_pull['sizes']['large'];
								$blog_image_caption = get_sub_field('caption');
								$blog_image_url = get_sub_field('url');

								if (!($blog_image)):
									$blog_image = $blog_image_url;
								endif;

							endwhile; endif;
	
							//Headlines
							$blog_title = get_the_title();
							$blog_head = get_field('blog-headline');
							$blog_sub = strip_tags(get_field('blog-subtitle'));

							if ($blog_head) :
								$blog_title = $blog_head;
							endif;

							//Excerpt
							$blog_body = strip_tags(get_field('blog-body'));
							$blog_excerpt = substr($blog_body, 0, 155);
							$blog_link = get_the_permalink();
							
							if (!($blog_body)) :
								$blog_content = strip_tags(get_the_content());
								$blog_excerpt = substr($blog_content, 0, 155);
							endif;
						
							//Show Category
							$cat_string ='';
							$blog_cats =  wp_get_post_terms(get_the_ID(), 'category', array('orderby' => 'term_order' ) );
							foreach($blog_cats as $cat){ 
								$blog_category = $cat->name; 
								$blog_cat_slug = $cat->slug; 
								$cat_string .= $cat->name . ", ";
								$categories = rtrim($cat_string, ", ");
								break; 
							}
						
							//Show Region
							$region_string ='';
							$blog_region =  wp_get_post_terms(get_the_ID(), 'region', array('orderby' => 'term_order' ) );
							foreach($blog_region as $reg){ 
								$blog_region = $reg->name; 
								$blog_region_slug = $reg->slug; 
								$region_string .= $reg->name . ", ";
								$regions = rtrim($region_string, ", ");
								break; 
							}

						?>

						<div class="listing square relative">
							<a class="link-wrap" href="<?php echo $blog_link; ?>">
								<div class="img-wrap square ">
									<img src="<?php echo $blog_image; ?>" alt="<?php echo $blog_title;?>"  title="<?php echo $blog_title;?>">
									<div class="text relative z-index">
									<div class="read-more label-14"><i class="fas fa-plus white"></i> READ MORE</div>
									<div class="inner">
									<div class="head-28 white"><?php echo $blog_title;?></div>
									<div class="font-14 white"><?php echo $blog_excerpt; ?> &hellip;</div>
									<div class="clear"></div>
									</div>
									</div>
								</div>
							</a>	
						</div>

						<?php endwhile; 
						$featuredblogs_query->wp_reset_postdata();
						endif; ?>

						</div>