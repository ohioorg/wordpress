<div class="three-column-carousel padtop">

					<!--FEATURED ARTICLES--->
					<?php foreach($blog_related_articles as $rel_article): 
						// variable must NOT be called $post!!!
						//here you can pretty much grab any field for the Article by using $rel_article->ID; in ACF's get_field, etc, you do: $image = get_field('image', $rel_article->ID);
			
						$aid = $rel_article->ID;
			
						//Headlines
						$rel_link = get_the_permalink($rel_article->ID);
						$rel_title = get_the_title($rel_article->ID);
						$rel_head = get_field('blog-headline', $rel_article->ID);
						$rel_sub = strip_tags(get_field('blog-subtitle', $rel_article->ID));
						$rel_body = strip_tags(get_field('blog-body', $rel_article->ID));
						
						//Check for body first
						if (!($rel_body)):
							$rel_body = strip_tags(get_the_content($rel_article->ID));
						endif;
			
						$rel_exc = substr($rel_body, 0, 255);

						if ($rel_head) :
							$rel_title = $rel_head;
						endif;

						//Main Image & Caption
						if( have_rows('blog-image', $aid ) ): while( have_rows('blog-image', $aid) ): the_row();

							$rel_image_pull = get_sub_field('image', $aid);
							$rel_image = $blog_image_pull['sizes']['large'];
							$rel_image_url = get_sub_field('url', $aid);

							if (!($rel_image)):
								$rel_image = $rel_image_url;
							endif;

						endwhile; endif;
	
						//Show Category
							$cat_string ='';
							$blog_cats =  wp_get_post_terms(get_the_ID($rel_article->ID), 'category', array('orderby' => 'term_order' ) );
							foreach($blog_cats as $cat){ 
								$blog_category = $cat->name; 
								$blog_cat_slug = $cat->slug; 
								$cat_string .= $cat->name . ", ";
								$categories = rtrim($cat_string, ", ");
								break; 
							}
						
							//Show Region
							$region_string ='';
							$blog_region =  wp_get_post_terms(get_the_ID($rel_article->ID), 'region', array('orderby' => 'term_order' ) );
							foreach($blog_region as $reg){ 
								$blog_region = $reg->name; 
								$blog_region_slug = $reg->slug; 
								$region_string .= $reg->name . ", ";
								$regions = rtrim($region_string, ", ");
								break; 
							}
				?>

						<div class="listing white-back relative">
							<a class="link-wrap" href="<?php echo $rel_link; ?>">
								<div class="img-wrap square">
									<img src="<?php echo $rel_image; ?>" alt="<?php echo $rel_title;?>"  title="<?php echo $rel_title;?>">
								</div>
							</a>	
								<div class="text relative">
									<div class="circle-icon overlap">
										<div class="red-back circle">
											<i class="i-<?php echo $blog_region_slug; ?> z-index white" style="z-index: 100000;" data-fa-transform="shrink-6"></i>
										</div>
									</div>
									<a class="link-wrap" href="<?php echo $rel_link; ?>">
									<div class="inner">
									<div class="head-24"><?php echo $rel_title;?></div>
									<p class="font-12"><?php echo $rel_exc; ?> &hellip;</p>
									<p class="font-14 bold"><i class="fas fa-map-marker-alt foam"></i> <?php echo $regions ;?></p>
									<p class="font-14 bold"><i class="fas fa-binoculars green"></i> <?php echo $categories ;?></p>
									</div>
									</a>
								</div>
								
						</div>

		<?php endforeach; ?> 

		</div>