<nav id="header" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

<div id="inner-header" class="cf">

<div class="col-menu center">
<div id="menu">

	<a class="open-menu">
		<i class="fas fa-2x fa-bars"></i>
	</a>
	
	<div id="side-menu">
		<ul class="vert-menu-wrap">
			<li class="vert-menu-item">
				<i class="fas fa-2x fa-binoculars"></i>
				<div class="vert-menu-details">
					<div class="vert-menu-label">Things To Do</div>
					<?php wp_nav_menu(array(
							 'container' => false,                          
							 'menu' => __( 'The Main Menu', 'bonestheme' ),  
	 						 'theme_location' => 'main-nav',  
					)); ?>
				</div>
			</li>
			<li class="vert-menu-item">
				<i class="fas fa-2x fa-building"></i>
				<div class="vert-menu-details">
					<div class="vert-menu-label">Where to Stay</div>
					<?php wp_nav_menu(array(
							 'container' => false,                           
							 'menu' => __( 'Lodging', 'bonestheme' ),                   
					)); ?>
				</div>
			</li>
			<li class="vert-menu-item">
				<i class="i-ohio" style="width: .875em; font-size: 2.8em; position: relative; top: 8px;"></i>
				<div class="vert-menu-details">
					<div class="vert-menu-label">Regions</div>
					<?php wp_nav_menu(array(
							 'container' => false,                           
							 'menu' => __( 'Regions', 'bonestheme' ),                 
					)); ?>
				</div>
			</li>
			<li class="vert-menu-item">
				<i class="fas fa-2x fa-sun"></i>
				<div class="vert-menu-details">
					<div class="vert-menu-label">Seasons</div>
					<?php wp_nav_menu(array(
							 'container' => false,                           
							 'menu' => __( 'The Main Menu', 'bonestheme' ),                 
					)); ?>
				</div>
			</li>
			<li class="vert-menu-item">
				<i class="fas fa-2x fa-calendar-alt"></i>
				<div class="vert-menu-details">
					<div class="vert-menu-label">Events</div>
					<?php wp_nav_menu(array(
							 'container' => false,                           
							 'menu' => __( 'The Main Menu', 'bonestheme' ),              
					)); ?>
				</div>
			</li>
			<li class="vert-menu-item">
				<i class="fas fa-2x fa-info"></i>
				<div class="vert-menu-details">
					<div class="vert-menu-label">Visitor's Info</div>
					<?php wp_nav_menu(array(
							 'container' => false,                           
							 'menu' => __( 'About Us Menu', 'bonestheme' ),                
					)); ?>
				</div>
			</li>
			<li class="vert-menu-item">
				<i class="fas fa-2x fa-pencil-alt"></i>
				<div class="vert-menu-details">
					<div class="vert-menu-label">Ohio Stories</div>
					<?php wp_nav_menu(array(
							 'container' => false,                           
							 'menu' => __( 'The Main Menu', 'bonestheme' ),  
                 
					)); ?>
				</div>
			</li>
			<li class="vert-menu-item">
				<i class="fas fa-2x fa-tag"></i>
				<div class="vert-menu-details">
					<div class="vert-menu-label">Deals</div>
					<?php wp_nav_menu(array(
							 'container' => false,                           
							 'menu' => __( 'The Main Menu', 'bonestheme' ),  
							               
					)); ?>
				</div>
			</li>
			
		</ul>
		<div class="details-wrap texture-light"></div>
	</div>


	
</div>
</div>

<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
<div class="col-logo center">
<div class="logo-main" itemscope itemtype="http://schema.org/Organization">
	<a href="<?php echo home_url(); ?>">
	<img class="icon-show" src="<?php echo get_template_directory_uri(); ?>/library/logo/OhioLogo-HorizColor.svg" width="180">
	<img class="desk-hide" src="<?php echo get_template_directory_uri(); ?>/library/logo/OhioLogo-VerticalColor.svg" width="160">
	</a>
</div>
<div class="logo-scroll">
	<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/library/logo/logo-tagline.svg" width="180"></a>
</div>
<div class="relative"><br></div>
</div>

<div class="col-nav">
	<div class="col-3-full desk-hide">
		<a href="<?php echo get_site_url(); ?>/things-to-do/"><div class="nav-item"><span>Things To Do</span></div></a>
	</div>
	<div class="col-3-full desk-hide">
		<a href="<?php echo get_site_url(); ?>/hotels-lodging/"><div class="nav-item"><span>Where To Stay</span></div></a>
	</div>
	<div class="col-3-full desk-hide">
		<a href="<?php echo get_site_url(); ?>/events/"><div class="nav-item"><span>Events</span></div></a>
	</div>
	<div class="col-3-full desk-hide">
		<a href="<?php echo get_site_url(); ?>/blog/"><div class="nav-item"><span>Trip Ideas</span></div></a>
	</div>
	<div class="clear"></div>
</div>

<div class="col-search center">
<div id="search">

	<a class="open-search">
		<i class="fas fa-2x fa-search"></i>
	</a>
	
	<div id="search-menu">
	<div id="search-wrap">
		<ul class="vert-menu-wrap">
			<li class="vert-menu-item">
				<i class="fas fa-2x fa-globe"></i>
				<div class="vert-menu-details">
					<div class="head-42 handy">Explore Ohio</div>
					<p>Search thousands of destinations, articles, events and more to find the perfect trip planning advice for you:</p>
					<?php echo get_search_form(); ?>
				</div>
			</li>
			<li class="vert-menu-item"><i class="fas fa-2x fa-building"></i></li>
			<li class="vert-menu-item"><i class="fas fa-2x fa-calendar"></i></li>
		</ul>
		<div class="details-wrap texture-light"></div>
		<div class="clear"></div>
	</div>
	</div>


	
</div>
</div>
	

<div class="clear"></div>
	
</div>
	
</nav>

<div id="side-menu-overlay" class="transition-5"></div>
<div id="search-menu-overlay" class="transition-5"></div>