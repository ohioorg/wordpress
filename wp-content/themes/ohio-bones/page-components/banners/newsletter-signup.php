<section class="full green-back">
	<div class="inner cf">
		<div class="col-4">
			<div class="relative"> &nbsp;
				<img src="/wordpress/wp-content/uploads/2018/03/covers-2_1.png" class="absolute" style="top: -60px;" width="90%">
			</div>
		</div>
		<div class="col-5">
			<div class="head-36 handy white">Download the Guide!</div>
			<p class="light"> View or Order Your Free Ohio Travel Guide Now!</p>
		</div>
		<div class="col-3">
			<a href="http://www.ohio.org/order-guide"><button class="large red-back">View or Order</button></a>
		</div>
	</div>
</section>