<section id="events-newsletter" class="full foam-back banner">
	<div class="inner cf">
		<div data-match-height="form" class="col-8 tab-6 phone-12">
			<div class="head-48 handy white center">Sign-up today!</div>
			<div class="white center"> Get updates on the best Ohio events, plus great deals and travel inspiration!</div>
			<?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]'); ?>
			<div class="clear"></div>
		</div>
		<div class="col-4 tab-6 phone-12">
			<div data-match-height="form" class="relative banner-image"> &nbsp;
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/EventsCover.png" class="absolute absbottom" style="bottom: -30px;" width="90%">
			</div>
		</div>
		<div class="clear"></div>
	</div>
</section>