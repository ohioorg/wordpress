<aside id="sidebar" class="red-back white texture-dark pad-30 relative ">
					
					<div class="col-12-full tab-6">
					<div class="white center padtop">
					<div class="head-42 handy">
						find stories
					</div>
					<p>
						Find what you're looking for by searching the thousands of great articles full of Ohio travel inspiration:
					</p>
					</div>
					<form role="search" method="post" class="search-form" action="<?php echo get_site_url(); ?>/blog/?s=<?php echo $search; ?>">
						<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="hidden" name="post_type" value="post" />
							<input type="search" class="search-field" placeholder="Search …" value="<?=($search) ? $search : '';?>" name="search">
						</label>
						<input type="submit" class="search-submit" value="Search">
					</form>
					
					<div id="categories" class="sidebar-section widget">
					<div class="sidebar-label">
						<i class="fas fa-check"></i> Categories
					</div>
					<ul class="clean">
					<?php wp_list_categories(array (
						taxonomy => 'category',
						show_count => 0,
						title_li => '',
					)); ?>
					</ul>
					</div>
				
					<div id="regions" class="sidebar-section widget">
					<div class="sidebar-label">
						<i class="fas fa-map-marker-alt"></i> Regions
					</div>
					<ul class="clean">
					<?php wp_list_categories(array (
						taxonomy => 'region',
						show_count => 0,
						hide_empty => 'true',
						title_li => '',
					)); ?>
					</ul>
					</div>
				    </div>
					
					<div class="col-12-full tab-6">
					<div id="cities" class="sidebar-section widget">
					<div class="sidebar-label">
						<i class="i-ohio"></i> City
					</div>
					<ul class="clean">
					<?php wp_list_categories(array (
						taxonomy => 'city',
						show_count => 0,
						hide_empty => 'true',
						title_li => ''
					)); ?>
					</ul>
					</div>
					
					<div id="seasons" class="sidebar-section widget">
					<div class="sidebar-label">
						<i class="fas fa-sun"></i> Seasons
					</div>
					<ul class="clean">
					<?php wp_list_categories(array (
						taxonomy => 'season',
						show_count => 0,
						hide_empty => 'true',
						title_li => ''
					)); ?>
					</ul>
					</div>
					
					<div id="archive" class="sidebar-section widget">
					<div class="sidebar-label">
						<i class="fas fa-calendar-alt"></i> Archives
					</div>
					<ul class="clean widget_archive">
					<?php wp_get_archives(array (
						type => 'monthly',
						show_count => 0,
						hide_empty => 'true',
						title_li => ''
					)); ?>
					</ul>
					</div>
					</div>
					<div class="clear"></div>

				</aside>