<script>
	
jQuery(function(){

// Updates Number Shown at top of Page
function updateListingsNumber(grid){
	$('#listings-number').text($(grid.isotope('getFilteredItemElements')).filter(':not(.featured-article)').length);
}

// Initialize Isotope Filtering
var $grid = $('#grid').isotope({
  itemSelector: '.iso-listing',
  layoutMode: 'fitRows',
  filter: function() {
    return qsRegex ? $(this).text().match( qsRegex ) : true;
  },
  getSortData: {
	title: '[data-title]',
	rating: '[data-rating] parseFloat'
  }
});
  updateListingsNumber($grid);
	
// Keyword Search
var qsRegex;
var $quicksearch = $('.quicksearch').keyup( debounce( function() {
  qsRegex = new RegExp( $quicksearch.val(), 'gi' );
  $grid.isotope();
  updateListingsNumber($grid);
}, 200 ) );

// debounce so filtering doesn't happen every millisecond
function debounce( fn, threshold ) {
  var timeout;
  threshold = threshold || 100;
  return function debounced() {
    clearTimeout( timeout );
    var args = arguments;
    var _this = this;
    function delayed() {
      fn.apply( _this, args );
    }
    timeout = setTimeout( delayed, threshold );
  };
}

// Sort By Options
$('.sort-options').on( 'change', function() {
  var sortByValue = $(".sort-options option:selected").attr('data-sort-by');
  var sortAscOrder = $(".sort-options option:selected").attr('data-sort-asc');
  var ascOrder = true;
  if(sortAscOrder === 'no'){ ascOrder = false; }
  $('#grid').isotope({ sortBy: sortByValue, sortAscending: ascOrder });
  updateListingsNumber($grid);
});		

// Region Map 
$('#filter-map').on( 'click', '.filter-map-link', function() {
	  
  $('.filter-show-all').removeClass('none');
  $('.filter-map-link').removeClass('active');
	
  $(this).toggleClass('active');
  var filterValue = $( this ).attr('data-filter');
  $('#grid').isotope({ filter: filterValue });
  updateListingsNumber($grid);
});

// Show All button for map
$('.filter-show-all').click(function() {
	var filterValue = $( this ).attr('data-filter');
	$('#grid').isotope({ filter: filterValue });
	
	$(this).addClass('none');
	$('.filter-map-link').removeClass('active');
	updateListingsNumber($grid);
});

// City Dropdown
$('#filter-select').change( function() {
	var filterValue = this.value;
    $('#grid').isotope({ filter: filterValue });
	updateListingsNumber($grid);
  });
	
//Checkboxes
var $checkboxes = $('#filter-by-rating input');

$checkboxes.change( function() {
  // map input values to an array
  var inclusives = [];
  // inclusive filters from checkboxes
  $checkboxes.each( function( i, elem ) {
    // if checkbox, use value if checked
    if ( elem.checked ) {
      inclusives.push( elem.value );
    }
  });

  // combine inclusive filters
  var filterValue = inclusives.length ? inclusives.join(', ') : '*';
  console.log(filterValue);
  $grid.isotope({ filter: filterValue });
	
  $('#rating-all').click(function(){
	$('#filter-by-rating input').removeAttr('checked');
  });
	
  updateListingsNumber($grid);
});

	
});

</script>

<aside id="filters" class="red-back white texture-dark pad-30 relative">
<div class="circle-icon">
<span class="fa-layers fa-fw">
	<i class="fas fa-circle orange"></i>
	<i class="fa-inverse fas fa-filter" data-fa-transform="shrink-6"></i>
</span>
</div>
	
<div class="col-12 tab-4 phone-12"><div class="head-54 handy center">filter listings</div></div>
<div class="col-12 tab-8 phone-12"><p class="center"> Find what you’re looking for using the options below to narrow down the thousands of Ohio destinations to explore:</p></div>
<div class="clear"></div>
	
<div class="tab-hide">
						
	<div class="sidebar-section">
		<div class="sidebar-label"><i class="fas fa-sort"></i> Sort</div>

			<form id="sort-by">
				<div class="sidebar-sub-label"> Sort by </div>
				<div class="relative">
				<select name="sortListings" class="sort-options relative">
					<option data-sort-by="" data-sort-asc="">Choose an option</option>
					<option data-sort-by="title" data-sort-asc="yes">Alphabetical (A to Z)</option>
					<option data-sort-by="title" data-sort-asc="no">Alphabetical (Z to A)</option>
					<option data-sort-by="rating" data-sort-asc="no">Highest Rated</option>
					<option data-sort-by="rating" data-sort-asc="yes">Lowest Rated</option>
					<option data-sort-by="*" data-sort-asc="no">Random</option>
				</select>
				<i class="fas fa-sort field-icon"></i>
				</div>
			</form>
	</div>
						
	<div class="sidebar-section">
		<div class="sidebar-label"><i class="fas fa-map"></i> Location</div>

			<div class="sidebar-sub-label bold"> By Region </div>

			<?php include( locate_template( '/page-components/sidebars/map.php', false, false ) ); ?>

			<form id="filter-by-city">
				<div class="sidebar-sub-label bold"> By City </div>
				<div class="relative">
				<select id="filter-select">
						<option class="select-opt" data-filter="*">Select a city</option>
					<?php //Show City
						$terms = get_terms('city');
							foreach( $terms as $term ) : 
								$term_slug = $term->slug;
								$term_name = $term->name;
						?>
						<option class="select-opt" value=".<?php echo $term_slug; ?>"><?php echo $term_name; ?></option>
					<?php endforeach; ?>
				</select>
				<i class="fas fa-map-marker-alt field-icon"></i>
				</div>
			</form>
	</div>
					
	<div class="sidebar-section">
		<div class="sidebar-label"><i class="fas fa-search"></i> Search</div>

			<form id="filter-by-keyword">
				<div class="sidebar-sub-label bold"> By Keyword </div>
				<div class="relative">
				<input class="quicksearch" type="text" placeholder="Term Name" >
				<i class="fas fa-search field-icon"></i>
				</div>
			</form>
	</div>
	
	<div class="sidebar-section">
	<div class="sidebar-label"><i class="fab fa-tripadvisor"></i> Rating</div>

		<form id="filter-by-rating">
			<div class="cat-checkbox padtop">
				<input type="checkbox" id="rating-all" value="*"> 
				<label class="bold" for="rating-all">All Ratings</label>
			</div>
			<div class="cat-checkbox padtop">

				<input type="checkbox" id="rating-5" value=".rating-5"> 
				<label class="bold" for="rating-5">
				<i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i></label>
			</div>
			<div class="cat-checkbox padtop">

				<input type="checkbox" id="rating-4" value=".rating-4">
				<label class="bold" for="rating-4">
				<i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-circle"></i></label>
			</div>
			<div class="cat-checkbox padtop">

				<input type="checkbox" id="rating-3" value=".rating-3">
				<label class="bold" for="rating-3">
				<i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></label>
			</div>
			<div class="cat-checkbox padtop">

				<input type="checkbox" id="rating-2" value=".rating-2">
				<label class="bold" for="rating-2">
				<i class="far fa-dot-circle"></i><i class="far fa-dot-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></label>
			</div>
			<div class="cat-checkbox padtop">

				<input type="checkbox" id="rating-1" value=".rating-1">
				<label class="bold" for="rating-1">
				<i class="far fa-dot-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i><i class="far fa-circle"></i></label>
			</div>
		</form>
	</div>
					
	<div class="sidebar-section margin-bottom-30">
		<div class="sidebar-label"><i class="fas fa-check"></i> Categories</div>

			<form id="filter-by-category">
				<div class="cat-checkbox padtop"><input type="checkbox" id="allCategories" name="allCategories" value="allCategories"><label class="bold" for="allCategories">All Categories</label></div>
				<?php echo get_terms_chekboxes('category', $args = array('hide_empty'=>true)); ?>
			</form>
	</div>
					
	
									
	</div>
</aside>