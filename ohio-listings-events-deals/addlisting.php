<?php

# addevent.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Add Listing</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			<label>Event Name</label>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-left">
			<label>Physical Address</label>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-right">
			<label>Physical Address 2</label>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-left">
			<label>City</label>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-right">
			<div class="float">
				<label>State</label>
				<input type="text" class="fill" style="width:60px;" />
			</div>
			<div class="float">
				<label>Zip</label>
				<input type="text" class="fill" />
			</div>
		</div>
		
		<div class="global-full">
			<label>Publish my listing under:</label>
			<br><?php include 'cities.php'; ?>
		</div>
		
		<div class="global-full">
			<label>Describe your listing here:
				<br>(400 characters minimum, 1500 characters maximum)</label>
			<br><em>Descriptions are required, your listing will be rejected without one.</em>
			<br><textarea class="fill"></textarea>
		</div>
		
		<div class="global-full">
			<h2>I think I should be listed under these categories for <span class="color-red font-handy-bold">Ohio.org</span>:</h2>
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Arts / Culture
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Art Exhibits/Exhibitions</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Art Museums and Galleries</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Botanical</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Theatres</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Breweries & Distilleries
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Craft Breweries</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Distilleries</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Family Fun
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>AgriTourism</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Amusement Parks/Fun Centers</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Day Spas</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Educational</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Halls of Fame</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Interactive Museums</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Lake and River Cruises</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Scenic Trains</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Science</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Tours</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Water Parks</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Zoo/Animal Parks</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Food
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Food Tours</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Restaurants</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				History
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Amish Heritage</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Appalachia</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Aviation/Space Exploration</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Black History</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Civil War</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Education/Research</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Heritage Area Tours</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Historic Downtowns</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Historic Sites</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Lighthouses</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Museums</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Native Americans</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Presidential</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Scenic Byways</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Lodging
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Bed & Breakfasts</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Hotels, Motels, & Inns</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Resorts, Retreats, & Lodges</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>RV Parks & Campgrounds</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Vacation Rentals & Cabins</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Outdoors
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Beaches</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Birding</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Canoeing</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Fishing</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Hiking</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Horseback Riding</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Hunting</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Local & Regional Parks</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>National Parks & Forests</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Ohio State Parks & Natural Areas</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Recreational Boating</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Skiing</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Ziplines</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Shopping
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Amish</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Antiques</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Arts & Crafts</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Farm Markets</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Flea Markets</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Malls</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Outlet Shopping</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Pottery & Glass</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Shopping Districts</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Specialty Shops</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Sports & Recreation
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Baseball</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Basketball</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Biking</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Extreme Sports</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Football</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Golf Courses</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Hockey</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Horse Racing</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Motorsports</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Other</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Soccer</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Stadiums & Arenas</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Visitor Services
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Convention & Meeting Facilities</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Convention & Visitor Bureaus</span>
				</label>
			</div>
			
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Transportation</span>
				</label>
			</div>
			
		</div>
		
		<div class="global-full">
			<label class="border-top">
				Wineries
			</label>
			<div class="float half">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Wineries</span>
				</label>
			</div>
			
			<div class="float half">
				<!--
				<label class="container-checkbox clear">
					xxxxxxxxxx
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>xxxxxxxxxx</span>
				</label>
				-->
			</div>
			
		</div>
		
		<div class="global-full align-center">
			<p>&nbsp;</p>
			<button>Save</button>
			<p><a href="#">Cancel</a></p>
		</div>
		
	</div>
	
</div>
