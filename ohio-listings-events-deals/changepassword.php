<?php

# changepassword.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Change Password</h2>
		<p>New passwords must be at least six characters long and may not include spaces.</p>
	</div>
	
	<div class="global-body">
		
		<div class="global-left">
			<label>Current Password</label>
			<br>
			<input type="password" class="fill" />
		</div>
		<div class="global-right"></div>
		
		<div class="global-left">
			<label>New Password</label>
			<br>
			<input type="password" class="fill" />
		</div>
		<div class="global-right"></div>
		
		<hr />
		
		<div class="global-left pad-top">
			<label>Confirm Password</label>
			<br>
			<input type="password" class="fill" />
		</div>
		<div class="global-right"></div>
		
		<div class="global-full align-center pad-top pad-bottom">
			<p>&nbsp;</p>
			<button>Save</button>
			<br>&nbsp;
			<br><a href="#">Cancel</a>
		</div>
		
	</div>
	
</div>

