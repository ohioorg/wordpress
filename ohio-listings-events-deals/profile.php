<?php

# profile.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Account Profile</h2>
	</div>
	
	<div class="global-body">
		
		<div class="profile-left">
			<label>First Name</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>Last Name</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>Title</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>Business Name</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>Street Address</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>Address Line 2</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>City</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<div class="float">
				<label>State</label>
				<input type="text" class="fill" style="width:60px;" />
			</div>
			<div class="float">
				<label>Zip</label>
				<input type="text" class="fill" />
			</div>
		</div>

		<div class="profile-left">
			<label>Phone</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>Fax</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>Email</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-full">
			<h2>Update Credentials</h2>
			<p>New passwords must be at least six characters long and may not include spaces. Usernames must be an email address.</p>
		</div>

		<div class="profile-full">
			<label>Username / Email Address</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-left">
			<label>Current Password</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-right hide-res">&nbsp;</div>

		<div class="profile-left">
			<label>New Password</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-right ">
			<label>Confirm Password</label>
			<input type="text" class="fill" />
		</div>

		<div class="profile-full align-center">
			<p>&nbsp;</p>
			<button>Save</button>
			<p><a href="#">Cancel</a></p>
		</div>
		
	</div>
	
</div>
