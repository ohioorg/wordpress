<?php
/*
Plugin Name: Ohio Listings, Events, and Deals v0.1
Plugin URI: http://www.ohio.org
description: Ohio Listings, Events, and Deals
Version: 0.1
Author: State Of Ohio
Author URI: http://www.ohio.org
License: GPL2
*/

#phpinfo();

#include 'dbconnect.php';

include 'header.php';

#include 'login.php';

?>

<section data-page="#">
	
	<div class="responsive">
	
<?php
	//echo basename($_SERVER['PHP_SELF']);
	
	if ( isset($_GET['page']) && !empty($_GET['page']) && $_GET['page'] != ''){
		$page = $_GET['page'];
	} else {
		$page = 'dashboard.php';
	}
	
	include $page;
	
	//include 'dashboard.php';

	//include 'profile.php';
	
?>

	</div><!-- end responsive -->
	
</section><!-- end data-page -->

<?php

include 'footer.php';

include 'listing_export_all.php';

include 'import_xml.php';



