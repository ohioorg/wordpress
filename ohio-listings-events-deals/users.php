<?php

# users.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>User Management</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			<label>Find User</label>
			<br>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-full">
			<label>Filiter by type:</label>
			<br>
			<select name="usertype_select">
				<option value="">All</option>
				<option value="8">Approver</option>
				<option value="2">Data</option>
				<option value="3">Listing</option>
			</select>
		</div>
		
		<div class="global-full pad-top pad-bottom">
			<button>Search</button>
		</div>
		
		<div class="global-full">
			<table style="width:100%;">
				<thead>
					<tr>
						<td style="background-color:#cccccc;color:#777777;"><b>First Name</b></td>
						<td style="background-color:#cccccc;color:#777777;"><b>Last Name</b></td>
						<td style="background-color:#cccccc;color:#777777;"><b>Username</b></td>
						<td style="background-color:#cccccc;color:#777777;"><b>Password</b></td>
						<td style="background-color:#cccccc;color:#777777;"><b>Manage</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7635">Amy</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7635">Summers</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7635">amy.summers@development.ohio.gov</a>
						</td>
						<td>Summ20051</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9876">Bobby</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9876">Taylor</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9876">Bobby.Taylor@MilesPartnership.com</a>
						</td>
						<td>spLeNdOr3</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10691">Braeden</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10691">Mincher</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10691">braeden.mincher@development.ohio.gov</a>
						</td>
						<td>SEcrets33</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/23414">Chris</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/23414">Bowsher</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/23414">christopher.bowsher@development.ohio.gov</a>
						</td>
						<td>5a4985ebb3</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/24891">Chris</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/24891">Mowder</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/24891">Christopher.Mowder@development.ohio.gov</a>
						</td>
						<td>ohio123</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19518">Columbus</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19518">Airport2</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19518">columbusairport_api2</a>
						</td>
						<td>1lhpihzo3jwmhlycLess</td>
						<td><a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19518/edit_api">manage products</a></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10240">Colin</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10240">Payton</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10240">cpayton@glpublishing.com</a>
						</td>
						<td>glpublishing</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/81">Claudia</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/81">Plumley</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/81">cplumley@glpublishing.com</a>
						</td>
						<td>isaball</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/5894">Sarah</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/5894">Cuiksa</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/5894">cuiksa@clevelandmagazine.com</a>
						</td>
						<td>cuiksa</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19297">David</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19297">King</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19297">David.King@development.ohio.gov</a>
						</td>
						<td>chevy1077</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/8401">Ohio</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/8401">API</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/8401">discoverohio_com_api</a>
						</td>
						<td>iq4vt9pq2kruzk1t</td>
						<td><a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/8401/edit_api">manage products</a></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9146">David</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9146">Magyar</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9146">dmagyar</a>
						</td>
						<td>ohio216</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/6960">Donald</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/6960">Nelson</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/6960">dnelson1175@gmail.com</a>
						</td>
						<td>cornedbeef</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/618">Eric</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/618">Herzog</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/618">eric.herzog@development.ohio.gov</a>
						</td>
						<td>katherine</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7717">Hannah</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7717">Kelbaugh</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7717">Hannah.Kelbaugh@milespartnership.com</a>
						</td>
						<td>ohioDE15</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/5497">Lute</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/5497">Harmon</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/5497">harmonjr@glpublishing.com</a>
						</td>
						<td>harmonjr</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/4347">Hiram</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/4347">Hubbard</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/4347">hiram.hubbard@development.ohio.gov</a>
						</td>
						<td>password</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/14152">Ileana</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/14152">Frascone</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/14152">Ileana.Frascone@MilesPartnership.com</a>
						</td>
						<td>ff18ae88e9</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/15282">Jason</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/15282">Brill</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/15282">jbrill@ohiomagazine.com</a>
						</td>
						<td>glpohio</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10773">Jessica</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10773">Greathouse</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/10773">jgreathouse@ohiomagazine.com</a>
						</td>
						<td>mustang12</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9147">Jassen</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9147">Tawil</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9147">jtawil</a>
						</td>
						<td>letmein606</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7385">jt</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7385">jt</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/7385">jtawil@ohiocontactcenter.com</a>
						</td>
						<td>Password1</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9381">Kelsey</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9381">Wagner</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/9381">kwagner@glpublishing.com</a>
						</td>
						<td>mm2104364</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19820">Lauren</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19820">Seckel</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/19820">lauren.seckel@development.ohio.gov</a>
						</td>
						<td>Julia032013!</td>
						<td></td>
					</tr>
							<tr>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/23011">Meg</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/23011">Schultz</a>
						</td>
						<td>
							<a class="edit" href="http://listings.ohio.org/action/editsection/usermanagement/23011">meg.schultz@development.ohio.gov</a>
						</td>
						<td>ohio2017</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		
		</div>
		
		<div class="global-full">
			<div class="float-left">
				25358 results found.
			</div>
			<div class="float-right">
				1 of 1015  <a href="#">Next</a>
			</div>
		</div>
		
		<div class="global-full align-center pad-top">
			<button>Add User</button>
		</div>
		
	</div>
	
</div>

