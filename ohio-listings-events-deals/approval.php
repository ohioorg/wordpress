<?php

# approval.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Approval Queue</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			Use the filter dropdowns below and sortable columns to locate a specific record. Click the listing name to review the submitted information.
			<br>&nbsp;
			<br>
			<div class="third">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Show events only</span>
				</label>
			</div>
			<div class="third">
				Filter By City&nbsp; <select>
					<option>All</option>
				</select>
			</div>
			<div class="third">
				Filter By Region &nbsp; <select>
					<option>All</option>
					<option>Amish</option>
					<option>Central</option>
					<option>Northeast</option>
					<option>Northwest</option>
					<option>Southeast</option>
					<option>Southwest</option>
				</select>
			</div>
		</div>

		<div class="global-full pad-top">
			<em>There are no pending changes requiring approval.</em>
		</div>
		
	</div>

</div>
