<?php

# forgotpassword.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Forgot Password?</h2>
		<p>If you know the email for this account, enter it below. A password reset link will be sent to that email address.</p>
	</div>
	
	<div class="global-body">
		
		<div class="global-left">
			<label>Username / Email Address</label>
			<br>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-full clear align-center pad-top pad-bottom">
			<button>Email Password</button>
		</div>
		
	</div>
	
</div>

