<?php

# header.php

$header = 'home';// listing | home

?>

<!DOCTYPE html>
<html>

<head>
	<title>Ohio Listings, Events, and Deals</title>
	
	<link href="style.css" rel="stylesheet" type="text/css">
	<link href="datepicker.css" rel="stylesheet" type="text/css">
	
	<!-- -->
	<link href="http://fonts.googleapis.com/css?family=Nunito+Sans:100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic,100,200,300,400,500,600,700,800,900" rel="stylesheet" type="text/css">
	<!-- -->
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.css">
	
	
	
	<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">-->
	 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	
	<script src="js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
	
</head>

<body>

<div class="main">

<header>
	
	<div class="responsive">
		
		<div class="header-left">
			
			<a href="index.php?page=dashboard.php"><img src="images/logo-tagline.svg" class="logo-header" /></a>
		
		</div>
		
		<!--
		<div class="header-center">
			&nbsp;
		</div>
		-->
		
		<?php
		
		if ( isset($_GET['page']) && !empty($_GET['page']) && $_GET['page'] != ''){
			$page = $_GET['page'];
		} else {
			$page = 'dashboard.php';
		}
		
		if (isset($page) && $page != 'login.php'){
		
		?>
		
		<div class="header-right">
			
			<div style="padding-top:6px;">Welcome <a href="index.php?page=profile.php" class="color-white font-bold">Peter</a></div>

			<small>Last log-in: May 3, 2018 3:55 PM</small>
			
			<br><a href="index.php?page=changepassword.php" class="color-white">change password</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.php?page=login.php" class="color-white">log out</a>
				<!--<button>LOG OUT</button>-->

		</div>
		
		<?php
		
		} else {
		
		?>
		
		<div class="header-right hide">
			
			<div style="padding-top:6px;">Welcome <a href="index.php?page=profile.php" class="color-white font-bold">Peter</a></div>

			<small>Last log-in: May 3, 2018 3:55 PM</small>
			
			<br><a href="index.php?page=changepassword.php" class="color-white">change password</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.php?page=login.php" class="color-white">log out</a>
				<!--<button>LOG OUT</button>-->

		</div>
		
		<?php
		
		}
		
		?>
		
		
		
	</div><!-- end .responsive -->
	
	<nav class="clear">
	
		<div class="responsive">
		
			<div class="nav">
				<a href="index.php?page=dashboard.php">Dashboard</a>
				<a href="index.php?page=addevent.php">Add Event</a>
				<a href="index.php?page=addlisting.php">Add Listing</a>
				<a href="index.php?page=approval.php" style="margin-right:-1px;">Approval Queue</a>
					<span>(0/0)&nbsp;&nbsp;</span>
				<a href="index.php?page=export.php">Export</a>
				<a href="index.php?page=collections.php">Collections</a>
				<a href="index.php?page=reports.php">Reports</a>
				<a href="index.php?page=blast.php">Blast</a>
				<a href="index.php?page=users.php">Users</a>
				<a href="index.php?page=approveusers.php">Approve Users</a>
				<a href="TourismOhio_DatabaseManual.pdf" target="_blank">How-To Manual</a>
			</div>
		
		</div>
	
	</nav>
	
</header>

<div class="header-spacer">
	&nbsp;
</div>

<section>
	
	<div class="responsive">
	
	<?php
	
	if ($header == '_home'){
	
	?>
	
		<div class="subheader-left">
			
			<img src="images/logo.png" class="logo-subheader" />
			
		</div>
		
		<div class="subheader-right">
			<p>Welcome Peter!</p>

			<p><a href="#">change password</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<button>LOG OUT</button></p>

			<p><small>Last log-in: May 3, 2018 3:55 PM</small></p>
		</div>
		
	<?php
	
	} else if ($header == '_listing'){
	
	?>
	
		<div class="subheader-left">
			
			<!--<img src="images/logo.png" class="logo-subheader" />-->
			
			<p>Where am I listed?</p>

			<p>Current Listing ID: 61</p>

			<p>#1 China Buffet</p>
		</div>
		
		<div class="subheader-right">
			<p>Welcome Peter!</p>

			<p>(change password) <button>LOG OUT</button></p>

			<p>Privacy & Usage Policy</p>

			<p>Approval Queue</p>

			<p>Duplicate Listing | Send Welcome Email</p>

			<p>RETURN TO DASHBOARD</p>

			<p>Last log-in: April 30, 2018 11:08 AM</p>
		</div>
		
	<?php
	
	}
	
	?>
	
	</div><!-- end .responsive -->
	
</section>