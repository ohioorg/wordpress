<?php

# register.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Register</h2>
		<p>Complete your user profile to create a login. Items with an asterisk (*) are required. All new user requests will be subject to approval.
			<br><a href="FAQ_Listing_Database_Administration_Guidelines.pdf" target="_blank">FAQs on Business & Event Listings</a>
		</p>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			
			<div class="float third">
				<label>First Name *</label>
				<br>
				<input type="text" class="fill" />
			</div>
			
			<div class="float third">
				<label>Last Name *</label>
				<br>
				<input type="text" class="fill" />
			</div>
			
			<div class="float third">
				<label>Title</label>
				<br>
				<input type="text" class="fill" />
			</div>
			
		</div>
		
		<div class="global-left clear">
			<label>Business Name *</label>
			<br>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-left clear">
			<label>Street Address *</label>
			<br>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-left clear">
			<label>Street Address 2 *</label>
			<br>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-left clear">
			<label>City *</label>
			<br>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-left clear">
			<div class="float">
				<label>State *</label>
				<br>
				<input type="text" style="width:60px;" />
			</div>
			<div class="float">
				<label>ZIP *</label>
				<br>
				<input type="text" />
			</div>
		</div>
		
		<div class="global-left clear">
			<div class="float">
				<label>Phone *</label>
				<br>
				<input type="text" />
			</div>
			<div class="float">
				<label>FAX</label>
				<br>
				<input type="text" />
			</div>
		</div>
		
		<div class="global-full">
			<label>What type of tourism experiences will you promote to someone traveling at least 50 miles to your location? (Please be specific) *</label>
			<br>
			<textarea class="fill"></textarea>
		</div>
		
		<div class="global-full-text pad-top">
			<em>Your email address will be used as your username.</em>
		</div>
		
		<div class="global-full">
			<label>Email Address *</label>
			<br>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-full-text">
			<em>Passwords must be at least six characters long and may not include spaces.</em>
		</div>
		
		<div class="global-left clear">
			<label>Password *</label>
			<br>
			<input type="password" class="fill" />
		</div>
		
		<div class="global-left clear">
			<label>Confirm Password *</label>
			<br>
			<input type="password" class="fill" />
		</div>
		
		<div class="global-full clear align-center pad-top pad-bottom">
			<br>&nbsp;
			<br>
			<button>Save</button>
			<br>&nbsp;
			<br><a href="#">Cancel</a>
		</div>
		
	</div>
	
</div>

