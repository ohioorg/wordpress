<?php

# addcollection.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Add Collection</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full pad-bottom">
			<label>Collection Name</label>
			<input type="text" class="fill" />
			
		</div>
		
		<div class="global-full pad-bottom">
			<label>Product</label>
			<br><select>
				<option>- Select -</option>
				<option>Ohio.org</option>
				<option>Ohio Travel Guide</option>
				<option>Ohio Calendar of Events</option>
			</select>
		</div>
		
		<div class="global-full pad-bottom">
			<div class="float"">
				<label>Start Date</label>
				<br><input id="collection-start-date" type="text" />
			</div>
			<div class="float"">
				<label>End Date</label>
				<br><input id="collection-end-date" type="text" />
			</div>
		</div>
		
		<div class="global-full-text">
			<label>Select one or more required sections</label>
		</div>
		
		<div class="global-full pad-bottom">
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>bizinfo</span>
			</label>
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>amenities</span>
			</label>
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>descriptions</span>
			</label>
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>photos</span>
			</label>
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>rates</span>
			</label>
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>documents</span>
			</label>
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>socialmedia</span>
			</label>
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>youtubelinks</span>
			</label>
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>eventdates</span>
			</label>
		</div>
		
		<div class="global-full align-center pad-top">
			<button>Save</button>
			<br>&nbsp;
			<br><a href="#">Delete</a>
			<br>&nbsp;
			<br><a href="#">Cancel</a>
		</div>
		
	</div>
	
</div>


