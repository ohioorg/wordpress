<?php

# main.php

#include 'dbconnect.php';

?>

<div class="dashboard content-border">
	
	<div class="global-header">
		<h2><!-- <i class="fa fa-cog"></i>--> Peter's Dashboard</h2>
	</div>
	
	<div class="global-body">
		
		<div class="dashboard-search">
			<label>Search Text</label>
			<input type="text" />
		</div>

		<div class="dashboard-search">
			<label>Product</label>
			<select>
				<option>All</option>
				<option>Product</option>
				<option>Product</option>
			</select>
			
			<label>Category</label>
			<select>
				<option>All</option>
				<option>Category</option>
				<option>Category</option>
			</select>
			
			<label>City</label>
			<select>
				<option>All</option>
				<option>City</option>
				<option>City</option>
			</select>
			
			<label>Region</label>
			<select>
				<option>All</option>
				<option>Region</option>
				<option>Region</option>
			</select>
			
			<label>OEF</label>
			<select>
				<option>All</option>
				<option>OEF</option>
				<option>OEF</option>
			</select>
		</div>
		
		<!--
		<input type="checkbox" />Active Only &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="checkbox" />Exclude Events &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="checkbox" />Unverified Only &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="checkbox" />Awaiting Approval Only
		-->
		
		<div class="dashboard-search">
			<label class="container-checkbox">Active Only
				<input type="checkbox" _checked="checked">
				<span class="checkmark"><span></span></span>
			</label>

			<label class="container-checkbox">Exclude Events
				<input type="checkbox">
				<span class="checkmark"><span></span></span>
			</label>

			<label class="container-checkbox">Unverified Only
				<input type="checkbox">
				<span class="checkmark"><span></span></span>
			</label>

			<label class="container-checkbox">Awaiting Approval Only
				<input type="checkbox">
				<span class="checkmark"><span></span></span>
			</label>
		</div>
		
		<div class="dashboard-search">
			<button>Search</button>
			<br>&nbsp;
		</div>
		
		<div class="dashboard-search">
			<hr />
		</div>
		
		<div class="dashboard-table">
			Search results: Page 1 of 709
			<br>&nbsp;
			<div class="dashboard-table-row header-row">
				<a class="d-col list-id">List ID</a>
				<a class="d-col property-name hi">Property Name</a>
				<a class="d-col address">Address</a>
				<a class="d-col phone">Phone</a>
				<div class="d-col listing-owner">Listing Owner</div>
				<div class="d-col users">Users</div>
			</div>
			
			<?php
			
			for ($d=1;$d<=10;$d++){
			
			?>
			
			<div class="dashboard-table-row">
				<div class="d-col"><?php echo $d; ?></div>
				<div class="d-col">
					My Ohio Listing
					<br>(2018-08-25 - 2018-08-25)
				</div>
				<div class="d-col">
					1234 Street Rd
					<br>Townsville, OH
					<br>43210
				</div>
				<div class="d-col">614-555-1234</div>
				<div class="d-col">Peter Fletcher</div>
				<div class="d-col">	
					u: aaaaa@bbbbbbbbbbbbb.com
					<br>p: Mypa$$w0rd
				</div>
			</div>
			
			<?php
			
			}
			
			?>
			
		</div>
		
		<div class="dashboard-footer">
			
			<div class="dashboard-footer-left">7088 results found.</div>
			
			<div class="dashboard-footer-right">
				<a href="#">Show All</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="#">Next</a>
			</div>
			
			<p>Red text indicates listing is not active for any products, and/or event record is expired.</p>
			
			<p>Recently updated/added listings appear inactive until approved. Please allow 72 hours for approval.</p>
		</div>
		
	</div>
	
</div>


