<?php

# approveusers.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Approve Users</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			<label>Find User</label>
			<br>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-full pad-top pad-bottom">
			<button>Search</button>
		</div>
		
		<div class="global-full">
			<table style="width:100%;">
				<thead>
					<tr>
						<td style="background-color:#cccccc;color:#777777;"><b>First Name</b></td>
						<td style="background-color:#cccccc;color:#777777;"><b>Last Name</b></td>
						<td style="background-color:#cccccc;color:#777777;"><b>Organization</b></td>
						<td style="background-color:#cccccc;color:#777777;"><b>Contact Info</b></td>
						<td style="background-color:#cccccc;color:#777777;"><b>Manage</b></td>
					</tr>
				</thead>
			</table>
		</div>
		
		<div class="global-full">
			<div class="float-left">
				0 results found.
			</div>
			<div class="float-right">
				1 of 0
			</div>
		</div>
		
	</div>
	
</div>

