<?php

# blast.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Collection Blast</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			<label>Pick A Collection</label>
			<br>
			<select>
				<option>SELECT</option>
			</select>
		</div>
		
		<div class="global-full pad-bottom">
			<label>Pick A Supertag</label>
			<br><small>Select Multiple: PC = Ctrl + click | Mac = Command + click</small>
			<br><select name="supertag_id[]" multiple="multiple" style="width:280px;height:275px;">
				<option value="10">AgriTourism</option>
				<option value="41">Amish</option>
				<option value="18">Amish Heritage</option>
				<option value="5">Amusement Parks/Fun Centers</option>
				<option value="42">Antiques</option>
				<option value="19">Appalachia</option>
				<option value="4">Art Exhibits/Exhibitions</option>
				<option value="1">Art Museums and Galleries</option>
				<option value="47">Arts &amp; Crafts</option>
				<option value="27">Aviation/Space Exploration</option>
				<option value="67">Baseball</option>
				<option value="68">Basketball</option>
				<option value="53">Beaches</option>
				<option value="34">Bed &amp; Breakfasts</option>
				<option value="66">Biking</option>
				<option value="57">Birding</option>
				<option value="25">Black History</option>
				<option value="3">Botanical</option>
				<option value="58">Canoeing</option>
				<option value="20">Civil War</option>
				<option value="1730">Convention &amp; Meeting Facilities</option>
				<option value="75">Convention &amp; Visitor Bureaus</option>
				<option value="39">Craft Breweries</option>
				<option value="15">Day Spas</option>
				<option value="1733">Distilleries</option>
				<option value="29">Education/Research</option>
				<option value="7">Educational</option>
				<option value="74">Extreme Sports</option>
				<option value="43">Farm Markets</option>
				<option value="17">Festivals &amp; Events</option>
				<option value="54">Fishing</option>
				<option value="44">Flea Markets</option>
				<option value="38">Food Tours</option>
				<option value="65">Football</option>
				<option value="64">Golf Courses</option>
				<option value="6">Halls of Fame</option>
				<option value="28">Heritage Area Tours</option>
				<option value="56">Hiking</option>
				<option value="21">Historic Downtowns</option>
				<option value="22">Historic Sites</option>
				<option value="69">Hockey</option>
				<option value="70">Horse Racing</option>
				<option value="63">Horseback Riding</option>
				<option value="32">Hotels, Motels, &amp; Inns</option>
				<option value="62">Hunting</option>
				<option value="14">Interactive Museums</option>
				<option value="13">Lake and River Cruises</option>
				<option value="30">Lighthouses</option>
				<option value="61">Local &amp; Regional Parks</option>
				<option value="1729">Local Resources</option>
				<option value="49">Malls</option>
				<option value="72">Motorsports</option>
				<option value="26">Museums</option>
				<option value="52">National Parks &amp; Forests</option>
				<option value="23">Native Americans</option>
				<option value="51">Ohio State Parks &amp; Natural Areas</option>
				<option value="1731">Other</option>
				<option value="45">Outlet Shopping</option>
				<option value="46">Pottery &amp; Glass</option>
				<option value="24">Presidential</option>
				<option value="60">Recreational Boating</option>
				<option value="33">Resorts, Retreats, &amp; Lodges</option>
				<option value="37">Restaurants</option>
				<option value="36">RV Parks &amp; Campgrounds</option>
				<option value="31">Scenic Byways</option>
				<option value="16">Scenic Trains</option>
				<option value="8">Science</option>
				<option value="48">Shopping Districts</option>
				<option value="55">Skiing</option>
				<option value="71">Soccer</option>
				<option value="50">Specialty Shops</option>
				<option value="73">Stadiums &amp; Arenas</option>
				<option value="2">Theatres</option>
				<option value="1728">Things to Do</option>
				<option value="12">Tours</option>
				<option value="1732">Transportation</option>
				<option value="35">Vacation Rentals &amp; Cabins</option>
				<option value="11">Water Parks</option>
				<option value="40">Wineries</option>
				<option value="59">Ziplines</option>
				<option value="9">Zoo/Animal Parks</option>
			</select>
		</div>
		
		<div class="global-full">
			<label>Select By Region</label>
			<br>
			<select>
				<option>All</option>
				<option>Amish</option>
				<option>Central</option>
				<option>Northeast</option>
				<option>Northwest</option>
				<option>Southeast</option>
				<option>Southwest</option>
			</select>
		</div>
		
		<div class="global-full">
			<label>Select By County</label>
			<br>
			<select name="county">
				<option value="">All</option>
				<option value="Adams">Adams</option>
				<option value="Allen">Allen</option>
				<option value="Ashland">Ashland</option>
				<option value="Ashtabula">Ashtabula</option>
				<option value="Athens">Athens</option>
				<option value="Auglaize">Auglaize</option>
				<option value="Belmont">Belmont</option>
				<option value="Brown">Brown</option>
				<option value="Butler">Butler</option>
				<option value="Carroll">Carroll</option>
				<option value="Champaign">Champaign</option>
				<option value="Clark">Clark</option>
				<option value="Clermont">Clermont</option>
				<option value="Clinton">Clinton</option>
				<option value="Columbiana">Columbiana</option>
				<option value="Coshocton">Coshocton</option>
				<option value="Crawford">Crawford</option>
				<option value="Cuyahoga">Cuyahoga</option>
				<option value="Darke">Darke</option>
				<option value="Defiance">Defiance</option>
				<option value="Delaware">Delaware</option>
				<option value="Erie">Erie</option>
				<option value="Fairfield">Fairfield</option>
				<option value="Fayette">Fayette</option>
				<option value="Franklin">Franklin</option>
				<option value="Fulton">Fulton</option>
				<option value="Gallia">Gallia</option>
				<option value="Geauga">Geauga</option>
				<option value="Greene">Greene</option>
				<option value="Guernsey">Guernsey</option>
				<option value="Hamilton">Hamilton</option>
				<option value="Hancock">Hancock</option>
				<option value="Hardin">Hardin</option>
				<option value="Harrison">Harrison</option>
				<option value="Henry">Henry</option>
				<option value="Highland">Highland</option>
				<option value="Hocking">Hocking</option>
				<option value="Holmes">Holmes</option>
				<option value="Huron">Huron</option>
				<option value="Jackson">Jackson</option>
				<option value="Jefferson">Jefferson</option>
				<option value="Knox">Knox</option>
				<option value="Lake">Lake</option>
				<option value="Lawrence">Lawrence</option>
				<option value="Licking">Licking</option>
				<option value="Logan">Logan</option>
				<option value="Lorain">Lorain</option>
				<option value="Lucas">Lucas</option>
				<option value="Madison">Madison</option>
				<option value="Mahoning">Mahoning</option>
				<option value="Marion">Marion</option>
				<option value="Medina">Medina</option>
				<option value="Meigs">Meigs</option>
				<option value="Mercer">Mercer</option>
				<option value="Miami">Miami</option>
				<option value="Monroe">Monroe</option>
				<option value="Montgomery">Montgomery</option>
				<option value="Morgan">Morgan</option>
				<option value="Morrow">Morrow</option>
				<option value="Muskingum">Muskingum</option>
				<option value="Noble">Noble</option>
				<option value="Ottawa">Ottawa</option>
				<option value="Paulding">Paulding</option>
				<option value="Perry">Perry</option>
				<option value="Pickaway">Pickaway</option>
				<option value="Pike">Pike</option>
				<option value="Portage">Portage</option>
				<option value="Preble">Preble</option>
				<option value="Putnam">Putnam</option>
				<option value="Richland">Richland</option>
				<option value="Ross">Ross</option>
				<option value="Sandusky">Sandusky</option>
				<option value="Scioto">Scioto</option>
				<option value="Seneca">Seneca</option>
				<option value="Shelby">Shelby</option>
				<option value="Stark">Stark</option>
				<option value="Summit">Summit</option>
				<option value="Trumbull">Trumbull</option>
				<option value="Tuscarawas">Tuscarawas</option>
				<option value="Union">Union</option>
				<option value="Van Wert">Van Wert</option>
				<option value="Vinton">Vinton</option>
				<option value="Warren">Warren</option>
				<option value="Washington">Washington</option>
				<option value="Wayne">Wayne</option>
				<option value="Williams">Williams</option>
				<option value="Wood">Wood</option>
				<option value="Wyandot">Wyandot</option>
			</select>
		</div>
		
		<div class="global-left">
			<label>Select By Zip Code</label>
			<br>
			<input type="text" maxlength="5" class="fill" />
		</div>
		
		<div class="global-right">
			&nbsp;
		</div>
		
		<div class="global-full">
			<label>Select A Group</label>
			<br>
			<div class="float">
				<label class="container-radio clear">Non-respondents only
					<input type="radio" name="radio" checked="checked">
					<span class="radio"><span>&nbsp;</span></span>
				</label>
			</div>
			<div class="float pad-left">
				<label class="container-radio clear">Respondents only
					<input type="radio" name="radio">
					<span class="radio"><span>&nbsp;</span></span>
				</label>
			</div>
			<div class="float pad-left">
				<label class="container-radio clear">All listings
					<input type="radio" name="radio">
					<span class="radio"><span>&nbsp;</span></span>
				</label>
			</div>
		</div>
		
		<div class="global-full">
			<label>Choose A Letter</label>
			<br>
			<select id="blast_letter_id" name="letter_id" class="required">
				<option value="">SELECT</option>
				<option value="11">2018 one-off Ohio Travel Guide verification letter</option>
				<option value="10">Verification 1 - 2018 SSCOE</option>
				<option value="9">Verification 1 - 2018 OTG</option>
				<option value="8">2018 Travel Guide Collection</option>
				<option value="7">2018_OH_travel_guide_collection - TEST</option>
				<option value="6">2017_OH_COE_Sept2017-Feb2018</option>
				<option value="5">2017_OH_calendar_of_events</option>
				<option value="4">2017_OH_travel_guide_collection</option>
				<option value="3">2016 Ohio COE_Sept2016-Feb2017</option>
				<option value="2">2016_OH_travel_guide</option>
				<option value="1">2016 Ohio Calendar of Events</option>
			</select>
		</div>
		
		<div class="global-full">
			<label>Choose Method</label>
			<br>
			<label class="container-radio clear">
				<input type="radio" name="radio">
				<span class="radio"><span>&nbsp;</span>Email</span>
			</label>
			<br>
			<div class="pad-left">
				<label>Options for Email method</label>
				<br>
				<label class="container-checkbox">Exclude listings with fax numbers:
					<br><span class="font-normal">Check this box to exclude any listings who either have a fax number or have a primary contact with a fax number.</span>
					<input type="checkbox">
					<span class="checkmark"><span></span></span>
				</label>
			</div>
			<label class="container-radio clear">
				<input type="radio" name="radio">
				<span class="radio"><span>&nbsp;</span>Fax</span>
			</label>
			<br>
			<label class="container-radio clear">
				<input type="radio" name="radio">
				<span class="radio"><span>&nbsp;</span>Mail</span>
			</label>
			<br>
			<label class="container-radio clear">
				<input type="radio" name="radio">
				<span class="radio"><span>&nbsp;</span>Phone</span>
			</label>
		</div>
		
		<div class="global-full align-center">
			<p>&nbsp;</p>
			<button>Send Test</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button>View Recipients</button>
			<br>&nbsp;
			<br><button>Send Blast</button>
		</div>
		
	</div>
	
</div>

