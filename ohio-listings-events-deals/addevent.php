<?php

# addevent.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Add Event</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			<label>Event Name</label>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-left">
			<label>Physical Address</label>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-right">
			<label>Physical Address 2</label>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-left">
			<label>City</label>
			<input type="text" class="fill" />
		</div>
		
		<div class="global-right">
			<div class="float">
				<label>State</label>
				<input type="text" class="fill" style="width:60px;" />
			</div>
			<div class="float">
				<label>Zip</label>
				<input type="text" class="fill" />
			</div>
		</div>
		
		<div class="global-full">
			<label>Publish my listing under:</label>
			<br><?php include 'cities.php'; ?>
		</div>
		
		<div class="global-left">
			<label>Describe your event here:
				<br>(400 characters minimum, 1500 characters maximum)</label>
			<br><em>Descriptions are required, your event will be rejected without one.</em>
			<br><textarea class="fill"></textarea>
		</div>
		
		<div class="global-right">
			<label>Enter a shorter description for our print guide:
				<br>(100 characters maximum)</label>
			<br><em>Descriptions are required, your event will be rejected without one.</em>
			<br><textarea class="fill"></textarea>
		</div>
		
		<div class="global-full">
			<em>Enter the event date and times below. Identify any recurrence options, as needed. Start and end dates are required; your event may not be published if this information is not supplied.</em>
			<br>
			<div class="float">
				<label>Pick a start date</label>
				<br><input id="event-start-date" type="text" />
			</div>
			<div class="float">
				<label>Pick an end date</label>
				<br><input id="event-end-date" type="text" />
			</div>
		</div>
		
		<div class="global-full">
			<div class="float">
				<label>Start Time</label>
				<br>
				<select>
					<option>- Start Time -</option><option>12:00am</option><option>12:15am</option><option>12:30am</option><option>12:45am</option><option>1:00am</option><option>1:15am</option><option>1:30am</option><option>1:45am</option><option>2:00am</option><option>2:15am</option><option>2:30am</option><option>2:45am</option><option>3:00am</option><option>3:15am</option><option>3:30am</option><option>3:45am</option><option>4:00am</option><option>4:15am</option><option>4:30am</option><option>4:45am</option><option>5:00am</option><option>5:15am</option><option>5:30am</option><option>5:45am</option><option>6:00am</option><option>6:15am</option><option>6:30am</option><option>6:45am</option><option>7:00am</option><option>7:15am</option><option>7:30am</option><option>7:45am</option><option>8:00am</option><option>8:15am</option><option>8:30am</option><option>8:45am</option><option>9:00am</option><option>9:15am</option><option>9:30am</option><option>9:45am</option><option>10:00am</option><option>10:15am</option><option>10:30am</option><option>10:45am</option><option>11:00am</option><option>11:15am</option><option>11:30am</option><option>11:45am</option><option>12:00pm</option><option>12:15pm</option><option>12:30pm</option><option>12:45pm</option><option>1:00pm</option><option>1:15pm</option><option>1:30pm</option><option>1:45pm</option><option>2:00pm</option><option>2:15pm</option><option>2:30pm</option><option>2:45pm</option><option>3:00pm</option><option>3:15pm</option><option>3:30pm</option><option>3:45pm</option><option>4:00pm</option><option>4:15pm</option><option>4:30pm</option><option>4:45pm</option><option>5:00pm</option><option>5:15pm</option><option>5:30pm</option><option>5:45pm</option><option>6:00pm</option><option>6:15pm</option><option>6:30pm</option><option>6:45pm</option><option>7:00pm</option><option>7:15pm</option><option>7:30pm</option><option>7:45pm</option><option>8:00pm</option><option>8:15pm</option><option>8:30pm</option><option>8:45pm</option><option>9:00pm</option><option>9:15pm</option><option>9:30pm</option><option>9:45pm</option><option>10:00pm</option><option>10:15pm</option><option>10:30pm</option><option>10:45pm</option><option>11:00pm</option><option>11:15pm</option><option>11:30pm</option>
				</select>
			</div>
			<div class="float">
				<label>End Time</label>
				<br>
				<select>
					<option>- End Time -</option><option>12:15am</option><option>12:30am</option><option>12:45am</option><option>1:00am</option><option>1:15am</option><option>1:30am</option><option>1:45am</option><option>2:00am</option><option>2:15am</option><option>2:30am</option><option>2:45am</option><option>3:00am</option><option>3:15am</option><option>3:30am</option><option>3:45am</option><option>4:00am</option><option>4:15am</option><option>4:30am</option><option>4:45am</option><option>5:00am</option><option>5:15am</option><option>5:30am</option><option>5:45am</option><option>6:00am</option><option>6:15am</option><option>6:30am</option><option>6:45am</option><option>7:00am</option><option>7:15am</option><option>7:30am</option><option>7:45am</option><option>8:00am</option><option>8:15am</option><option>8:30am</option><option>8:45am</option><option>9:00am</option><option>9:15am</option><option>9:30am</option><option>9:45am</option><option>10:00am</option><option>10:15am</option><option>10:30am</option><option>10:45am</option><option>11:00am</option><option>11:15am</option><option>11:30am</option><option>11:45am</option><option>12:00pm</option><option>12:15pm</option><option>12:30pm</option><option>12:45pm</option><option>1:00pm</option><option>1:15pm</option><option>1:30pm</option><option>1:45pm</option><option>2:00pm</option><option>2:15pm</option><option>2:30pm</option><option>2:45pm</option><option>3:00pm</option><option>3:15pm</option><option>3:30pm</option><option>3:45pm</option><option>4:00pm</option><option>4:15pm</option><option>4:30pm</option><option>4:45pm</option><option>5:00pm</option><option>5:15pm</option><option>5:30pm</option><option>5:45pm</option><option>6:00pm</option><option>6:15pm</option><option>6:30pm</option><option>6:45pm</option><option>7:00pm</option><option>7:15pm</option><option>7:30pm</option><option>7:45pm</option><option>8:00pm</option><option>8:15pm</option><option>8:30pm</option><option>8:45pm</option><option>9:00pm</option><option>9:15pm</option><option>9:30pm</option><option>9:45pm</option><option>10:00pm</option><option>10:15pm</option><option>10:30pm</option><option>10:45pm</option><option>11:00pm</option><option>11:15pm</option><option>11:30pm</option><option>11:45pm</option>
				</select>
			</div>
		</div>
		
		<div class="global-full">
			<label>Choose 1 category that accurately represents your event:</label>
			<br>
			<div class="float third">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Arts & Cultural</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Crafts, Antiques & Shopping</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Fairs & Festivals</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Farms & Farmers Markets</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Food, Beer & Wine</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>History & Heritage</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Holiday & Seasonal Celebrations</span>
				</label>
			</div>
			
			<div class="float third">
				
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Music – Country</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Music – Dance</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Music – Festival</span>
				</label>
				<label class="container-checkbox clear"
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Music – Jazz</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Music – Orchestra/Symphony</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Music – Other</span>
				</label>
			</div>
			
			<div class="float third">
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Nature & Outdoors</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Sports & Competitions</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Theater & Film</span>
				</label>
				<label class="container-checkbox clear">
					<input type="checkbox" _checked="checked">
					<span class="checkmark"><span></span>Vehicles & Collectors</span>
				</label>
			</div>
			
			<div class="global-full align-center">
				<p>&nbsp;</p>
				<button>Save</button>
				<p><a href="#">Cancel</a></p>
			</div>
			
		</div>
		
	</div>
	
</div>
