<?php
$xmlstr = <<<XML
<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <LastAuthor>Fletcher, Peter</LastAuthor>
  <Created>2018-04-20T17:37:10Z</Created>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>11370</WindowHeight>
  <WindowWidth>19200</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s62">
   <NumberFormat ss:Format="General Date"/>
  </Style>
  <Style ss:ID="s63">
   <NumberFormat ss:Format="Long Time"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
  </Style>
  <Style ss:ID="s65">
   <NumberFormat ss:Format="Short Date"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="listing_export_all">
  <Table ss:ExpandedColumnCount="50" ss:ExpandedRowCount="4827" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:Index="3" ss:AutoFitWidth="0" ss:Width="204"/>
   <Column ss:Index="16" ss:AutoFitWidth="0" ss:Width="238.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="372.75"/>
   
   
   
   
   
   <Row>
    <Cell><Data ss:Type="String">listing_id</Data></Cell>
    <Cell><Data ss:Type="String">prefix</Data></Cell>
    <Cell><Data ss:Type="String">name</Data></Cell>
    <Cell><Data ss:Type="String">address</Data></Cell>
    <Cell><Data ss:Type="String">address2</Data></Cell>
    <Cell><Data ss:Type="String">city</Data></Cell>
    <Cell><Data ss:Type="String">state</Data></Cell>
    <Cell><Data ss:Type="String">zip</Data></Cell>
    <Cell><Data ss:Type="String">county</Data></Cell>
    <Cell><Data ss:Type="String">region</Data></Cell>
    <Cell><Data ss:Type="String">latitude</Data></Cell>
    <Cell><Data ss:Type="String">longitude</Data></Cell>
    <Cell><Data ss:Type="String">local</Data></Cell>
    <Cell><Data ss:Type="String">toll_free_us</Data></Cell>
    <Cell><Data ss:Type="String">fax</Data></Cell>
    <Cell><Data ss:Type="String">business_email</Data></Cell>
    <Cell><Data ss:Type="String">business_url</Data></Cell>
    <Cell><Data ss:Type="String">booking_url</Data></Cell>
    <Cell><Data ss:Type="String">start_date</Data></Cell>
    <Cell><Data ss:Type="String">end_date</Data></Cell>
    <Cell><Data ss:Type="String">categories</Data></Cell>
    <Cell><Data ss:Type="String">description</Data></Cell>
    <Cell><Data ss:Type="String">hours_json</Data></Cell>
    <Cell><Data ss:Type="String">contact_first_name</Data></Cell>
    <Cell><Data ss:Type="String">contact_last_name</Data></Cell>
    <Cell><Data ss:Type="String">contact_email</Data></Cell>
    <Cell><Data ss:Type="String">contact_address</Data></Cell>
    <Cell><Data ss:Type="String">contact_address2</Data></Cell>
    <Cell><Data ss:Type="String">contact_city</Data></Cell>
    <Cell><Data ss:Type="String">contact_state</Data></Cell>
    <Cell><Data ss:Type="String">contact_zip</Data></Cell>
    <Cell><Data ss:Type="String">contact_phone</Data></Cell>
    <Cell><Data ss:Type="String">contact_fax</Data></Cell>
    <Cell><Data ss:Type="String">contact_username</Data></Cell>
    <Cell><Data ss:Type="String">last_update_date</Data></Cell>
    <Cell><Data ss:Type="String">sun_start</Data></Cell>
    <Cell><Data ss:Type="String">sun_end</Data></Cell>
    <Cell><Data ss:Type="String">mon_start</Data></Cell>
    <Cell><Data ss:Type="String">mon_end</Data></Cell>
    <Cell><Data ss:Type="String">tue_start</Data></Cell>
    <Cell><Data ss:Type="String">tue_end</Data></Cell>
    <Cell><Data ss:Type="String">wed_start</Data></Cell>
    <Cell><Data ss:Type="String">wed_end</Data></Cell>
    <Cell><Data ss:Type="String">thu_start</Data></Cell>
    <Cell><Data ss:Type="String">thu_end</Data></Cell>
    <Cell><Data ss:Type="String">fri_start</Data></Cell>
    <Cell><Data ss:Type="String">fri_end</Data></Cell>
    <Cell><Data ss:Type="String">sat_start</Data></Cell>
    <Cell><Data ss:Type="String">sat_end</Data></Cell>
    <Cell><Data ss:Type="String">last_completed_step</Data></Cell>
   </Row>
   
   
   
   
   
   <Row>
    <Cell><Data ss:Type="Number">99</Data></Cell>
    <Cell ss:Index="3"><Data ss:Type="String">&quot;Heritage Loft&quot; at Pine Tree</Data></Cell>
    <Cell><Data ss:Type="String">4374 Shreve Rd</Data></Cell>
    <Cell ss:Index="6"><Data ss:Type="String">Wooster</Data></Cell>
    <Cell><Data ss:Type="String">OH</Data></Cell>
    <Cell><Data ss:Type="Number">44691</Data></Cell>
    <Cell><Data ss:Type="String">Wayne</Data></Cell>
    <Cell><Data ss:Type="String">Northeast</Data></Cell>
    <Cell><Data ss:Type="Number">40.744241000000002</Data></Cell>
    <Cell><Data ss:Type="Number">-81.985271999999995</Data></Cell>
    <Cell><Data ss:Type="String">330-264-1014</Data></Cell>
    <Cell ss:Index="16"><Data ss:Type="String">info@pinetreebarn.com</Data></Cell>
    <Cell><Data ss:Type="String">http://www.pinetreebarn.com</Data></Cell>
    <Cell ss:Index="21"><Data ss:Type="String">Specialty Shops</Data></Cell>
    <Cell><Data ss:Type="String">&lt;p&gt;Past the creaking, weathered heart-pine floors of the restored c.1868 bank barn, you are greeted by an old wooden staircase leading to what was, in days long past, the barn's original hayloft. Ascending this staircase, the hand-pegged beams and hand-hewn timbers whisper of&#160; a simpler time...before the &quot;Industrial Revolution&quot;. A time when hearts and hands were the machines of industry...&#160; Now home to the Heritage Loft, this warm and cozy space is a treasure trove, offering nearly 4,000 square feet of the Country's finest reproduction 18th century home furnishings and accessories.&#160;&#160; The Heritage Loft features the very best names in 18th Century furniture, crafted by hand in tiger maple, cherry and pine. Period accessories and crafts by the best local, Ohio and nationally recognized craftsmen and women.&#160; And an unparalleled selection of resources for those interested in period furnishings and decoration - fabrics, paints, wall coverings and more.&#160;A sampling of artisans includes&#160;D.R. Dimes, J.L. Treharn,&#160;Stephen Plaud, Bucks County, Pennsylvania Folk, David Benner, and Amanda Wilder Collection.&#160; VISIT US ON FACEBOOK!!&lt;/p&gt;</Data></Cell>
    <Cell><Data ss:Type="String">[[&quot;Sunday&quot;,&quot;10:00:00&quot;,&quot;17:00:00&quot;],[&quot;Monday&quot;,&quot;09:00:00&quot;,&quot;17:00:00&quot;],[&quot;Tuesday&quot;,&quot;09:00:00&quot;,&quot;17:00:00&quot;],[&quot;Wednesday&quot;,&quot;09:00:00&quot;,&quot;17:00:00&quot;],[&quot;Thursday&quot;,&quot;09:00:00&quot;,&quot;17:00:00&quot;],[&quot;Friday&quot;,&quot;09:00:00&quot;,&quot;17:00:00&quot;],[&quot;Saturday&quot;,&quot;09:00:00&quot;,&quot;17:00:00&quot;]]</Data></Cell>
    <Cell><Data ss:Type="String">Matt</Data></Cell>
    <Cell><Data ss:Type="String">Kilbourne</Data></Cell>
    <Cell><Data ss:Type="String">woostermatt@gmail.com</Data></Cell>
    <Cell><Data ss:Type="String">4374 Shreve Road</Data></Cell>
    <Cell ss:Index="29"><Data ss:Type="String">Wooster</Data></Cell>
    <Cell><Data ss:Type="String">OH</Data></Cell>
    <Cell><Data ss:Type="Number">44691</Data></Cell>
    <Cell><Data ss:Type="String">330-264-1014</Data></Cell>
    <Cell ss:Index="34"><Data ss:Type="String">matt@pinetreebarn.com</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="DateTime">2016-03-29T13:05:30.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T10:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T17:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T09:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T17:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T09:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T17:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T09:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T17:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T09:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T17:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T09:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T17:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T09:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T17:00:00.000</Data></Cell>
   </Row>
   
   
   
   
   
   <Row>
    <Cell><Data ss:Type="Number">61</Data></Cell>
    <Cell ss:Index="3"><Data ss:Type="String">#1 China Buffet</Data></Cell>
    <Cell><Data ss:Type="String">1587 Rombach Avenue</Data></Cell>
    <Cell ss:Index="6"><Data ss:Type="String">Wilmington</Data></Cell>
    <Cell><Data ss:Type="String">OH</Data></Cell>
    <Cell><Data ss:Type="Number">45177</Data></Cell>
    <Cell><Data ss:Type="String">Clinton</Data></Cell>
    <Cell><Data ss:Type="String">Southwest</Data></Cell>
    <Cell><Data ss:Type="Number">39.450063999999998</Data></Cell>
    <Cell><Data ss:Type="Number">-83.801586999999998</Data></Cell>
    <Cell><Data ss:Type="String">937-383-3888</Data></Cell>
    <Cell ss:Index="16"><Data ss:Type="String">christopher.bowsher@development.ohio.gov</Data></Cell>
    <Cell ss:Index="21"><Data ss:Type="String">Restaurants</Data></Cell>
    <Cell><Data ss:Type="String">Authentic Chinese restaurant. For dine in, carry out, or catering. All you can eat buffet. Serving seafood on Fridays and Saturdays.</Data></Cell>
    <Cell><Data ss:Type="String">[[&quot;Sunday&quot;,&quot;11:00:00&quot;,&quot;21:00:00&quot;],[&quot;Monday&quot;,&quot;11:00:00&quot;,&quot;21:00:00&quot;],[&quot;Tuesday&quot;,&quot;11:00:00&quot;,&quot;21:00:00&quot;],[&quot;Wednesday&quot;,&quot;11:00:00&quot;,&quot;21:00:00&quot;],[&quot;Thursday&quot;,&quot;11:00:00&quot;,&quot;21:00:00&quot;],[&quot;Friday&quot;,&quot;11:00:00&quot;,&quot;21:00:00&quot;],[&quot;Saturday&quot;,&quot;11:00:00&quot;,&quot;21:00:00&quot;]]</Data></Cell>
    <Cell><Data ss:Type="String">Tracy</Data></Cell>
    <Cell><Data ss:Type="String">Daniels</Data></Cell>
    <Cell><Data ss:Type="String">tdaniels@clintoncountyohio.com</Data></Cell>
    <Cell><Data ss:Type="String">13 N. South Street</Data></Cell>
    <Cell ss:Index="29"><Data ss:Type="String">Wilmington</Data></Cell>
    <Cell><Data ss:Type="String">OH</Data></Cell>
    <Cell><Data ss:Type="Number">45177</Data></Cell>
    <Cell><Data ss:Type="String">937-382-1738</Data></Cell>
    <Cell ss:Index="34"><Data ss:Type="String">tdaniels@clintoncountyohio.com</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="DateTime">2017-09-28T13:44:08.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T11:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T21:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T11:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T21:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T11:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T21:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T11:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T21:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T11:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T21:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T11:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T21:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T11:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T21:00:00.000</Data></Cell>
   </Row>
   
   
   
   
   
   <Row>
    <Cell><Data ss:Type="Number">73</Data></Cell>
    <Cell ss:Index="3"><Data ss:Type="String">1 More Fish Chartering</Data></Cell>
    <Cell><Data ss:Type="String">308 Illinois Avenue</Data></Cell>
    <Cell ss:Index="6"><Data ss:Type="String">Lorain</Data></Cell>
    <Cell><Data ss:Type="String">OH</Data></Cell>
    <Cell><Data ss:Type="Number">44052</Data></Cell>
    <Cell><Data ss:Type="String">Lorain</Data></Cell>
    <Cell><Data ss:Type="String">Northeast</Data></Cell>
    <Cell><Data ss:Type="Number">41.472352999999998</Data></Cell>
    <Cell><Data ss:Type="Number">-82.162609000000003</Data></Cell>
    <Cell><Data ss:Type="String">216-410-8654</Data></Cell>
    <Cell ss:Index="16"><Data ss:Type="String">fishbowlcar@yahoo.com</Data></Cell>
    <Cell><Data ss:Type="String">http://www.1morefishchartering.com</Data></Cell>
    <Cell ss:Index="21"><Data ss:Type="String">Fishing</Data></Cell>
    <Cell ss:StyleID="s64"><Data ss:Type="String">Lake Erie sport fishing charter; walleye, perch, steelhead.&#160; Located in Lorain, Ohio (Black River Landing/ Spitzer Marina).&#160; Trips run daily!&#10;</Data></Cell>
    <Cell><Data ss:Type="String">[[&quot;Sunday&quot;,&quot;07:00:00&quot;,&quot;19:00:00&quot;],[&quot;Monday&quot;,&quot;07:00:00&quot;,&quot;19:00:00&quot;],[&quot;Tuesday&quot;,&quot;07:00:00&quot;,&quot;19:00:00&quot;],[&quot;Wednesday&quot;,&quot;07:00:00&quot;,&quot;19:00:00&quot;],[&quot;Thursday&quot;,&quot;07:00:00&quot;,&quot;19:00:00&quot;],[&quot;Friday&quot;,&quot;07:00:00&quot;,&quot;19:00:00&quot;],[&quot;Saturday&quot;,&quot;07:00:00&quot;,&quot;19:00:00&quot;]]</Data></Cell>
    <Cell><Data ss:Type="String">Calvin</Data></Cell>
    <Cell><Data ss:Type="String">Coleman</Data></Cell>
    <Cell><Data ss:Type="String">fishbowlcar@yahoo.com</Data></Cell>
    <Cell><Data ss:Type="String">308 Illinois Avenue</Data></Cell>
    <Cell ss:Index="29"><Data ss:Type="String">Lorain</Data></Cell>
    <Cell><Data ss:Type="String">OH</Data></Cell>
    <Cell><Data ss:Type="Number">44052</Data></Cell>
    <Cell><Data ss:Type="String">216-410-8654</Data></Cell>
    <Cell ss:Index="34"><Data ss:Type="String">fishbowlcar@yahoo.com</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="DateTime">2016-09-14T17:19:16.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T07:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T19:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T07:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T19:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T07:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T19:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T07:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T19:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T07:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T19:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T07:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T19:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T07:00:00.000</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="DateTime">1899-12-31T19:00:00.000</Data></Cell>
   </Row>
   
   
   
   
   
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Selected/>
   <TopRowVisible>4792</TopRowVisible>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
XML;
?>