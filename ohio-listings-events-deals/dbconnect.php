<?php

$servername = 'localhost';
$username = 'root';
$password = '';
$db = 'ohio_listings';

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
	$connected = 'false';
	die('Connection failed: ' . $conn->connect_error);
} else {
	$connected = 'true';
}
