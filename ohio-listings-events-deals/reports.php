<?php

# reports.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Collection Reports</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			<label>Pick A Collection</label>
			<br>
			<select>
				<option></option>
			</select>
		</div>
		
		<div class="global-full">
			<label>Select By Region</label>
			<br>
			<select>
				<option>All</option>
				<option>Amish</option>
				<option>Central</option>
				<option>Northeast</option>
				<option>Northwest</option>
				<option>Southeast</option>
				<option>Southwest</option>
			</select>
		</div>
		
		<div class="global-full">
			<label>Select By County</label>
			<br>
			<select name="county">
				<option value="">All</option>
				<option value="Adams">Adams</option>
				<option value="Allen">Allen</option>
				<option value="Ashland">Ashland</option>
				<option value="Ashtabula">Ashtabula</option>
				<option value="Athens">Athens</option>
				<option value="Auglaize">Auglaize</option>
				<option value="Belmont">Belmont</option>
				<option value="Brown">Brown</option>
				<option value="Butler">Butler</option>
				<option value="Carroll">Carroll</option>
				<option value="Champaign">Champaign</option>
				<option value="Clark">Clark</option>
				<option value="Clermont">Clermont</option>
				<option value="Clinton">Clinton</option>
				<option value="Columbiana">Columbiana</option>
				<option value="Coshocton">Coshocton</option>
				<option value="Crawford">Crawford</option>
				<option value="Cuyahoga">Cuyahoga</option>
				<option value="Darke">Darke</option>
				<option value="Defiance">Defiance</option>
				<option value="Delaware">Delaware</option>
				<option value="Erie">Erie</option>
				<option value="Fairfield">Fairfield</option>
				<option value="Fayette">Fayette</option>
				<option value="Franklin">Franklin</option>
				<option value="Fulton">Fulton</option>
				<option value="Gallia">Gallia</option>
				<option value="Geauga">Geauga</option>
				<option value="Greene">Greene</option>
				<option value="Guernsey">Guernsey</option>
				<option value="Hamilton">Hamilton</option>
				<option value="Hancock">Hancock</option>
				<option value="Hardin">Hardin</option>
				<option value="Harrison">Harrison</option>
				<option value="Henry">Henry</option>
				<option value="Highland">Highland</option>
				<option value="Hocking">Hocking</option>
				<option value="Holmes">Holmes</option>
				<option value="Huron">Huron</option>
				<option value="Jackson">Jackson</option>
				<option value="Jefferson">Jefferson</option>
				<option value="Knox">Knox</option>
				<option value="Lake">Lake</option>
				<option value="Lawrence">Lawrence</option>
				<option value="Licking">Licking</option>
				<option value="Logan">Logan</option>
				<option value="Lorain">Lorain</option>
				<option value="Lucas">Lucas</option>
				<option value="Madison">Madison</option>
				<option value="Mahoning">Mahoning</option>
				<option value="Marion">Marion</option>
				<option value="Medina">Medina</option>
				<option value="Meigs">Meigs</option>
				<option value="Mercer">Mercer</option>
				<option value="Miami">Miami</option>
				<option value="Monroe">Monroe</option>
				<option value="Montgomery">Montgomery</option>
				<option value="Morgan">Morgan</option>
				<option value="Morrow">Morrow</option>
				<option value="Muskingum">Muskingum</option>
				<option value="Noble">Noble</option>
				<option value="Ottawa">Ottawa</option>
				<option value="Paulding">Paulding</option>
				<option value="Perry">Perry</option>
				<option value="Pickaway">Pickaway</option>
				<option value="Pike">Pike</option>
				<option value="Portage">Portage</option>
				<option value="Preble">Preble</option>
				<option value="Putnam">Putnam</option>
				<option value="Richland">Richland</option>
				<option value="Ross">Ross</option>
				<option value="Sandusky">Sandusky</option>
				<option value="Scioto">Scioto</option>
				<option value="Seneca">Seneca</option>
				<option value="Shelby">Shelby</option>
				<option value="Stark">Stark</option>
				<option value="Summit">Summit</option>
				<option value="Trumbull">Trumbull</option>
				<option value="Tuscarawas">Tuscarawas</option>
				<option value="Union">Union</option>
				<option value="Van Wert">Van Wert</option>
				<option value="Vinton">Vinton</option>
				<option value="Warren">Warren</option>
				<option value="Washington">Washington</option>
				<option value="Wayne">Wayne</option>
				<option value="Williams">Williams</option>
				<option value="Wood">Wood</option>
				<option value="Wyandot">Wyandot</option>
			</select>
		</div>
		
		<div class="global-full">
			<label>Select A Group</label>
			<br>
			<div class="float">
				<label class="container-radio clear">Non-respondents only
					<input type="radio" checked="checked" name="reports-group">
					<span class="radio"><span></span></span>
				</label>
			</div>
			<div class="float">
				<label class="container-radio clear">Respondents only
					<input type="radio" name="reports-group">
					<span class="radio"><span></span></span>
				</label>
			</div>
		</div>
		
		<div class="global-full">
			<label>Choose File Output</label>
			<br>
			<div class="float">
				<label class="container-radio clear">CSV
					<input type="radio" checked="checked" name="reports-file-output">
					<span class="radio"><span></span></span>
					<p>
						<div style="font-weight:400;">
							CSV Output Options
							<p>Group By:
								<select>
									<option>Owner</option>
									<option>Listing (inlucdes collection dates)</option>
								</select>
							</p>
							<p>Sort By:
								<select>
									<option>Owner</option>
									<option>Region</option>
								</select>
							</p>
						</div>
					</p>
				</label>
			</div>
			<div class="float">
				<label class="container-radio clear">PDF
					<input type="radio" name="reports-file-output">
					<span class="radio"><span></span></span>
					<p>
						<div style="font-weight:400;">
							PDF Output Options
							<p>Sort By:
								<select>
									<option>Owner</option>
									<option>Region</option>
								</select>
							</p>
						</div>
					</p>
				</label>
			</div>
		</div>
		
		<div class="global-full align-center">
			<button>Download</button>
			<p><a href="#">Cancel</a></p>
		</div>
		
	</div>
	
</div>

