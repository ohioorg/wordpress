<?php

# login.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Login</h2>
		<p>Enter your username and password below and we'll move you right along.</p>
	</div>
	
	<div class="global-body">
		
		<div class="global-full-text">
			<a href="index.php?page=forgotpassword.php">Forgot Password?</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="index.php?page=register.php">Register for an Account</a>
		</div>
		
		
		
		<div class="global-left clear pad-top">
			<label>Username</label>
			<br>
			<input type="text" class="fill" value="peter.fletcher@development.ohio.gov" />
		</div>
		
		<div class="global-left pad-top clear">
			<label>Password</label>
			<br>
			<input type="password" class="fill" value="ohio123" />
			<br>
			<label class="container-checkbox clear">
				<input type="checkbox" _checked="checked">
				<span class="checkmark"><span></span>Remember my username</span>
			</label>
		</div>
		
		<div class="global-full clear align-center pad-top pad-bottom">
			<a href="index.php?page=dashboard.php" class="button">Log In</a>
		</div>
		
	</div>
	
</div>

