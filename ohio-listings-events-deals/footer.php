<?php

# footer.php

?>

<footer>

	<div class="responsive">
	
		&#0169; <?php echo date('Y') . ' Ohio Development Services Agency, TourismOhio.<br>John R. Kasich, Governor. Mary Taylor, Lt. Governor.'; ?>

	</div>
	
</footer>

</div><!-- close .container -->


	<script type="text/javascript">
		
		$(document).ready(function($){
			//alert('jQuery loaded successfully.');
		});
		
		/*
		$.datepicker.setDefaults({
			showOn: "both",
			buttonImageOnly: true,
			buttonImage: "calendar.gif",
			buttonText: "Calendar"
		});
		*/
		
		$( "#event-start-date" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		$( "#event-end-date" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		
		$( "#export-events-from-date" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		$( "#export-events-to-date" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		
		$( "#export-deals-from-date" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		$( "#export-deals-to-date" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		
		$( "#collection-start-date" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		$( "#collection-end-date" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
		
	</script>
	

</body>

</html>
