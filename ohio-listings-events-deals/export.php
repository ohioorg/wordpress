<?php

# export.php

?>


<div class="profile content-border">
	
	<div class="global-header">
		<h2>Export Listings</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-left">
			Select a Product
			<select>
				<option>Ohio Calendar of Events</option>
				<option>Ohio Travel Guide</option>
				<option selected="selected">Ohio.org</option>
			</select>
		</div>
		
		<div class="global-right">
			<label class="container-checkbox">
				<input type="checkbox">
				<span class="checkmark"><span></span>Active Only</span>
			</label>
		</div>
		
		<div class="global-full-text">
			<label class="container-radio clear">
				<input type="radio" checked="checked" name="radio">
				<span class="radio"><span>&nbsp;</span>Listings</span>
			</label>
		</div>
		
		<div class="global-full-text pad-top pad-left">
			Choose Categories
		</div>
		
		<div class="global-left pad-left">
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Arts/Culture</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Art Exhibits/Exhibitions</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Art Museums and Galleries</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Botanical</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Theatres</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Breweries &amp; Distilleries</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Craft Breweries</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Distilleries</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Family Fun</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>AgriTourism</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Amusement Parks/Fun Centers</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Day Spas</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Educational</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Halls of Fame</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Interactive Museums</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Lake and River Cruises</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Scenic Trains</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Science</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Tours</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Water Parks</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Zoo/Animal Parks</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Food</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Food Tours</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Restaurants</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>History</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Amish Heritage</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Appalachia</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Aviation/Space Exploration</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Black History</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Civil War</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Education/Research</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Heritage Area Tours</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Historic Downtowns</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Historic Sites</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Lighthouses</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Museums</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Native Americans</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Presidential</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Scenic Byways</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Lodging</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Bed &amp; Breakfasts</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Hotels, Motels, &amp; Inns</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Resorts, Retreats, &amp; Lodges</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>RV Parks &amp; Campgrounds</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Vacation Rentals &amp; Cabins</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Outdoors</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Beaches</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Birding</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Canoeing</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Fishing</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Hiking</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Horseback Riding</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Hunting</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Local &amp; Regional Parks</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>National Parks &amp; Forests</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Ohio State Parks &amp; Natural Areas</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Recreational Boating</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Skiing</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Ziplines</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Shopping</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Amish</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Antiques</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Arts &amp; Crafts</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Farm Markets</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Flea Markets</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Malls</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Outlet Shopping</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Pottery &amp; Glass</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Shopping Districts</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Specialty Shops</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Sports &amp; Recreation</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Baseball</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Basketball</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Biking</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Extreme Sports</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Football</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Golf Courses</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Hockey</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Horse Racing</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Motorsports</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Other</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Soccer</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Stadiums &amp; Arenas</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Visitor Services</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Convention &amp; Meeting Facilities</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Convention &amp; Visitor Bureaus</span>
				</label>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Transportation</span>
				</label>
			</label>
			
			<label class="container-checkbox clear">
				<input type="checkbox"><span class="checkmark"><span></span>Wineries</span>
				<label class="container-checkbox-sub clear">
					<input type="checkbox"><span class="checkmark"><span></span>Wineries</span>
				</label>
			</label>
			
			<p>&nbsp;</p>
		</div>
		
		<div class="global-full-text pad-left">
			Filter By:
		</div>
		
		<div class="global-full pad-left">
			<div class="float pad-left">
				<label>Region</label>
				<br>
				<select>
					<option>Any</option>
					<option>Amish</option>
					<option>Central</option>
					<option>Northeast</option>
					<option>Northwest</option>
					<option>Southeast</option>
					<option>Southwest</option>
				</select>
			</div>
			<div class="float pad-left">
				<label>County</label>
				<br>
				<select>
					<option>Any</option>
				</select>
			</div>
		</div>
		
		<div class="global-full">
			<label class="container-radio clear">
				<input type="radio" name="radio">
				<span class="radio"><span>&nbsp;</span>Events</span>
			</label>
			<br>
			<div class="float pad-left">
				<label>From</label>
				<br><input id="export-events-from-date" type="text" />
			</div>
			<div class="float pad-left">
				<label>To</label>
				<br><input id="export-events-to-date" type="text" />
			</div>
			<div class="clear"></div>
			<div class="float pad-left pad-top pad-bottom">
				<label>Region</label>
				<br>
				<select>
					<option>Any</option>
					<option>Amish</option>
					<option>Central</option>
					<option>Northeast</option>
					<option>Northwest</option>
					<option>Southeast</option>
					<option>Southwest</option>
				</select>
			</div>
			<div class="float pad-left pad-top pad-bottom">
				<label>County</label>
				<br>
				<select>
					<option>Any</option>
				</select>
			</div>
			<div class="float pad-left pad-top pad-bottom">
				<label>OEF</label>
				<br>
				<select>
					<option>Any</option>
					<option>OEF Only</option>
					<option>DE Only</option>
				</select>
			</div>
		</div>
		
		<div class="global-full">
			<label class="container-radio clear">
				<input type="radio" name="radio">
				<span class="radio"><span>&nbsp;</span>Deals</span>
			</label>
			<br>
			<div class="float pad-left">
				<label>From</label>
				<br><input id="export-deals-from-date" type="text" />
			</div>
			<div class="float pad-left">
				<label>To</label>
				<br><input id="export-deals-to-date" type="text" />
			</div>
			<div class="clear"></div>
			<div class="float pad-left pad-top pad-bottom">
				<label>Region</label>
				<br>
				<select>
					<option>Any</option>
					<option>Amish</option>
					<option>Central</option>
					<option>Northeast</option>
					<option>Northwest</option>
					<option>Southeast</option>
					<option>Southwest</option>
				</select>
			</div>
			<div class="float pad-left pad-top pad-bottom">
				<label>County</label>
				<br>
				<select>
					<option>Any</option>
				</select>
			</div>
		</div>
		
		
		<div class="global-full align-center pad-top">
			<div class="pad-top">
				<button>View Results</button>
				<br>&nbsp;
				<br>
				<button>Export</button>
			</div>
		</div>
		
		
		
		
		