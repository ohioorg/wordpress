<?php

# collections.php

?>

<div class="profile content-border">
	
	<div class="global-header">
		<h2>Collection Management</h2>
	</div>
	
	<div class="global-body">
		
		<div class="global-full">
			Existing Collections:
			
			<div class="dashboard-table">
				<div class="dashboard-table-row header-row">
					<a class="d-col" style="min-width:200px;">Collection Name</a>
					<a class="d-col">Product Name</a>
					<div class="d-col">Start Date</div>
					<div class="d-col">End Date</div>
				</div>
				
				<div class="dashboard-table-row">
					<div class="d-col"></div>
					<div class="d-col"></div>
					<div class="d-col"></div>
					<div class="d-col"></div>
				</div>
			</div>
			
		</div>
		
		<div class="global-full align-center pad-top">
			<a class="button" href="index.php?page=addcollection.php">Add Collection</a>
		</div>
		
	</div>
	
</div>


